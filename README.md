Analysis Framework for PROMPT HEAVY NEUTRAL LEPTON
====================================


based on the SUSYTool framework


# To start:
get the code from repository

```bash
git clone https://gitlab.cern.ch/bngair/hnl_framework_r22.git



# Configuration of run.py:

In ``ss3ldev/DiLepton/scripts/run.py`` (and ``launch_jobs.py``):

prefix     : the tag of the ntuples (do not forget to update the prefix also in ``launch_jobs.py`` when submitting jobs per list of samples!)
LocalSample: the folder where the input file is. The line code is
```python
parser.add_argument("--LocalSample", help="input sample used for local mode", default="LocalSampleName")
```
Note: for the local input files you can use simbolic links created in the folder INPUTFILES, using as name the ''LocalSampleName''.
e.g.
```bash
ln -s /path/to/your/input/samples LocalSampleName
```
anyway we advice precaution in doing this.



# To compile
```bash
cd hnl_framework_r22/ss3ldev
source initialize.sh
```


# To run locally:

MC
```bash
python /$BASEDIR/Run/run.py --doSys 0
```

DATA
```bash
python /$BASEDIR/Run/run.py --isData 1
```

PHYSLITE
```bash
python /$BASEDIR/Run/run.py --doSys 0 --isPHYSLITE 1
```

you can choose as well other options like '--nevents 1000' for testing from the help menu:

More details will be added Soon ...
