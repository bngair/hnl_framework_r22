/*****************************************************************************/
/*                                                                           */
/* File Name        : FakeLepMCTemplate.cxx                                  */
/* Author           : Othmane Rifki			                                 */
/* Email            : othmane.rifki@cern.ch			                         */
/* Description      : Source file for the MCTemplateCorr class               */
/*                                                                           */
/***** C 2016 ****************************************************************/

/**
  hhttps://acode-browser.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h?v=21.1
  https://acode-browser.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/MCTruthClassifier/Root/MCTruthClassifier.cxx?v=21.1

  type:
  IsoElectron     =  2
  NonIsoElectron  =  3
  BkgElectron     =  4
  IsoMuon         =  6
  NonIsoMuon      =  7

  origin:
  PhotonConv    = 5
  top           = 10
  WBoson        = 12
  ZBoson        = 13
  SUSY          = 22
  FSRPhot       = 40
  BottomMeson   = 26
  BBbarMeson    = 29
  BottomBaryon  = 33
*/

#include "DiLepton/MCTemplateCorr.h"

MCTemplateCorr::MCTemplateCorr()
{
  Reset();
}

float MCTemplateCorr::GetFakeCorrection(float &unc)
{
  if (m_reset)
  {
    std::cerr << "ERROR [MCTemplateCorr]: Come on, you must provide truth information about signal leptons first!!!!\n";
    throw;
  }

  sortLeptons(&m_lepinfo);
  classify_leptons(&m_lepinfo);
  SetMCResult(m_lepinfo.channelNumber);

  if (debug_me)
    for (int ilep = 0; ilep < m_lepinfo.num_leptons; ilep++)
    {
      if (m_lepinfo.is_electron[ilep])
        printf("lepton %d -> electron with pt=%.2fGeV charge=%+d type=%d firstmum_type=%d fallback_type=%d origin=%d firstmum_origin=%d  fallback_origin=%d pdgid=%d firstmum_pdgid=%d -->  is_fake_HF=%d is_fake_LF=%d is_chmisid=%d\n", ilep, m_lepinfo.pT[ilep] / 1000., m_lepinfo.recoCharge[ilep], m_lepinfo.truthType[ilep], m_lepinfo.truthFirstMumType[ilep], m_lepinfo.truthFallBackType[ilep], m_lepinfo.truthOrigin[ilep], m_lepinfo.truthFirstMumOrigin[ilep], m_lepinfo.truthFallBackOrigin[ilep], m_lepinfo.truthPdgId[ilep], m_lepinfo.truthFirstMumPdgid[ilep], m_lepinfo.is_fake_HF[ilep], m_lepinfo.is_fake_LF[ilep], m_lepinfo.is_chmisid[ilep]);
      else
        printf("lepton %d -> muon with pt=%.2fGeV charge=%+d type=%d fallback_type=%d origin=%d fallback_origin=%d -->  is_fake_HF=%d is_fake_LF=%d is_chmisid=%d\n", ilep, m_lepinfo.pT[ilep] / 1000., m_lepinfo.recoCharge[ilep], m_lepinfo.truthType[ilep], m_lepinfo.truthFallBackType[ilep], m_lepinfo.truthOrigin[ilep], m_lepinfo.truthFallBackOrigin[ilep], m_lepinfo.is_fake_HF[ilep], m_lepinfo.is_fake_LF[ilep], m_lepinfo.is_chmisid[ilep]);
    }

  double fakeCorr = 1.;

  int index_fake_HF = -1;
  int index_fake_LF = -1;
  int index_chmisid = -1;
  int num_lept = 0;
  for (int i = 0; i < m_lepinfo.num_leptons && num_lept < 4; i++)
  {
    num_lept++;
    if (m_lepinfo.is_fake_HF[i])
    {
      index_fake_HF = i;
      break;
    }
    else if (m_lepinfo.is_fake_LF[i])
    {
      index_fake_LF = i;
      break;
    }
  }
  num_lept = 0;
  for (int i = 0; i < m_lepinfo.num_leptons && num_lept < 4; i++)
  {
    num_lept++;
    if (m_lepinfo.is_chmisid[i])
    {
      index_chmisid = i;
      break;
    }
  }

  if (index_fake_HF >= 0)
  {
    if (m_lepinfo.is_electron[index_fake_HF])
    {
      fakeCorr = HF_EL_Correction;
      unc = HF_EL_Uncert;
    }
    else
    {
      fakeCorr = HF_MU_Correction;
      unc = HF_MU_Uncert;
    }
  }
  else if (index_fake_LF >= 0)
  {
    if (m_lepinfo.is_electron[index_fake_LF])
    {
      fakeCorr = LF_EL_Correction;
      unc = LF_EL_Uncert;
    }
    else
    {
      fakeCorr = LF_MU_Correction;
      unc = LF_MU_Uncert;
    }
  }
  else if (index_chmisid >= 0)
  {
    fakeCorr = chMisId_Correction;
    unc = chMisId_Uncert;
  }
  else
  {
    fakeCorr = 1.0;
    unc = 0.0;
  }

  if (debug_me)
    printf("MC Template Fake Correction = %f +- %f\n", fakeCorr, unc);

  Reset();
  return fakeCorr;
}

void MCTemplateCorr::AddElectron(int channelnumber, double pt, int charge, int type, int firstmum_type, int fallback_type, int origin, int firstmum_origin, int fallback_origin, int pdgid, int firstmum_pdgid)
{
  if (debug_me)
  {
    if (m_index == 0)
      printf("---------------- In the electron loop ----------------\n");

    printf("AddElectron::Added an electron with pt=%.2fGeV charge=%+d type=%d firstmum_type=%d fallback_type=%d origin=%d firstmum_origin=%d  fallback_origin=%d pdgid=%d firstmum_pdgid=%d \n", pt / 1000., charge, type, firstmum_type, fallback_type, origin, firstmum_origin, fallback_origin, pdgid, firstmum_pdgid);
  }

  m_lepinfo.channelNumber = channelnumber;
  m_lepinfo.is_electron[m_index] = true;
  m_lepinfo.pT[m_index] = pt;
  m_lepinfo.recoCharge[m_index] = charge;
  m_lepinfo.truthType[m_index] = type;
  m_lepinfo.truthFirstMumType[m_index] = firstmum_type;
  m_lepinfo.truthFallBackType[m_index] = fallback_type;
  m_lepinfo.truthOrigin[m_index] = origin;
  m_lepinfo.truthFirstMumOrigin[m_index] = firstmum_origin;
  m_lepinfo.truthFallBackOrigin[m_index] = fallback_origin;
  m_lepinfo.truthPdgId[m_index] = pdgid;
  m_lepinfo.truthFirstMumPdgid[m_index] = firstmum_pdgid;
  m_lepinfo.recoPdgId[m_index] = charge * -11;

  m_index++;
  m_lepinfo.num_leptons = m_index;
  m_reset = false;
}

void MCTemplateCorr::AddMuon(int channelnumber, double pt, int charge, int type, int fallback_type, int origin, int fallback_origin)
{
  if (debug_me)
  {
    if (m_index == 0)
      printf("----------------In the muon loop ----------------\n");

    printf("AddMuon::Added a muon with pt=%.2fGeV charge=%+d type=%d fallback_type=%d origin=%d fallback_origin=%d \n", pt / 1000., charge, type, fallback_type, origin, fallback_origin);
  }

  m_lepinfo.channelNumber = channelnumber;
  m_lepinfo.is_electron[m_index] = false;
  m_lepinfo.pT[m_index] = pt;
  m_lepinfo.recoCharge[m_index] = charge;
  m_lepinfo.truthType[m_index] = type;
  m_lepinfo.truthFirstMumType[m_index] = -1;
  m_lepinfo.truthFallBackType[m_index] = fallback_type;
  m_lepinfo.truthOrigin[m_index] = origin;
  m_lepinfo.truthFirstMumOrigin[m_index] = -1;
  m_lepinfo.truthFallBackOrigin[m_index] = fallback_origin;
  m_lepinfo.truthPdgId[m_index] = -1;
  m_lepinfo.truthFirstMumPdgid[m_index] = -1;
  m_lepinfo.recoPdgId[m_index] = charge * -13;

  m_index++;
  m_lepinfo.num_leptons = m_index;
  m_reset = false;
}

///------------------------------------------------------------------------
void MCTemplateCorr::classify_leptons(my_lep *lep)
{
  /// if (lep->num_leptons < 0 || lep->pT[0] < LeptPtMIN) {
  /// std::cerr << "ERROR [MCTemplateCorr]: Well, there is an incorrect usage of cuts!!!! (lep->num_leptons = " << lep->num_leptons << ", lep->pT[0] = " << lep->pT[0] << ", lep->pT[1] = " << lep->pT[1] << std::endl;
  ///	throw;
  /// }

  bool iso_e = false;
  bool iso_m = false;
  bool HF_e = false;
  bool HF_m = false;
  bool chmisId = false;

  int num_lept = 0;
  for (int i = 0; i < lep->num_leptons && num_lept < 4; i++)
  {
    lep->is_fake_HF[i] = false;
    lep->is_fake_LF[i] = false;
    lep->is_chmisid[i] = false;
    num_lept++;
  }

  num_lept = 0;
  for (int i = 0; i < lep->num_leptons && num_lept < 4; i++)
  {
    num_lept++;

    if (lep->is_electron[i])
    {
      /// Prompt electron with correct charge
      bool isCorrectCharge = (lep->recoPdgId[i] != 0) ? ((lep->truthFirstMumPdgid[i] * (-lep->recoPdgId[i])) < 0) : ((lep->truthFirstMumPdgid[i] * (-lep->truthPdgId[i])) < 0);
      /// std::cout << lep->recoPdgId[i] << " " << lep->truthFirstMumPdgid[i] << " " << lep->recoPdgId[i] << " " << lep->truthFirstMumPdgid[i] << " " << lep->truthPdgId[i] << std::endl;
      bool isPrompEle = (lep->truthType[i] == 2 || ((lep->truthFirstMumType[i] == 2 || lep->truthFallBackType[i] == 2) && (fabs(lep->truthFirstMumPdgid[i]) == 11 /*|| abs(lep->recoPdgId[i])==11*/)));
      bool isFRS = ((lep->truthType[i] == 15 && lep->truthOrigin[i] == 40) || (lep->truthType[i] == 4 && lep->truthOrigin[i] == 5 && lep->truthFirstMumType[i] == 15 && lep->truthFirstMumOrigin[i] == 40));
      iso_e = ((isPrompEle && isCorrectCharge) || isFRS);
      if (debug_me)
        printf("lepton %d isPrompEle = %d isFRS = %d isCorrectCharge = %d (iso_e = %d)\n", i, (int)isPrompEle, (int)isFRS, (int)isCorrectCharge, (int)iso_e);

      /// Charge flip electron
      bool isWrongCharge = (lep->recoPdgId[i] != 0) ? ((lep->truthFirstMumPdgid[i] * (-lep->recoPdgId[i])) > 0) : ((lep->truthFirstMumPdgid[i] * (-lep->truthPdgId[i])) > 0);
      chmisId = ((isPrompEle && isWrongCharge));
      if (debug_me)
        printf("lepton %d isPrompEle = %d  isWrongCharge = %d chmisId = %d\n", i, (int)isPrompEle, (int)isWrongCharge, (int)chmisId);

      /// electron from HF
      bool has_b_hadron_origin = (lep->truthOrigin[i] == 26 || lep->truthOrigin[i] == 29 || lep->truthOrigin[i] == 33 || lep->truthFirstMumOrigin[i] == 26 || lep->truthFirstMumOrigin[i] == 29 || lep->truthFirstMumOrigin[i] == 33);
      bool has_b_hadron_fallbackorigin = (lep->truthFallBackOrigin[i] == 26 || lep->truthFallBackOrigin[i] == 29 || lep->truthFallBackOrigin[i] == 33);
      bool has_c_hadron_origin = (lep->truthOrigin[i] == 25 || lep->truthOrigin[i] == 32 || lep->truthOrigin[i] == 27 || lep->truthFirstMumOrigin[i] == 25 || lep->truthFirstMumOrigin[i] == 32 || lep->truthFirstMumOrigin[i] == 27);
      bool has_c_hadron_origin_fallBack = (lep->truthFallBackOrigin[i] == 25 || lep->truthFallBackOrigin[i] == 32 || lep->truthFallBackOrigin[i] == 27);
      bool has_tau_origin1 = ((lep->truthType[i] == 3 || lep->truthFirstMumType[i] == 3 || lep->truthType[i] == 15 || lep->truthFirstMumType[i] == 15) && (lep->truthOrigin[i] == 9 || lep->truthFirstMumOrigin[i] == 9));
      bool from_FSR_Tau = (lep->truthFirstMumOrigin[i] == 40 && lep->truthFallBackOrigin[i] == 9);
      HF_e = (has_b_hadron_origin || ((lep->truthOrigin[i] == 40 || lep->truthFirstMumOrigin[i] == 40) && has_b_hadron_fallbackorigin) || (has_c_hadron_origin && lep->truthType[i] != 2) || ((lep->truthOrigin[i] == 40 || lep->truthFirstMumOrigin[i] == 40 || lep->truthFallBackType[i] == 3) && has_c_hadron_origin_fallBack) || has_tau_origin1 || from_FSR_Tau);
    }
    else
    {
      /// Prompt muon
      iso_m = ((lep->truthType[i] == 6 && abs(lep->recoPdgId[i]) == 13) || (lep->truthType[i] == 6 && (lep->truthOrigin[i] == 27 || lep->truthOrigin[i] == 29)));
      if (debug_me)
        printf("lepton %d iso_m = %d\n", i, (int)iso_m);

      /// muon from HF
      bool has_b_hadron_origin = (lep->truthOrigin[i] == 26 || lep->truthOrigin[i] == 29 || lep->truthOrigin[i] == 33);
      bool has_b_hadron_fallbackorigin = (lep->truthFallBackOrigin[i] == 26 || lep->truthFallBackOrigin[i] == 29 || lep->truthFallBackOrigin[i] == 33);
      // bool has_c_hadron_origin = ( lep->truthOrigin[i]==25 || lep->truthOrigin[i]==32 || lep->truthOrigin[i]==27);
      // bool has_c_hadron_origin_fallBack = ( lep->truthFallBackOrigin[i]==25 || lep->truthFallBackOrigin[i]==32 || lep->truthFallBackOrigin[i]==27 );
      // bool has_tau_origin1 = ( (lep->truthType[i]==15 || lep->truthFirstMumType[i]==15) && (lep->truthOrigin[i]==9 || lep->truthFirstMumOrigin[i]==9) );
      // bool has_tau_origin2 = ( lep->truthType[i]==7 && lep->truthOrigin[i]==9 );
      // bool from_FSR_Tau = ( lep->truthFirstMumOrigin[i]==40 && lep->truthFallBackOrigin[i]==9 );
      HF_m = (has_b_hadron_origin || ((lep->truthOrigin[i] == 40 || lep->truthFirstMumOrigin[i] == 40 || lep->truthFallBackType[i] == 7) && has_b_hadron_fallbackorigin));
      // || ( has_c_hadron_origin && lep->truthType[i]!=6 ) || ( (lep->truthOrigin[i]==40 || lep->truthFirstMumOrigin[i]==40) && has_c_hadron_origin_fallBack )
      // || has_tau_origin1 || has_tau_origin2 || from_FSR_Tau );
    }

    if (iso_e || iso_m)
    { /// Prompt lepton
      lep->is_fake_HF[i] = false;
      lep->is_fake_LF[i] = false;
      lep->is_chmisid[i] = false;
      if (debug_me)
        printf("lepton %d iso_e = %d iso_m = %d\n", i, (int)iso_e, (int)iso_m);
    }
    else if (chmisId)
    { /// charge flipped electrons from the isolated electron collection
      lep->is_fake_HF[i] = false;
      lep->is_fake_LF[i] = false;
      lep->is_chmisid[i] = true;
      if (debug_me)
        printf("lepton %d chmisId = %d\n", i, (int)chmisId);
    }
    else if (HF_e || HF_m)
    { /// non-prompt leptons from HF
      lep->is_fake_HF[i] = true;
      lep->is_fake_LF[i] = false;
      lep->is_chmisid[i] = false;
      if (debug_me)
        printf("lepton %d HF_e = %d HF_m = %d\n", i, (int)HF_e, (int)HF_m);
    }
    else
    { /// everything else is LF
      lep->is_fake_HF[i] = false;
      lep->is_fake_LF[i] = true;
      lep->is_chmisid[i] = false;
      if (debug_me)
        printf("lepton %d light flavor = 1\n", i);
    }
  }

  return;
}

/// Compare lep pt's
///----------------------------------------------------------------------------
int MCTemplateCorr::lep_pt_comparator(const void *a, const void *b)
{
  float pt1 = ((srtLep *)a)->pT;
  float pt2 = ((srtLep *)b)->pT;
  if (pt1 < pt2)
    return 1;
  else if (pt1 == pt2)
    return 0;
  else
    return -1;
}

/// sort leptons
///----------------------------------------------------------------------------
void MCTemplateCorr::sortLeptons(my_lep *lep)
{
  srtLep lep_srt[MAX_LEPT];
  my_lep temp_lep;

  for (int i = 0; i < lep->num_leptons; i++)
  {
    lep_srt[i].ind = i;
    lep_srt[i].pT = lep->pT[i];
  }

  qsort(lep_srt, lep->num_leptons, sizeof(srtLep), lep_pt_comparator);

  for (int i = 0; i < lep->num_leptons; i++)
  {
    temp_lep.is_electron[i] = lep->is_electron[lep_srt[i].ind];
    temp_lep.pT[i] = lep->pT[lep_srt[i].ind];
    temp_lep.recoCharge[i] = lep->recoCharge[lep_srt[i].ind];
    temp_lep.truthType[i] = lep->truthType[lep_srt[i].ind];
    temp_lep.truthFirstMumType[i] = lep->truthFirstMumType[lep_srt[i].ind];
    temp_lep.truthFallBackType[i] = lep->truthFallBackType[lep_srt[i].ind];
    temp_lep.truthOrigin[i] = lep->truthOrigin[lep_srt[i].ind];
    temp_lep.truthFirstMumOrigin[i] = lep->truthFirstMumOrigin[lep_srt[i].ind];
    temp_lep.truthFallBackOrigin[i] = lep->truthFallBackOrigin[lep_srt[i].ind];
    temp_lep.truthPdgId[i] = lep->truthPdgId[lep_srt[i].ind];
    temp_lep.truthFirstMumPdgid[i] = lep->truthFirstMumPdgid[lep_srt[i].ind];
    temp_lep.recoPdgId[i] = lep->recoPdgId[lep_srt[i].ind];
  }

  for (int i = 0; i < lep->num_leptons; i++)
  {
    lep->is_electron[i] = temp_lep.is_electron[i];
    lep->pT[i] = temp_lep.pT[i];
    lep->recoCharge[i] = temp_lep.recoCharge[i];
    lep->truthType[i] = temp_lep.truthType[i];
    lep->truthFirstMumType[i] = temp_lep.truthFirstMumType[i];
    lep->truthFallBackType[i] = temp_lep.truthFallBackType[i];
    lep->truthOrigin[i] = temp_lep.truthOrigin[i];
    lep->truthFirstMumOrigin[i] = temp_lep.truthFirstMumOrigin[i];
    lep->truthFallBackOrigin[i] = temp_lep.truthFallBackOrigin[i];
    lep->truthPdgId[i] = temp_lep.truthPdgId[i];
    lep->truthFirstMumPdgid[i] = temp_lep.truthFirstMumPdgid[i];
    lep->recoPdgId[i] = temp_lep.recoPdgId[i];
  }

  return;
}

/// set the correction factors
///----------------------------------------------------------------------------
void MCTemplateCorr::SetMCResult(int channelnum)
{
  if (debug_me)
    std::cout << "Channel Number = " << channelnum << std::endl;

  float correction[NCorrections];
  float uncertainty[NCorrections];

  for (int i = 0; i < NCorrections; i++)
  {
    correction[i] = 1.0;
    uncertainty[i] = 0.0;
  }

  for (int i = 0; i < NCorrections; i++)
  {
    /// Pythia & Sherpa: ttbar,  ttbar filtered, single top (nominal samples)
    if (channelnum == 410470 || channelnum == 410471 || (407342 <= channelnum && channelnum <= 407347) || channelnum == 410658 || channelnum == 410659 || (410644 <= channelnum && channelnum <= 410645) || (413029 <= channelnum && channelnum <= 413036))
    {
      if (debug_me && i == 0)
        std::cout << "Pythia Setup (even if some of these samples are Sherpa)!" << std::endl;
      correction[i] = pythia_correction[i];
      uncertainty[i] = pythia_uncertainty[i];
    }
    /// Sherpa: 221 Vjets, 2211 Vjets
    else if ((364156 <= channelnum && channelnum <= 364197) || (364100 <= channelnum && channelnum <= 364141) || (308092 <= channelnum && channelnum <= 308098) || (700320 <= channelnum && channelnum <= 700334) || (700338 <= channelnum && channelnum <= 700349))
    {
      if (debug_me && i == 0)
        std::cout << "sherpa Setup!" << std::endl;
      correction[i] = sherpa_correction[i];
      uncertainty[i] = sherpa_uncertainty[i];
    }
    /// Other fakes, Sherpa
    else if (channelnum == 364290 || channelnum == 364254 || channelnum == 366089 || channelnum == 345715 || channelnum == 345718 || channelnum == 345723 || channelnum == 407315)
    {
      if (debug_me && i == 0)
        std::cout << "Sherpa Setup!" << std::endl;
      correction[i] = sherpa_correction[i];
      uncertainty[i] = sherpa_uncertainty[i];
    }
    ///
    else
    {
      std::cout << "MCTemplateCorr::SetMCResult  -->  Unknown MC sample!!!" << std::endl;
    }
  }

  SetMCResult(channelnum, correction, uncertainty);
}

void MCTemplateCorr::SetMCResult(int channelnum, float *corr, float *unc)
{
  /// order: template3_ChFlip template4_LFHFe template5_AllButBm template6_Bm

  chMisId_Correction = corr[0];
  LF_EL_Correction = corr[1];
  HF_EL_Correction = corr[1]; 
  LF_MU_Correction = corr[2];
  HF_MU_Correction = corr[3];

  chMisId_Uncert = unc[0];
  LF_EL_Uncert = unc[1];
  HF_EL_Uncert = unc[1];
  LF_MU_Uncert = unc[2];
  HF_MU_Uncert = unc[3]; 

  return;
}

/// reset everything to 0
///----------------------------------------------------------------------------
void MCTemplateCorr::Reset()
{

  if (debug_me)
    printf("----------------End of event----------------\n\n");

  m_index = 0;
  m_reset = true;

  for (int i = 0; i < MAX_LEPT; i++)
  {
    m_lepinfo.is_fake_HF[i] = false;
    m_lepinfo.is_fake_LF[i] = false;
    m_lepinfo.is_chmisid[i] = false;
  }
}
