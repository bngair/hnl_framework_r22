#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DiLepton/DiLepton.h>
#include "EventLoop/OutputStream.h"

#include <SampleHandler/MetaFields.h>

#include <PathResolver/PathResolver.h>

#include "xAODEventInfo/EventInfo.h"
#include "xAODEventFormat/EventFormat.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBTagging/BTagging.h"
#include "xAODBTagging/BTaggingUtilities.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"

#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "TrigConfxAOD/xAODConfigTool.h"

#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/SystematicSet.h"
// #include "PATInterfaces/SystematicCode.h"
#include "METUtilities/METSystematicsTool.h"
#include "PATCore/PATCoreEnums.h"
#include "AsgTools/AnaToolHandle.h"

#include "LHAPDF/LHAPDF.h"
#include "LHAPDF/Reweighting.h"
#include "MCTruthClassifier/MCTruthClassifier.h"
#include "JetReclustering/JetReclusteringTool.h"

#include "AthContainers/AuxElement.h"
#include "AsgMessaging/StatusCode.h"

#include <iostream>
#include <fstream>
#include <TEnv.h>


#include "AsgTools/ToolStore.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/StandaloneToolHandle.h"
#include "AsgTools/IAsgTool.h"


using namespace xAOD;
using namespace std;
using namespace ST;

ClassImp(DiLepton)

    DiLepton::DiLepton()
    : MCClassifier(0)
{
    /// Here you put any code for the base initialization of variables,
    /// e.g. initialize all pointers to 0.  Note that you should only put
    /// the most basic initialization here, since this method will be
    /// called on both the submission and the worker node.  Most of your
    /// initialization code will go into histInitialize() and
    /// initialize().
}

EL::StatusCode DiLepton::setupJob(EL::Job &job)
{
    job.useXAOD();
    xAOD::Init("DiLepton").ignore();

    EL::OutputStream out("output");
    job.outputAdd(out);

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiLepton::histInitialize()
{
    const char *APP_NAME = "DiLepton::histInitialize()";

    /// Generic Histos
    hCutflow = new TH1F("hCutflow", "hCutflow", 50, -0.5, 49.5);
    hEventsTotal = new TH1F("hEventsTotal", "hEventsTotal", 10, 0, 10);
    Info(APP_NAME, "Created histogram %s", hEventsTotal->GetName());

    return EL::StatusCode::SUCCESS;
}

bool DiLepton::InitializeHistograms()
{
    const char *APP_NAME = "DiLepton::InitializeHistograms()";

    if (!makeHist)
        return true;
    Info(APP_NAME, "Initialize histograms");

    NSys = SysInfoList_K.size();
    NSr = SignalRegions.size();
    Info(APP_NAME, "Size of histogram array (%i, %i) ", (int)NSr, (int)NSys);

    for (unsigned int sr(0); sr < NSr; ++sr)
    {
        TString SRsuf = "_" + (TString)SignalRegions.at(sr).Name;
        for (unsigned int sys(0); sys < NSys; sys++)
        {
            TString SYSsuf = sys ? "_" + (TString)(this->getSysNames(SysInfoList_K)).at(sys) : "";

            hEvents[sr][sys] = new TH1F("hEvents" + SRsuf + SYSsuf, "hEvents" + SRsuf + SYSsuf, 10, 0, 10);
            hEventWeight[sr][sys] = new TH1F("hEventWeight" + SRsuf + SYSsuf, "hEventWeight" + SRsuf + SYSsuf, 100, 0, 1000);
            hEventWeightSF[sr][sys] = new TH1F("hEventWeightSF" + SRsuf + SYSsuf, "hEventWeightSF" + SRsuf + SYSsuf, 100, 0, 1000);

            /// Baseline histos
            BaselineMet[sr][sys] = new TH1F("BaselineMet" + SRsuf + SYSsuf, "BaselineMet" + SRsuf + SYSsuf, 200, 0, 2000000);
            BaselineLepPt[sr][sys] = new TH2F("BaselineLepPt" + SRsuf + SYSsuf, "BaselineLepPt" + SRsuf + SYSsuf, 200, 0, 2000000, 200, 0, 2000000);
            BaselineLepEta[sr][sys] = new TH2F("BaselineLepEta" + SRsuf + SYSsuf, "BaselineLepEta" + SRsuf + SYSsuf, 100, -3.5, 3.5, 100, -3.5, 3.5);

            BaselineJet_Mj[sr][sys] = new TH1F("BaselineJet_Mj" + SRsuf + SYSsuf, "BaselineJet_Mj" + SRsuf + SYSsuf, 200, 0, 1000000);
            BaselineJet_Mjj[sr][sys] = new TH1F("BaselineJet_Mjj" + SRsuf + SYSsuf, "BaselineJet_Mjj" + SRsuf + SYSsuf, 200, 0, 1000000);
            BaselineJet_MjjdR[sr][sys] = new TH1F("BaselineJet_MjjdR" + SRsuf + SYSsuf, "BaselineJet_MjjdR" + SRsuf + SYSsuf, 200, 0, 1000000);
            BaselineJet_MjRec[sr][sys] = new TH1F("BaselineJet_MjRec" + SRsuf + SYSsuf, "BaselineJet_MjRec" + SRsuf + SYSsuf, 200, 0, 1000000);
            BaselineJet_MjAll[sr][sys] = new TH1F("BaselineJet_MjAll" + SRsuf + SYSsuf, "BaselineJet_MjAll" + SRsuf + SYSsuf, 200, 0, 1000000);
            BaselineJet_MjRecAll[sr][sys] = new TH1F("BaselineJet_MjRecAll" + SRsuf + SYSsuf, "BaselineJet_MjRecAll" + SRsuf + SYSsuf, 200, 0, 1000000);

            /// Trigger histos
            TrigMet[sr][sys] = new TH1F("TrigMet" + SRsuf + SYSsuf, "TrigMet" + SRsuf + SYSsuf, 200, 0, 2000000);
            TrigMuPt[sr][sys] = new TH2F("TrigMuPt" + SRsuf + SYSsuf, "TrigMuPt" + SRsuf + SYSsuf, 200, 0, 2000000, 200, 0, 2000000);
            TrigElPt[sr][sys] = new TH2F("TrigElPt" + SRsuf + SYSsuf, "TrigElPt" + SRsuf + SYSsuf, 200, 0, 2000000, 200, 0, 2000000);
            TrigMuEta[sr][sys] = new TH2F("TrigMuEta" + SRsuf + SYSsuf, "TrigMuEta" + SRsuf + SYSsuf, 100, -3.5, 3.5, 100, -3.5, 3.5);
            TrigElEta[sr][sys] = new TH2F("TrigElEta" + SRsuf + SYSsuf, "TrigElEta" + SRsuf + SYSsuf, 100, -3.5, 3.5, 100, -3.5, 3.5);
            TrigJetPt[sr][sys] = new TH2F("TrigJetPt" + SRsuf + SYSsuf, "TrigJetPt" + SRsuf + SYSsuf, 200, 0, 5000000, 200, 0, 5000000);

            /// Signal histos
            NMu[sr][sys] = new TH1F("NMu" + SRsuf + SYSsuf, "NMu" + SRsuf + SYSsuf, 10, 0, 10);
            NMu_EWcombi[sr][sys] = new TH1F("NMu_EWcombi" + SRsuf + SYSsuf, "NMu_EWcombi" + SRsuf + SYSsuf, 10, 0, 10);
            NEl[sr][sys] = new TH1F("NEl" + SRsuf + SYSsuf, "NEl" + SRsuf + SYSsuf, 10, 0, 10);
            NEl_EWcombi[sr][sys] = new TH1F("NEl_EWcombi" + SRsuf + SYSsuf, "NEl_EWcombi" + SRsuf + SYSsuf, 10, 0, 10);
            NJet[sr][sys] = new TH1F("NJet" + SRsuf + SYSsuf, "NJet" + SRsuf + SYSsuf, 20, 0, 20);
            NBJet[sr][sys] = new TH1F("NBJet" + SRsuf + SYSsuf, "NBJet" + SRsuf + SYSsuf, 20, 0, 20);

            Mu_1stPt[sr][sys] = new TH1F("Mu_1stPt" + SRsuf + SYSsuf, "Mu_1stPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            Mu_2ndPt[sr][sys] = new TH1F("Mu_2ndPt" + SRsuf + SYSsuf, "Mu_2ndPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            Mu_3rdPt[sr][sys] = new TH1F("Mu_3rdPt" + SRsuf + SYSsuf, "Mu_3rdPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            El_1stPt[sr][sys] = new TH1F("El_1stPt" + SRsuf + SYSsuf, "El_1stPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            El_2ndPt[sr][sys] = new TH1F("El_2ndPt" + SRsuf + SYSsuf, "El_2ndPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            El_3rdPt[sr][sys] = new TH1F("El_3rdPt" + SRsuf + SYSsuf, "El_3rdPt" + SRsuf + SYSsuf, 200, 0, 2000000);
/*            Lep_1stPt[sr][sys] = new TH1F("Lep_1stPt" + SRsuf + SYSsuf, "Lep_1stPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            Lep_2ndPt[sr][sys] = new TH1F("Lep_2ndPt" + SRsuf + SYSsuf, "Lep_2ndPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            Lep_3rdPt[sr][sys] = new TH1F("Lep_3rdPt" + SRsuf + SYSsuf, "Lep_3rdPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            Jet_1stPt[sr][sys] = new TH1F("Jet_1stPt" + SRsuf + SYSsuf, "Jet_1stPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            Jet_2ndPt[sr][sys] = new TH1F("Jet_2ndPt" + SRsuf + SYSsuf, "Jet_2ndPt" + SRsuf + SYSsuf, 200, 0, 2000000);
  */        Lep_1stPt[sr][sys]  = new TH1F("Lep_1stPt"+SRsuf+SYSsuf,  "Lep_1stPt"+SRsuf+SYSsuf, 200, 0, 1500);
            //Lep_1stPt_bt[sr][sys]  = new TH1F("Lep_1stPt_bt"+SRsuf+SYSsuf,  "Lep_1stPt_bt"+SRsuf+SYSsuf, 200, 0, 1500);
            //Lep_1stPt_at[sr][sys]  = new TH1F("Lep_1stPt_at"+SRsuf+SYSsuf,  "Lep_1stPt_at"+SRsuf+SYSsuf, 200, 0, 1500);
            Lep_2ndPt[sr][sys]  = new TH1F("Lep_2ndPt"+SRsuf+SYSsuf,  "Lep_2ndPt"+SRsuf+SYSsuf, 200, 0, 1500);
            Lep_3rdPt[sr][sys]  = new TH1F("Lep_3rdPt"+SRsuf+SYSsuf,  "Lep_3rdPt"+SRsuf+SYSsuf, 200, 0, 2000000);
            Jet_1stPt[sr][sys]  = new TH1F("Jet_1stPt"+SRsuf+SYSsuf,  "Jet_1stPt"+SRsuf+SYSsuf, 200, 0, 1500);
            Jet_2ndPt[sr][sys]  = new TH1F("Jet_2ndPt"+SRsuf+SYSsuf,  "Jet_2ndPt"+SRsuf+SYSsuf, 200, 0, 1500);
  	    Mu_1stEta[sr][sys] = new TH1F("Mu_1stEta" + SRsuf + SYSsuf, "Mu_1stEta" + SRsuf + SYSsuf, 100, -3.5, 3.5);
            Mu_2ndEta[sr][sys] = new TH1F("Mu_2ndEta" + SRsuf + SYSsuf, "Mu_2ndEta" + SRsuf + SYSsuf, 100, -3.5, 3.5);
            El_1stEta[sr][sys] = new TH1F("El_1stEta" + SRsuf + SYSsuf, "El_1stEta" + SRsuf + SYSsuf, 100, -3.5, 3.5);
            El_2ndEta[sr][sys] = new TH1F("El_2ndEta" + SRsuf + SYSsuf, "El_2ndEta" + SRsuf + SYSsuf, 100, -3.5, 3.5);

	    Jet_1stEta[sr][sys]  = new TH1F("Jet_1stEta"+SRsuf+SYSsuf,  "Jet_1stEta"+SRsuf+SYSsuf, 200, -7, 7);
            Jet_2ndEta[sr][sys]  = new TH1F("Jet_2ndEta"+SRsuf+SYSsuf,  "Jet_2ndEta"+SRsuf+SYSsuf, 200, -7, 7);
         Jet_3thEta[sr][sys]  = new TH1F("Jet_3thEta"+SRsuf+SYSsuf,  "Jet_3thEta"+SRsuf+SYSsuf, 200, -7, 7);
            Jet_4thEta[sr][sys]  = new TH1F("Jet_4thEta"+SRsuf+SYSsuf,  "Jet_4thEta"+SRsuf+SYSsuf, 200, -7, 7);
            Jet_1stPhi[sr][sys]  = new TH1F("Jet_1stPhi"+SRsuf+SYSsuf,  "Jet_1stPhi"+SRsuf+SYSsuf, 200, -7, 7);
            Jet_2ndPhi[sr][sys]  = new TH1F("Jet_2ndPhi"+SRsuf+SYSsuf,  "Jet_2ndPhi"+SRsuf+SYSsuf, 200, -7, 7);
            Jet_mulEta[sr][sys]  = new TH1F("Jet_mulEta"+SRsuf+SYSsuf,  "Jet_mulEta"+SRsuf+SYSsuf, 200, -7, 7);
Jet_diffPhi[sr][sys]  = new TH1F("Jet_diffPhi"+SRsuf+SYSsuf,  "Jet_diffPhi"+SRsuf+SYSsuf, 200, -7, 7);
Jet_diffEta[sr][sys]  = new TH1F("Jet_diffEta"+SRsuf+SYSsuf,  "Jet_diffEta"+SRsuf+SYSsuf, 200, -7, 7);

C3[sr][sys]  = new TH1F("C3"+SRsuf+SYSsuf,  "C3"+SRsuf+SYSsuf, 200, 0, 7);
C4[sr][sys]  = new TH1F("C4"+SRsuf+SYSsuf,  "C4"+SRsuf+SYSsuf, 200, 0, 7);

            Jet_Mj[sr][sys] = new TH1F("Jet_Mj" + SRsuf + SYSsuf, "Jet_Mj" + SRsuf + SYSsuf, 200, 0, 2000000);
            Jet_Mjj[sr][sys] = new TH1F("Jet_Mjj" + SRsuf + SYSsuf, "Jet_Mjj" + SRsuf + SYSsuf, 200, 0, 2000);
Jet_M13[sr][sys]    = new TH1F("Jet_M13"+SRsuf+SYSsuf,   "Jet_M13"+SRsuf+SYSsuf,  200, 0, 2000);
          Jet_M14[sr][sys]    = new TH1F("Jet_M14"+SRsuf+SYSsuf,   "Jet_M14"+SRsuf+SYSsuf,  200, 0, 2000);
           Jet_M23[sr][sys]    = new TH1F("Jet_M23"+SRsuf+SYSsuf,   "Jet_M23"+SRsuf+SYSsuf,  200, 0, 2000);
        Jet_M24[sr][sys]    = new TH1F("Jet_M24"+SRsuf+SYSsuf,   "Jet_M24"+SRsuf+SYSsuf,  200, 0, 2000);
Jet_Mrel3[sr][sys]    = new TH1F("Jet_Mrel3"+SRsuf+SYSsuf,   "Jet_Mrel3"+SRsuf+SYSsuf,  200, 0, 2);
Jet_Mrel4[sr][sys]    = new TH1F("Jet_Mrel4"+SRsuf+SYSsuf,   "Jet_Mrel4"+SRsuf+SYSsuf,  200, 0, 2);
Jet_MjjdR[sr][sys]  = new TH1F("Jet_MjjdR"+SRsuf+SYSsuf, "Jet_MjjdR"+SRsuf+SYSsuf,200, 0, 2000000);
	    //            Jet_MjjdR[sr][sys] = new TH1F("Jet_MjjdR" + SRsuf + SYSsuf, "Jet_MjjdR" + SRsuf + SYSsuf, 200, 0, 2000000);
            Jet_MjRec[sr][sys] = new TH1F("Jet_MjRec" + SRsuf + SYSsuf, "Jet_MjRec" + SRsuf + SYSsuf, 200, 0, 2000000);
            Jet_MjAll[sr][sys] = new TH1F("Jet_MjAll" + SRsuf + SYSsuf, "Jet_MjAll" + SRsuf + SYSsuf, 200, 0, 2000000);
            Jet_MjRecAll[sr][sys] = new TH1F("Jet_MjRecAll" + SRsuf + SYSsuf, "Jet_MjRecAll" + SRsuf + SYSsuf, 200, 0, 2000000);

            ETmiss[sr][sys] = new TH1F("ETmiss" + SRsuf + SYSsuf, "ETmiss" + SRsuf + SYSsuf, 200, 0, 1000);
            Meff[sr][sys] = new TH1F("Meff" + SRsuf + SYSsuf, "Meff" + SRsuf + SYSsuf, 200, 0, 2000);
          Etll[sr][sys]   = new TH1F("Etll"+SRsuf+SYSsuf,  "Etll"+SRsuf+SYSsuf,  200, 0, 1500);
        Mll[sr][sys]   = new TH1F("Mll"+SRsuf+SYSsuf,  "Mll"+SRsuf+SYSsuf,  200, 0, 1500);
Mtn[sr][sys]     = new TH1F("Mtn"+SRsuf+SYSsuf,    "Mtn"+SRsuf+SYSsuf,    200, 0, 1500);

 	    Minv[sr][sys] = new TH1F("Minv" + SRsuf + SYSsuf, "Minv" + SRsuf + SYSsuf, 200, 0, 5000000);
            MT[sr][sys] = new TH1F("MT" + SRsuf + SYSsuf, "MT" + SRsuf + SYSsuf, 200, 0, 5000000);
            HT[sr][sys] = new TH1F("HT" + SRsuf + SYSsuf, "HT" + SRsuf + SYSsuf, 200, 0, 5000000);
 
            Mu_SumPt[sr][sys] = new TH1F("Mu_SumPt" + SRsuf + SYSsuf, "Mu_SumPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            El_SumPt[sr][sys] = new TH1F("El_SumPt" + SRsuf + SYSsuf, "El_SumPt" + SRsuf + SYSsuf, 200, 0, 2000000);
            Jet_SumPt[sr][sys] = new TH1F("Jet_SumPt" + SRsuf + SYSsuf, "Jet_SumPt" + SRsuf + SYSsuf, 200, 0, 2000000);

            /// Add histos to output
            TDirectory *out_TDir = (TDirectory *)wk()->getOutputFile("output");

            hEvents[sr][sys]->SetDirectory(out_TDir);
            hEventWeight[sr][sys]->SetDirectory(out_TDir);
            hEventWeightSF[sr][sys]->SetDirectory(out_TDir);

            BaselineMet[sr][sys]->SetDirectory(out_TDir);
            BaselineLepPt[sr][sys]->SetDirectory(out_TDir);
            BaselineLepEta[sr][sys]->SetDirectory(out_TDir);

            BaselineJet_Mj[sr][sys]->SetDirectory(out_TDir);
            BaselineJet_Mjj[sr][sys]->SetDirectory(out_TDir);
            BaselineJet_MjjdR[sr][sys]->SetDirectory(out_TDir);
            BaselineJet_MjRec[sr][sys]->SetDirectory(out_TDir);
            BaselineJet_MjAll[sr][sys]->SetDirectory(out_TDir);
            BaselineJet_MjRecAll[sr][sys]->SetDirectory(out_TDir);

            TrigMuPt[sr][sys]->SetDirectory(out_TDir);
            TrigElPt[sr][sys]->SetDirectory(out_TDir);
            TrigMuEta[sr][sys]->SetDirectory(out_TDir);
            TrigElEta[sr][sys]->SetDirectory(out_TDir);
            TrigJetPt[sr][sys]->SetDirectory(out_TDir);
            TrigMet[sr][sys]->SetDirectory(out_TDir);

            NMu[sr][sys]->SetDirectory(out_TDir);
            NMu_EWcombi[sr][sys]->SetDirectory(out_TDir);
            NEl[sr][sys]->SetDirectory(out_TDir);
            NEl_EWcombi[sr][sys]->SetDirectory(out_TDir);
            NJet[sr][sys]->SetDirectory(out_TDir);
            NBJet[sr][sys]->SetDirectory(out_TDir);

            Mu_1stPt[sr][sys]->SetDirectory(out_TDir);
            Mu_2ndPt[sr][sys]->SetDirectory(out_TDir);
            Mu_3rdPt[sr][sys]->SetDirectory(out_TDir);
            El_1stPt[sr][sys]->SetDirectory(out_TDir);
            El_2ndPt[sr][sys]->SetDirectory(out_TDir);
            El_3rdPt[sr][sys]->SetDirectory(out_TDir);
            Lep_1stPt[sr][sys]->SetDirectory(out_TDir);
            Lep_2ndPt[sr][sys]->SetDirectory(out_TDir);
            Lep_3rdPt[sr][sys]->SetDirectory(out_TDir);
            Jet_1stPt[sr][sys]->SetDirectory(out_TDir);
            Jet_2ndPt[sr][sys]->SetDirectory(out_TDir);
            Mu_1stEta[sr][sys]->SetDirectory(out_TDir);
            Mu_2ndEta[sr][sys]->SetDirectory(out_TDir);
            El_1stEta[sr][sys]->SetDirectory(out_TDir);
            El_2ndEta[sr][sys]->SetDirectory(out_TDir);

            Jet_Mj[sr][sys]->SetDirectory(out_TDir);
            Jet_Mjj[sr][sys]->SetDirectory(out_TDir);
Jet_M13[sr][sys]->SetDirectory(out_TDir);
Jet_M23[sr][sys]->SetDirectory(out_TDir);
Jet_M14[sr][sys]->SetDirectory(out_TDir);
Jet_M24[sr][sys]->SetDirectory(out_TDir);
Jet_MjjdR[sr][sys]->SetDirectory(out_TDir);
Jet_Mrel4[sr][sys]->SetDirectory(out_TDir);
Jet_Mrel3[sr][sys]->SetDirectory(out_TDir);
Jet_MjRec[sr][sys]->SetDirectory(out_TDir);
Etll[sr][sys]->SetDirectory(out_TDir);
Mll[sr][sys]->SetDirectory(out_TDir);
Mtn[sr][sys]->SetDirectory(out_TDir);
	    Jet_MjjdR[sr][sys]->SetDirectory(out_TDir);
            Jet_MjRec[sr][sys]->SetDirectory(out_TDir);
            Jet_MjAll[sr][sys]->SetDirectory(out_TDir);
            Jet_MjRecAll[sr][sys]->SetDirectory(out_TDir);
Jet_1stEta[sr][sys]->SetDirectory(out_TDir);
 C3[sr][sys]->SetDirectory(out_TDir);
C4[sr][sys]->SetDirectory(out_TDir);
  Jet_mulEta[sr][sys]->SetDirectory(out_TDir);
            Jet_2ndEta[sr][sys]->SetDirectory(out_TDir);
            Jet_1stPhi[sr][sys]->SetDirectory(out_TDir);
            Jet_diffPhi[sr][sys]->SetDirectory(out_TDir);
            Jet_diffEta[sr][sys]->SetDirectory(out_TDir);
            Jet_2ndPhi[sr][sys]->SetDirectory(out_TDir);
//Jet_diffPhi[sr][sys]->SetDirectory(out_TDir);
Jet_4thEta[sr][sys]->SetDirectory(out_TDir);
            Jet_3thEta[sr][sys]->SetDirectory(out_TDir);

            Meff[sr][sys]->SetDirectory(out_TDir);
            ETmiss[sr][sys]->SetDirectory(out_TDir);
            Minv[sr][sys]->SetDirectory(out_TDir);
            MT[sr][sys]->SetDirectory(out_TDir);
            HT[sr][sys]->SetDirectory(out_TDir);

            Mu_SumPt[sr][sys]->SetDirectory(out_TDir);
            El_SumPt[sr][sys]->SetDirectory(out_TDir);
            Jet_SumPt[sr][sys]->SetDirectory(out_TDir);
        }
    }
    Info(APP_NAME, "Initialized histograms successfully");

    return true;
}

bool DiLepton::InitializeTree()
{
    const char *APP_NAME = "DiLepton::InitializeTree()";

    if (!makeTree)
        return true;
    Info(APP_NAME, "Initialize TTree");

    TFile *file = wk()->getOutputFile("output");
    if (file)
        Info("Created file", file->GetName());

    ControlTree = new TTree("ControlTree", "ControlTree");
    Info(APP_NAME, "Created TTree %s", ControlTree->GetName());
    ControlTree->SetDirectory(file);
    ControlTree->Branch("EventNumber", &EventNumber, "EventNumber/L");
    ControlTree->Branch("RunNumber", &RunNumber, "RunNumber/L");
    ControlTree->Branch("IntPerX_CorrectedScaled", &IntPerX_CorrectedScaled);
    ControlTree->Branch("LumiBlock", &LB);
    ControlTree->Branch("MCId", &MCId);
    ControlTree->Branch("MCWeight", &EventWeight);
    ControlTree->Branch("NFiles", &NFiles);
    ControlTree->Branch("FileEntry", &m_fileentry);
    ControlTree->Branch("xAODEventSum", &xAODEventSum);
    ControlTree->Branch("xAODWeightSum", &xAODWeightSum);
    ControlTree->Branch("GLDec1", &GLDec1);
    ControlTree->Branch("GLDec2", &GLDec2);

    NSys = SysInfoList_K.size();
    NSr = SignalRegions.size();
    Info(APP_NAME, "Size of TTree array (%i, %i) ", (int)NSr, (int)NSys);
    setROOTmsg(0);

    for (unsigned int sr(0); sr < NSr; ++sr)
    {
        TString SRsuf = "_" + (TString)SignalRegions.at(sr).Name;
        for (unsigned int sys(0); sys < NSys; sys++)
        {
            TString SYSsuf = sys ? "_" + (TString)(this->getSysNames(SysInfoList_K)).at(sys) : "";

            DiLeptonTree[sr][sys] = new TTree("DiLeptonTree" + SRsuf + SYSsuf, "DiLeptonTree" + SRsuf + SYSsuf);
            std::cout << APP_NAME << "\t Created TTree " << DiLeptonTree[sr][sys]->GetName() << std::endl;
            DiLeptonTree[sr][sys]->SetDirectory(file);
            DiLeptonTree[sr][sys]->Branch("EventNumber", &EventNumber, "EventNumber/L");
            DiLeptonTree[sr][sys]->Branch("RunNumber", &RunNumber, "RunNumber/L");
            DiLeptonTree[sr][sys]->Branch("RandomRN", &randomRN);
            DiLeptonTree[sr][sys]->Branch("MCId", &MCId);
            DiLeptonTree[sr][sys]->Branch("MCWeight", &EventWeight);
            DiLeptonTree[sr][sys]->Branch("PileUpWeight", &PileUpWeight);
            DiLeptonTree[sr][sys]->Branch("ObjectSF", &ObjectSF);

            DiLeptonTree[sr][sys]->Branch("TriggerSF", &triggerSF);
            DiLeptonTree[sr][sys]->Branch("FakeLepMCTemplateWeight", &FakeLepMCTemplateWeight);
            DiLeptonTree[sr][sys]->Branch("FakeMuMCTemplateUnc", &FakeMuMCTemplateUnc);
            DiLeptonTree[sr][sys]->Branch("FakeEleMCTemplateUnc", &FakeEleMCTemplateUnc);
            DiLeptonTree[sr][sys]->Branch("TotalWeight", &weight);
            DiLeptonTree[sr][sys]->Branch("EtMiss", &EtMiss);
            DiLeptonTree[sr][sys]->Branch("EtMissPhi", &EtMissPhi);
              DiLeptonTree[sr][sys]->Branch("mll",       &mll);
            DiLeptonTree[sr][sys]->Branch("lep_2ndPt",       &lep_2ndPt);
            DiLeptonTree[sr][sys]->Branch("lep_1stPt",       &lep_1stPt);
            //DiLeptonTree[sr][sys]->Branch("lep_1stPt_at",       &lep_1stPt_at);
            DiLeptonTree[sr][sys]->Branch("lep_2ndEta",       &lep_2ndEta);
            DiLeptonTree[sr][sys]->Branch("lep_1stEta",       &lep_1stEta);
            DiLeptonTree[sr][sys]->Branch("lep_2ndPhi",       &lep_2ndPhi);
            DiLeptonTree[sr][sys]->Branch("lep_1stPhi",       &lep_1stPhi);
           // DiLeptonTree[sr][sys]->Branch("mjj",       &mjj);

DiLeptonTree[sr][sys]->Branch("jet_mulEta",       &jet_mulEta);
DiLeptonTree[sr][sys]->Branch("jet_diffPhi",       &jet_diffPhi);
DiLeptonTree[sr][sys]->Branch("jet_diffEta",       &jet_diffEta);
DiLeptonTree[sr][sys]->Branch("Njet25x",       &Njet25x);
DiLeptonTree[sr][sys]->Branch("mjj",       &mjj);

                    DiLeptonTree[sr][sys]->Branch("jet_2ndPt",       &jet_2ndPt);
                    DiLeptonTree[sr][sys]->Branch("jet_1stPt",       &jet_1stPt);
                    DiLeptonTree[sr][sys]->Branch("jet_2ndEta",       &jet_2ndEta);
                    DiLeptonTree[sr][sys]->Branch("jet_1stEta",       &jet_1stEta);
                    DiLeptonTree[sr][sys]->Branch("jet_2ndPhi",       &jet_2ndPhi);
                    DiLeptonTree[sr][sys]->Branch("jet_1stPhi",       &jet_1stPhi);
	    DiLeptonTree[sr][sys]->Branch("EtMissSig", &EtMissSig);
            DiLeptonTree[sr][sys]->Branch("NlepBL", &NlepBL);
            DiLeptonTree[sr][sys]->Branch("mu_n_EWcombi", &mu_n_EWcombi);
            DiLeptonTree[sr][sys]->Branch("el_n_EWcombi", &el_n_EWcombi);

            DiLeptonTree[sr][sys]->Branch("jetBtag", &jetBtag);
            DiLeptonTree[sr][sys]->Branch("jetTLV", &jetTLV);
            DiLeptonTree[sr][sys]->Branch("lepTLV", &lepTLV);
            DiLeptonTree[sr][sys]->Branch("lepCharges", &lepCharges);
            DiLeptonTree[sr][sys]->Branch("lepTruth", &lepTruth);
            DiLeptonTree[sr][sys]->Branch("EventTM", &EventTM);
            DiLeptonTree[sr][sys]->Branch("SSChannel", &SSChannel);
            DiLeptonTree[sr][sys]->Branch("SysNames", &SysNames);
            DiLeptonTree[sr][sys]->Branch("SysWeights", &SysWeights);
            /// DiLeptonTree[sr][sys]->Branch("triggerInfo",  &triggerInfo);
            DiLeptonTree[sr][sys]->Branch("GLDec1", &GLDec1);
            DiLeptonTree[sr][sys]->Branch("GLDec2", &GLDec2);
            DiLeptonTree[sr][sys]->Branch("IntPerX_CorrectedScaled", &IntPerX_CorrectedScaled);
            /// DiLeptonTree[sr][sys]->Branch("jetDL1r",      &jetDL1r);
            /// DiLeptonTree[sr][sys]->Branch("jetJVT",       &jetJVT);

            if (makeTree > 1)
            {
                DiLeptonTree[sr][sys]->Branch("IntPerX_CorrectedScaled", &IntPerX_CorrectedScaled);
                DiLeptonTree[sr][sys]->Branch("topoStr", &topoStr);
                DiLeptonTree[sr][sys]->Branch("muTLV", &muTLV);
                DiLeptonTree[sr][sys]->Branch("elTLV", &elTLV);
                DiLeptonTree[sr][sys]->Branch("jetTLV_R1", &jetTLV_R1);
                DiLeptonTree[sr][sys]->Branch("jetD12_R1", &jetD12_R1);
                DiLeptonTree[sr][sys]->Branch("muCharges", &muCharges);
                DiLeptonTree[sr][sys]->Branch("elCharges", &elCharges);
                DiLeptonTree[sr][sys]->Branch("muSF", &muSF);
                DiLeptonTree[sr][sys]->Branch("elSF", &elSF);
                DiLeptonTree[sr][sys]->Branch("btagSFCentral", &m_btagSFCentral);
                DiLeptonTree[sr][sys]->Branch("jvtSF", &m_jvtSF);
                DiLeptonTree[sr][sys]->Branch("jetDL1r", &jetDL1r);
                DiLeptonTree[sr][sys]->Branch("jetJVT", &jetJVT);
            }
        }
    }

    setROOTmsg(1);
    Info(APP_NAME, "Initialized TTree successfully");
   /* std::cout << "Create cutflow txt file" << std::endl;
    cutflow_file_badmu.open("list_events_badmu.txt");
    cutflow_file_pvt.open("list_events_pvt.txt");
    cutflow_file_jetOR.open("list_events_jetOR.txt");
    cutflow_file_badjet.open("list_events_badjet.txt");
    cutflow_file_sjet.open("list_events_sjet.txt");
    cutflow_file_grl.open("list_events_grl.txt");
    cutflow_file_flags.open("list_events_flags.txt");
    cutflow_file_trig.open("list_events_trig.txt");
    cutflow_file_baseline.open("list_events_baseline.txt");
    cutflow_file_all.open("list_events_sig_all.txt");
    cutflow_file_EWcombi.open("list_events_combiEWlep.txt");
    cutflow_file_SS.open("list_events_sig_SS.txt");
    cutflow_file_ee.open("list_events_sig_ee.txt");
    cutflow_file_emu.open("list_events_sig_emu.txt");
    cutflow_file_mumu.open("list_events_sig_mumu.txt");
    cutflow_file_ee_bjet.open("list_events_sig_ee_bjet.txt");
    cutflow_file_emu_bjet.open("list_events_sig_emu_bjet.txt");
    cutflow_file_mumu_bjet.open("list_events_sig_mumu_bjet.txt");
    cutflow_file_ee_jet.open("list_events_sig_ee_jet.txt");
    cutflow_file_emu_jet.open("list_events_sig_emu_jet.txt");
    cutflow_file_mumu_jet.open("list_events_sig_mumu_jet.txt");
    cutflow_file_ee_MET.open("list_events_sig_ee_MET.txt");
    cutflow_file_emu_MET.open("list_events_sig_emu_MET.txt");
    cutflow_file_mumu_MET.open("list_events_sig_mumu_MET.txt");
   */
    return true;
}

EL::StatusCode DiLepton::fileExecute()
{
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiLepton::changeInput(bool firstFile)
{
    const char *APP_NAME = "DiLepton::changeInput()";
    m_event = wk()->xaodEvent();
    m_fileentry = 0;

    int sumEvents(0);
    float sumWeights(0), sumWeightsSq(0);
    xAODEventSum = 0, xAODWeightSum = 0;
    if (firstFile)
        NFiles = 0;

    Info(APP_NAME, "Number of files processed = %i", (int)NFiles + 1);
    Info(APP_NAME, "Number of events in this file = %11i", (int)m_event->getEntries());

    /// Retrive total events numbers of original xAOD
    Info(APP_NAME, "Retrieve MetaData");
    const xAOD::CutBookkeeperContainer *completeCBC(0);
    if (!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess())
    {
        Error(APP_NAME, "Failed to retrieve CutBookkeepers from MetaData");
        return EL::StatusCode::SUCCESS;
    }
    const xAOD::CutBookkeeper *allEventsCBK(0);
    int maxCycle(-1);
    for (auto cbk : *completeCBC)
    {
        if (cbk->inputStream() == "StreamAOD" && cbk->name() == "AllExecutedEvents" && cbk->cycle() > maxCycle)
        {
            maxCycle = cbk->cycle();
            allEventsCBK = cbk;
        }
    }
    if (!allEventsCBK)
    {
        Error(APP_NAME, "Failed to retrieve CutBookkeeper for AllExecutedEvents");
        return EL::StatusCode::SUCCESS;
    }

    sumEvents = (int)allEventsCBK->nAcceptedEvents();
    sumWeights = (float)allEventsCBK->sumOfEventWeights();
    sumWeightsSq = (float)allEventsCBK->sumOfEventWeightsSquared();

    Info(APP_NAME, "CutBookkeeper::Events in original xAOD File %i, sumWeights %.1f, sumWeightsSq %.1f", sumEvents, sumWeights, sumWeightsSq);
    xAODEventSum = sumEvents;
    xAODWeightSum = sumWeights;

    hEventsTotal->Fill(0., sumEvents);
    hEventsTotal->Fill(1., sumWeights);
    hEventsTotal->Fill(2., sumWeightsSq);

    NFiles++;

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiLepton::initialize()
{
    if (ccCheck)
    {
        CP::CorrectionCode::enableFailure();
        StatusCode::enableFailure();
        // CP::SystematicCode::enableFailure();
    }
    xAOD::TFileAccessTracer::enableDataSubmission(false);


    const char *APP_NAME = "DiLepton::initialize()";
    m_event = wk()->xaodEvent();
    //  store = *(wk()->xaodStore());
    // xAOD::TStore store;
    m_entry = 0;

    const xAOD::EventInfo *evtInfo(0);
    ATH_CHECK(evtStore()->retrieve(evtInfo, "EventInfo"));


  ATH_MSG_INFO( "Initialising... " );


  bool autoconf(false);
/*
/////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef XAOD_STANDALONE // For now metadata is Athena-only
  if ( m_dataSource < 0 ) {
    autoconf = true;
    ATH_MSG_INFO("Autoconfiguring: dataSource, mcCampaign, isPHYSLITE");
    std::string projectName = "";
    ATH_CHECK( AthAnalysisHelper::retrieveMetadata("/TagInfo", "project_name", projectName, inputMetaStore() ) );
    if ( projectName == "IS_SIMULATION" ) {
        std::string simFlavour = "";
        ATH_CHECK( AthAnalysisHelper::retrieveMetadata("/Simulation/Parameters", "SimulationFlavour", simFlavour, inputMetaStore() ) );
        TString s(simFlavour); s.ToUpper();
        m_dataSource = s.Contains("ATLFAST") ? AtlfastII : FullSim;
    } else if (projectName.compare(0, 4, "data") == 0 ) {
        m_dataSource = Data;
    } else {
      ATH_MSG_ERROR("Failed to autoconfigure -- project_name matches neither IS_SIMULATION nor data!");
      return StatusCode::FAILURE;
    }

  }
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////
*/  

  const xAOD::EventInfo *eventInfo(0);
  
  if (!m_event->retrieve(eventInfo, "EventInfo").isSuccess())
    {
        Error(APP_NAME, "Failed to retrieve event info collection. Exiting.");
        return EL::StatusCode::FAILURE;
    }

    /// Judge AFII or Full simulation
    isAtlfast = 0;
    isAtlfast2 = 0;
    isAtlfast3 = 0;
    std::string samplename = "";
   
    const xAOD::FileMetaData* fmd = nullptr;
    std::string simFlavour = "";
    std::string dsid = "";
    std::string amiTag = "";

    if (!isData)
    {
/*
//////////////////////////////check conditions////////////////////////////////
#ifndef XAOD_STANDALONE 
    if ( inputMetaStore()->contains<xAOD::FileMetaData>("FileMetaData") && inputMetaStore()->retrieve(fmd,"FileMetaData").isSuccess() )         {
      fmd->value(xAOD::FileMetaData::mcProcID, dsid);
      fmd->value(xAOD::FileMetaData::amiTag, amiTag);
      fmd->value(xAOD::FileMetaData::simFlavour, simFlavour);

      if(simFlavour.find("ATLFASTII")==0) simType = "AFII";
      else if(simFlavour.find("ATLFAST3")==0) simType = "AF3";
      else simType = "FS";
std::cout<<"dsidTest----------------------------:"<<dsid<<std::endl;
std::cout<<"amiTagTest----------------------------:"<<amiTag<<std::endl;
std::cout<<"simFlavourTest----------------------------:"<<simFlavour<<std::endl;
    } 

#endif
//////////////////////////////////////////////////////////////

std::cout<<"dsidTest----------out----------------:"<<dsid<<std::endl;
std::cout<<"amiTagTest--------out-----------------:"<<amiTag<<std::endl;
std::cout<<"simFlavourTest----out---------------------:"<<simFlavour<<std::endl;

*/

//////////////////////////////////////////////buggy metadata info/////////////////////////////////////////

    asg::AsgMetadataTool ATMetaData("OurNewMetaDataObject");
    if (ATMetaData.inputMetaStore()->contains<xAOD::FileMetaData>("FileMetaData")  && ATMetaData.inputMetaStore()->retrieve(fmd,"FileMetaData").isSuccess() ) 
{  
    //readMetaData(FMD);
      //fmd->value(xAOD::FileMetaData::mcProcID, dsid);
      fmd->value(xAOD::FileMetaData::amiTag, amiTag);
      fmd->value(xAOD::FileMetaData::simFlavour, simFlavour);	
}

	std::cout<<"dsidTest----------------------------:"<<dsid<<std::endl;
	std::cout<<"amiTagTest----------------------------:"<<amiTag<<std::endl;
	std::cout<<"simFlavourTest----------------------------:"<<simFlavour<<std::endl;

        std::string gridName = wk()->metaData()->castString(SH::MetaFields::gridName);
        std::cout << "GridName: " << gridName << std::endl;
        if (!gridName.empty())
        { /// Do not trust sample name on the grid, in case there are multiple samples per job
            std::stringstream ss(gridName);
            while (std::getline(ss, samplename, ','))
            {
                std::string s = samplename;
                std::string delimiter = ".";
                size_t pos = 0;
                std::string token;
                while ((pos = s.find(delimiter)) != std::string::npos)
                {
                    token = s.substr(0, pos);
                    s.erase(0, pos + delimiter.length());
                    if (std::to_string(eventInfo->mcChannelNumber()) == token)
                    {
                        std::regex rex("_a\\d{3,4}_");
                        std::smatch m;
                        std::regex_search(samplename, m, rex);
                        for (auto x : m)
                        {
                            if (x.length() > 0)
                                isAtlfast = 1;
                        }
                        std::cout << "Sample name: " << samplename << std::endl;
                        break;
                    }
                }
            }
        }
        else /// Use sample name when running locally
        {
            samplename = wk()->metaData()->castString(SH::MetaFields::sampleName);
            std::regex rex("_a\\d{3,4}_");
            std::smatch m;
	    std::cout << "SampleNameTest: " << samplename << std::endl;
            std::regex_search(samplename, m, rex);
            for (auto x : m)
            {
                if (x.length() > 0)
                    isAtlfast = 1;
            }
        }
    }
    else
    {
        samplename = wk()->metaData()->castString(SH::MetaFields::sampleName);
    }

    isGGMHinoZh = 0;
    if (samplename.find("GGMHinoZh") != std::string::npos)
    {
        isGGMHinoZh = 1;
    }

    Info(APP_NAME, "Starting initialize");
    Info(APP_NAME, "Sample name       = %s", samplename.c_str());
    Info(APP_NAME, "Debug messages    = %i", (int)Debug);
    //Info(APP_NAME, "isAtlfast         = %i", (int)isAtlfast);
    Info(APP_NAME, "Create Histos     = %i", (int)makeHist);
    Info(APP_NAME, "Create TTree      = %i", (int)makeTree);
    Info(APP_NAME, "Run systematics   = %i", (int)doSys);
    Info(APP_NAME, "Add the fake lep corrections obtained with the MCTemplate method  = %i", (int)isFakeMC);
    Info(APP_NAME, "Set leptons       = %i", (int)setLeptons);
    Info(APP_NAME, "N signal leptons  = %i", (int)NSignalLeptons);
    Info(APP_NAME, "Do b-Jet veto     = %i", (int)doBJetVeto);
    Info(APP_NAME, "Process SR        = %s", SigRegion.c_str());
    Info(APP_NAME, "Sys. Variations   = %s", SysList.c_str());
    std::cout << std::endl;

    /// Initialize SUSY Tools
    Info(APP_NAME, "Initializing Tool: \t %s", "SUSYObjDef_xAOD");
    switch ((int)Debug)
    {
    case 1:
        SusyObjTool.setProperty("OutputLevel", MSG::ERROR);
        break;
    case 2:
        SusyObjTool.setProperty("OutputLevel", MSG::DEBUG);
        break;
    case 3:
        SusyObjTool.setProperty("OutputLevel", MSG::VERBOSE);
        break;
    default:
        SusyObjTool.setProperty("OutputLevel", MSG::ERROR);
        break;
    }
 
    /// Set PRW config
    std::vector<std::string> PileUpLumiCalc, confFilesV;
//std::string prwPath = "dev/PileupReweighting/share/";

    
    std::string prwPath = "/eos/user/b/bngair/hnl_framework_r22/";    
/// std::string prwPath = PathResolverFindCalibDirectory("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/share/");
    if (prwPath == "")
    {
        std::cout << "get_auto_configured_prw    "
                     "ERROR Could not locate PRW_AUTOCONFIG location (="
                  << prwPath << std::endl;
        exit(1);
    }

    uint32_t RunNb = eventInfo->runNumber(); /// to select year
    if (!isData)
    {
        std::cout << "RunNb = " << RunNb << ", mcChannelNumber = " << eventInfo->mcChannelNumber() << std::endl;

        std::string confFiles, confFilesSuperSample;
        //       std::string confFiles_pileup_mc16X_dsid = (RunNb==284500)?"pileup_mc16a_dsid":(RunNb==300000)?"pileup_mc16d_dsid":(RunNb==310000)?"pileup_mc16e_dsid":"";
        std::string confFiles_pileup_mc20X_dsid = (RunNb == 284500) ? "pileup_mc20a_dsid" : (RunNb == 300000) ? "pileup_mc20d_dsid"
                                                                                        : (RunNb == 310000)   ? "pileup_mc20e_dsid"
                                                                                        : (RunNb == 410000)   ? "pileup_mc21a_dsid"
                                                                                                              : "";
        ;
        std::string isSimType = "";
      
 if(simFlavour.find("FullG4")==0)
        isSimType = "_FS";
 /* else
{
	if(        isAtlfast == 1)
        isSimType = "_AF3";
	else{
	isSimType = "_FS";
	}
  }
*/

else if (simFlavour.find("ATLFAST3_QS")==0)
	isSimType = "_AF3";

 
/*
	 if (isAtlfast && isAtlfast2)
            isSimType = "_AFII";
	else if(isAtlfast)
	    isSimType = "_AF3";
        else
            isSimType = "_FS";
*/

        //confFiles += prwPath+"DSID" + std::to_string(eventInfo->mcChannelNumber()/1000) + "xxx/" + confFiles_pileup_mc16X_dsid + std::to_string(eventInfo->mcChannelNumber()) +
        confFiles += prwPath + "DSID" + std::to_string(eventInfo->mcChannelNumber() / 1000) + "xxx/" + confFiles_pileup_mc20X_dsid + std::to_string(eventInfo->mcChannelNumber()) + isSimType + ".root";
        confFiles = PathResolverFindCalibFile(confFiles);
        TFile testF(confFiles.c_str(), "read");
        if (testF.IsZombie())
        {
            ANA_MSG_ERROR("getPRWConfigFile(): file not found -> " << confFiles << ", trying AFII in case is a SUSY supersample file");
            if (samplename.find("supersample") != std::string::npos)
            {
                isAtlfast = true; /// if it reaches this point, check if the sample is indeed AFII
                isSimType = "_AFII";
                //confFilesSuperSample +=  prwPath+"DSID" + std::to_string(eventInfo->mcChannelNumber()/1000) + "xxx/" + confFiles_pileup_mc16X_dsid +
                confFilesSuperSample += prwPath + "DSID" + std::to_string(eventInfo->mcChannelNumber() / 1000) + "xxx/" + confFiles_pileup_mc20X_dsid + std::to_string(eventInfo->mcChannelNumber()) + isSimType + ".root";
                ;
                confFilesSuperSample = PathResolverFindCalibFile(confFilesSuperSample);
                TFile testSSF(confFilesSuperSample.c_str(), "read");

                if (testSSF.IsZombie())
                {
                    ANA_MSG_ERROR("getPRWConfigFile(): file still not found (running on SUSY supersample) -> " << confFilesSuperSample);
                    return EL::StatusCode::FAILURE;
                }
                else
                {
                    confFilesV.push_back(confFilesSuperSample);
                    ANA_MSG_INFO("getPRWConfigFile(): setting PRW conf to " << confFilesSuperSample);
                }
            }
            else
            {
                ANA_MSG_ERROR("getPRWConfigFile(): file not found!");
                return EL::StatusCode::FAILURE;
            }
        }
        else
        {
            confFilesV.push_back(confFiles);
            ANA_MSG_INFO("getPRWConfigFile(): setting PRW conf to " << confFiles);
        }

        if (RunNb == 284500)
        {
            std::cout << "!!! run on mc20a !" << std::endl
                      << std::endl
                      << std::endl
                      << std::endl;
            ;

            PileUpLumiCalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root")); /// 2016 data

            PileUpLumiCalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root")); /// 2015
        }
        else if (RunNb == 300000)
        {
            std::cout << "!!! run on mc20d!" << std::endl
                      << std::endl
                      << std::endl
                      << std::endl;
            ;
            PileUpLumiCalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root")); // 2017 data

            confFilesV.push_back(PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"));
        }
        else if (RunNb == 310000)
        {
            std::cout << "!!! run on mc20e!" << std::endl
                      << std::endl
                      << std::endl
                      << std::endl;
            ;
            PileUpLumiCalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root")); // 2018 data
            confFilesV.push_back(PathResolverFindCalibFile("GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"));
        }
        else if (RunNb == 410000)
        {
            std::cout << "!!! run on mc20e!" << std::endl
                      << std::endl
                      << std::endl
                      << std::endl;
            ;
            PileUpLumiCalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data22_13p6TeV/20230207/ilumicalc_histograms_None_427405-428580_OflLumi-Run3-003.root")); /// 2023 data
        }
        else
        {
            std::cout << "ERROR!!  Cannot take the files for the PRW!! exiting..." << std::endl;
            return EL::StatusCode::FAILURE;
        }

        ANA_CHECK(SusyObjTool.setProperty("PRWConfigFiles", confFilesV)); /// remove if using AutoconfigurePRWTool
                                                                          // ANA_CHECK( SusyObjTool.setProperty("PRWLumiCalcFiles", PileUpLumiCalc) );
    }
    else if (isData) /// to take the corrected mu value in data
    {
        std::cout << "!!! run on Data" << std::endl
                  << std::endl
                  << std::endl
                  << std::endl;
        ;
        PileUpLumiCalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root")); /// 2015 data
        PileUpLumiCalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root")); /// 2016 data
        PileUpLumiCalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"));     /// 2017 data
        PileUpLumiCalc.push_back(PathResolverFindCalibFile("GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"));      /// 2018 data
        confFilesV.push_back(PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"));         /// 2017 data
        confFilesV.push_back(PathResolverFindCalibFile("GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"));         /// 2018 data
        ANA_CHECK(SusyObjTool.setProperty("PRWConfigFiles", confFilesV));
    }

    /// take shower type
    int ShowerType = 0;
    if (!isData)
    {
        ShowerType = ST::getMCShowerType(samplename);
    }
    std::cout << "ShowerType = " << ShowerType << std::endl;

    /// get other SUSYTools configurations
    ST::ISUSYObjDef_xAODTool::DataSource DataSource = (isData ? ST::ISUSYObjDef_xAODTool::Data : (isAtlfast ? ST::ISUSYObjDef_xAODTool::AtlfastII : ST::ISUSYObjDef_xAODTool::FullSim));
    std::cout << "DataSource = " << DataSource << std::endl;
    /// set SusyObjTool properties
    // std::string ConfigFile = PathResolverFindCalibFile("DiLepton/SS3L_Default.conf");
    std::string ConfigFile;
    if (isPHYSLITE == 0)
        ConfigFile = PathResolverFindCalibFile("DiLepton/SS3L_Default.conf");
    else
        ConfigFile = PathResolverFindCalibFile("DiLepton/SS3L_Default_LITE.conf");

    ANA_CHECK(SusyObjTool.setProperty("DataSource", DataSource));
    ANA_CHECK(SusyObjTool.setProperty("ConfigFile", ConfigFile));
    ANA_CHECK(SusyObjTool.setProperty("ShowerType", ShowerType));
    ANA_CHECK(SusyObjTool.setProperty("DebugMode", (Debug > 1)));
    ANA_CHECK(SusyObjTool.setProperty("PRWLumiCalcFiles", PileUpLumiCalc));
    // ANA_CHECK( SusyObjTool.setProperty("AutoconfigurePRWTool", true ) );
////////////////////////////////////////////////////
//
//here read our file




   std::string HiggsConfigFile;
   HiggsConfigFile = PathResolverFindCalibFile("DiLepton/Higgs_Default.conf");
   std::ifstream input_higgs_file( HiggsConfigFile );       
 
   TEnv rEnv;
   if ( rEnv.ReadFile(HiggsConfigFile.c_str(), kEnvAll) != 0 ) {
      ANA_MSG_ERROR( "Cannot open config file, exiting.");
      return 1;
    }
    ANA_MSG_INFO( "Config file opened" );

    //std::map<std::string,std::string> config_period = {};
    //config_period["2016-1"] = rEnv.GetValue("2016-1", "AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets" );
    //std::cout << "map:" <<   configDict["Jet.LargeRcollection"]  << std::endl;




///////triggere SF////////////////////


std::map<std::string, std::string> triggers={};
std::string trig_key = "triggerSFmap";
std::vector<std::string> theTriggerPeriods;


std::cout << "calefisier" << HiggsConfigFile << std::endl;

    for( std::string line; getline( input_higgs_file, line ); ){
    if (line.find(trig_key) != std::string::npos){
     std::cout << "linietext: " << line << std::endl;
     theTriggerPeriods= split_elements(line,"\\");   
     triggers[theTriggerPeriods[1]]=theTriggerPeriods[2];
     std::cout << "Trigere lista "<< triggers[theTriggerPeriods[1]] << std::endl;
     }
    else {
    std::cout << "linietext not found" << line << std::endl;
    } 
    }

std::cout << "TRIGERE"  << std::endl;
for (int i=0; i<theTriggerPeriods.size(); i++){
   std::cout << "Trigere 2016 "<< theTriggerPeriods[i] << std::endl;
}



std::cout << "Trigere lista generata "<< std::endl;
for(const auto& elem : triggers)
{
   std::cout << elem.first << " " << elem.second << " " << "\n";
}
////////////////////////////////////////////////////


 input_higgs_file.clear();
 input_higgs_file.seekg(0,ios::beg);

 std::map<std::string,std::string> electronTriggerSFMapMixedLepton1={};
 std::string trig_key1 = "triggersMapMix";
 std::vector<std::string> theMapMixPeriods;
 
 for( std::string line; getline( input_higgs_file, line ); ){
    if (line.find(trig_key1) != std::string::npos){  
      std::cout << "linietext: " << line << std::endl;
    }
}
 
  electronTriggerSFMapMixedLepton1["triggersMapMix_set1"] = rEnv.GetValue("triggersMapMix_set1"," ");
 
 std::cout << "electronTriggerSFMapMixedLepton1 "<< electronTriggerSFMapMixedLepton1["triggersMapMix_set1"] << std::endl;
//std::cout << "electronTriggerSFMapMixedLepton1 "<< rEnv.GetValue("triggersMapMix_set1"," ") << std::endl;
/////////////////////////////////////////////////////


   PATCore::ParticleDataType::DataType data_type(PATCore::ParticleDataType::Data);
    if (!isData)
    {
        if (isAtlfast)
            data_type = PATCore::ParticleDataType::Fast;
        else
            data_type = PATCore::ParticleDataType::Full;
    }

/*
 std::map<std::string, std::string> triggers={};


   triggers["2015"] = "2e12_lhloose_L12EM10VH"
                       "|| mu18_mu8noL1"
                       "|| 2mu10"
                       "|| e17_lhloose_mu14";
   triggers["296939-300287"] = "2e17_lhvloose_nod0"
                               "|| mu20_mu8noL1"
                               "|| 2mu10"
                               "|| e17_lhloose_nod0_mu14";
   triggers["300345-302872"] = "2e17_lhvloose_nod0"
                               "|| 2mu14"
                               "|| mu20_mu8noL1"
                               "|| e17_lhloose_nod0_mu14";
    triggers["302919-311481"] = "2e17_lhvloose_nod0"
                                "|| mu22_mu8noL1"
                                "|| 2mu14"
                                "|| e17_lhloose_nod0_mu14";
    triggers["2017"] = "2e24_lhvloose_nod0"
                       "|| mu22_mu8noL1"
                       "|| 2mu14"
                       "|| e17_lhloose_nod0_mu14";
    triggers["2018"] = "2e24_lhvloose_nod0"
                       "|| mu22_mu8noL1"
                       "|| 2mu14"
                       "|| e17_lhloose_nod0_mu14";
*/
//////////////////////////////////////////////////////
// triggers["2015"] = rEnv.GetValue("2015", "triggers2015" );

//            std::cout << "Trigere 2016 "<< triggers["2015"] << std::endl;



/////////////////////////////////////////////////////////
    //asg::AnaToolHandle<Trig::IMatchingTool> m_trigMatchingTool; 
    m_trigMatchingTool.setTypeAndName("Trig::MatchFromCompositeTool/TrigMatchFromCompositeTool");
    m_trigMatchingTool.isUserConfigured();
    ANA_CHECK( m_trigMatchingTool.setProperty("OutputLevel", MSG::ERROR));
    //ANA_CHECK( m_trigMatchingTool.setProperty("OutputLevel", MSG::DEBUG));
    if(isPHYSLITE){
      ANA_CHECK( m_trigMatchingTool.setProperty("InputPrefix", "AnalysisTrigMatch_"));
	}
    else{
    ANA_CHECK( m_trigMatchingTool.setProperty("InputPrefix", "TrigMatch_"));
    }
    ANA_CHECK( m_trigMatchingTool.setProperty("RemapBrokenLinks", true));
    ANA_CHECK( m_trigMatchingTool.retrieve() );



    //asg::AnaToolHandle<CP::IMuonTriggerScaleFactors> m_muonTriggerSFTool;
    //ToolHandleArray<CP::IMuonTriggerScaleFactors> m_muonTrigSFTools;
    m_muonTriggerSFTool.setTypeAndName("CP::MuonTriggerScaleFactors/MuonTriggerScaleFactors_Medium");
    m_muonTriggerSFTool.isUserConfigured();
    ANA_CHECK( m_muonTriggerSFTool.setProperty("AllowZeroSF", true) );
    ANA_CHECK( m_muonTriggerSFTool.setProperty("OutputLevel", MSG::ERROR) );
    //ANA_CHECK( m_muonTriggerSFTool.setProperty("OutputLevel", MSG::DEBUG) );
    ANA_CHECK( m_muonTriggerSFTool.setProperty("MuonQuality", "Tight") );
    ANA_CHECK( m_muonTriggerSFTool.retrieve() );
    m_muonTrigSFTools.push_back(m_muonTriggerSFTool.getHandle());

   std::map<std::string,std::string> electronTriggerSFMapMixedLepton {
  {"e17_lhloose, e17_lhloose_nod0", "MULTI_L_2015_e17_lhloose_2016_2018_e17_lhloose_nod0"},
            {"e12_lhloose_L1EM10VH, e17_lhvloose_nod0, e24_lhvloose_nod0_L1EM20VH", "DI_E_2015_e12_lhloose_L1EM10VH_2016_e17_lhvloose_nod0_2017_2018_e24_lhvloose_nod0_L1EM20VH"}

  };

    std::string  m_eleEffMapFilePathRun2 = "ElectronEfficiencyCorrection/2015_2018/rel21.2/Precision_Summer2020_v1/map4.txt";
    std::map<std::string,std::string> m_legsPerTool;
    std::string toolName;

   // ToolHandleArray<IAsgElectronEfficiencyCorrectionTool> m_elecTrigSFTools;
   // ToolHandleArray<IAsgElectronEfficiencyCorrectionTool> m_elecTrigEffTools;
   // std::vector<asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool>> m_elecEfficiencySFTool_trig_mixLep;
   // std::vector<asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool>> m_elecEfficiencySFTool_trigEff_mixLep;

    for(auto const& item : electronTriggerSFMapMixedLepton){
    	toolName = "AsgElectronEfficiencyCorrectionTool_trig_mixLep_" + (item.first).substr(0,8)+ "TightLLH";
   
        auto t_sf = m_elecEfficiencySFTool_trig_mixLep.emplace(m_elecEfficiencySFTool_trig_mixLep.end(), "AsgElectronEfficiencyCorrectionTool/"+toolName);
          ANA_CHECK( t_sf->setProperty("MapFilePath", m_eleEffMapFilePathRun2) );
          ANA_CHECK( t_sf->setProperty("TriggerKey", item.second) );
          ANA_CHECK( t_sf->setProperty("IdKey", "Tight") );
          ANA_CHECK( t_sf->setProperty("IsoKey", "FCTight") );
          ANA_CHECK( t_sf->setProperty("CorrelationModel", "TOTAL") );
          if (!isData) {
          ANA_CHECK( t_sf->setProperty("ForceDataType", (int) (data_type==PATCore::ParticleDataType::Fast)? PATCore::ParticleDataType::Full : data_type) );
          }
          //ANA_CHECK( t_sf->setProperty("OutputLevel",  MSG::DEBUG) );
          ANA_CHECK( t_sf->setProperty("OutputLevel",  MSG::ERROR ));
	  ANA_CHECK( t_sf->initialize() );
          m_elecTrigSFTools.push_back(t_sf->getHandle());
          //#ifndef XAOD_STANDALONE
          //  m_legsPerTool[toolName] = item.first;
          //#else
            m_legsPerTool["ToolSvc."+toolName] = item.first;
          //#endif
   



       toolName = "AsgElectronEfficiencyCorrectionTool_trigEff_mixLep_" + (item.first).substr(0,8) + "TightLLH";
        auto t_eff = m_elecEfficiencySFTool_trigEff_mixLep.emplace(m_elecEfficiencySFTool_trigEff_mixLep.end(), "AsgElectronEfficiencyCorrectionTool/"+toolName);
          ANA_CHECK( t_eff->setProperty("MapFilePath",  m_eleEffMapFilePathRun2) );
          ANA_CHECK( t_eff->setProperty("TriggerKey", "Eff_"+item.second) );
          ANA_CHECK( t_eff->setProperty("IdKey", "Tight") );
          ANA_CHECK( t_eff->setProperty("IsoKey", "FCTight") );
          ANA_CHECK( t_eff->setProperty("CorrelationModel", "TOTAL") );
          if (!isData) {
            ANA_CHECK( t_eff->setProperty("ForceDataType", (int) (data_type==PATCore::ParticleDataType::Fast)? PATCore::ParticleDataType::Full : data_type) );
          }
          //ANA_CHECK( t_eff->setProperty("OutputLevel", MSG::DEBUG));
          ANA_CHECK( t_eff->setProperty("OutputLevel", MSG::ERROR ));
	  ANA_CHECK( t_eff->initialize() );
          m_elecTrigEffTools.push_back(t_eff->getHandle());
          m_legsPerTool["ToolSvc."+toolName] = item.first;
 
    }  


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   //asg::AnaToolHandle<ITrigGlobalEfficiencyCorrectionTool> m_trigGlobalEffCorrTool_diLep;
   m_trigGlobalEffCorrTool_diLep.setTypeAndName("TrigGlobalEfficiencyCorrectionTool/TrigGlobal_diLep");
   m_trigGlobalEffCorrTool_diLep.isUserConfigured(); 
   ANA_CHECK( m_trigGlobalEffCorrTool_diLep.setProperty("MuonTools", m_muonTrigSFTools) );
   ANA_CHECK( m_trigGlobalEffCorrTool_diLep.setProperty("ElectronEfficiencyTools", m_elecTrigEffTools) );
   ANA_CHECK( m_trigGlobalEffCorrTool_diLep.setProperty("ElectronScaleFactorTools", m_elecTrigSFTools) );
   ANA_CHECK( m_trigGlobalEffCorrTool_diLep.setProperty("TriggerCombination", triggers));
   ANA_CHECK(m_trigGlobalEffCorrTool_diLep.setProperty("TriggerMatchingTool", m_trigMatchingTool.getHandle()));
   if(true) m_trigGlobalEffCorrTool_diLep.setProperty("OutputLevel", MSG::ERROR);
   //if(true) m_trigGlobalEffCorrTool_diLep.setProperty("OutputLevel", MSG::DEBUG);
   ANA_CHECK( m_trigGlobalEffCorrTool_diLep.setProperty("ListOfLegsPerTool", m_legsPerTool) );
   ANA_CHECK( m_trigGlobalEffCorrTool_diLep.setProperty("NumberOfToys", 0) );
   m_trigGlobalEffCorrTool_diLep.retrieve();


 if (SusyObjTool.initialize() != StatusCode::SUCCESS)    {
        Error(APP_NAME, "Cannot intialize SUSYTools");
        return EL::StatusCode::FAILURE;
          }

////////////////////////////////////////////////////////////////////////////////////////
    Info(APP_NAME, "SUSYObjDef_xAOD initialized... ");
    Info(APP_NAME, "ConfigFile = %s", ConfigFile.c_str());
    Info(APP_NAME, "IsData     = %i", isData);
    Info(APP_NAME, "IsAtlfast  = %i", isAtlfast);
    Info(APP_NAME, "ShowerType = %i", ShowerType);
    Info(APP_NAME, "IsPHYSLITE = %i", isPHYSLITE);

    /// Reset systematics
    if (!this->resetSystematics())
    {
        Error(APP_NAME, "Cannot reset systematics");
        return EL::StatusCode::FAILURE;
    }

    /// Set some other flags
    usePhotons = true;
    doSys = (doSys && !isData);
    isFakeMC = (isFakeMC && !isData);
    applySF = true;
    ccCheck = false;
    truthMsg = false;
    doJetCut = true;
    doPileup = PileupReweighting;
    Info(APP_NAME, "Apply SF  = %i", applySF);
    Info(APP_NAME, "Do PileupReweighting   = %i", doPileup);
    Info(APP_NAME, "Use Photons = %i", usePhotons);

    /// Set lepton-pt cuts
    lep_PtCut[0] = 1000.;
    lep_PtCut[1] = 1000.;
    lep_PtCut[2] = 1000.;

    /// Jet/bjet-pt cuts  for Jet reclustering tool and cutflow
    jet_PtCut[0] = 2000.;
    jet_PtCut[1] = 2000.;

    Info(APP_NAME, "Signal objects:");
    Info(APP_NAME, "Lepton[1st|2nd|3rd]:: PtCut = %.0f, %.0f, %.0f", lep_PtCut[0], lep_PtCut[1], lep_PtCut[2]);
    Info(APP_NAME, "Jets[LF|HF]        :: PtCut = %.0f, %.0f,", jet_PtCut[0], jet_PtCut[1]);

    /// SignalRegions(Name, Nj, Nb, NbMax, pT(j), MET, Meff, pairing)
    SignalRegions.clear();
    this->addSR("SRall", 0, 0, -1, 0, 0., 0., "2L+1");
    // this->addSR("SR3b1",   6,  3, -1,  25,  150000.,  750000., "2L+1");
    // this->addSR("SR3L0b1", 4,  0,  0,  40,  200000.,  600000., "3L"  );
    // this->addSR("SR3L0b1", 4,  0,  1,  40,  200000.,  600000., "3L"  );

    /// Select only specific SR
    this->checkNames(SignalRegions, this->tokenize(SigRegion));
    Info(APP_NAME, "Running on %i Signal Regions", (int)SignalRegions.size());

    /// GoodRunList
    Info(APP_NAME, "Initializing Tool: \t %s", "GoodRunsListSelectionTool");
    std::vector<std::string> GRLs;
    GRLs.push_back(PathResolverFindCalibFile("GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml"));
    GRLs.push_back(PathResolverFindCalibFile("GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"));
    GRLs.push_back(PathResolverFindCalibFile("GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"));
    GRLs.push_back(PathResolverFindCalibFile("GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"));
    ANA_CHECK(GRLTool.setProperty("GoodRunsListVec", GRLs));
    ANA_CHECK(GRLTool.setProperty("PassThrough", false));
    ANA_CHECK(GRLTool.initialize());
    for (auto GRL : GRLs)
        Info(APP_NAME, "Initialized GoodRunList %s", GRL.c_str());

    /// Set Triggers
    Info(APP_NAME, "Set trigger configuration: \t %s", TriggerName.c_str());
    if (!this->setTriggers(triggerList2015, TriggerName, 2015))
    {
        Error(APP_NAME, "Cannot set triggers");
        return EL::StatusCode::FAILURE;
    }
    if (!this->setTriggers(triggerList2016, TriggerName, 2016))
    {
        Error(APP_NAME, "Cannot set triggers");
        return EL::StatusCode::FAILURE;
    }
    if (!this->setTriggers(triggerList2017, TriggerName, 2017))
    {
        Error(APP_NAME, "Cannot set triggers");
        return EL::StatusCode::FAILURE;
    }
    if (!this->setTriggers(triggerList2018, TriggerName, 2018))
    {
        Error(APP_NAME, "Cannot set triggers");
        return EL::StatusCode::FAILURE;
    }

    /// Initialize systematics & print the considered syst sources
    SysInfoList = SusyObjTool->getSystInfoList();
    this->checkNames(SysInfoList, this->tokenize(SysList));       /// keep only the list of syst given in the as input to run.py script
    this->checkNames_ToRemoveSyst(SysInfoList, SysList_toRemove); /// remove some unwanted syst uncertainties
    Info(APP_NAME, "Number of found systematics %i", (int)SysInfoList.size() - 1);

    SysInfoList_W = this->getSysInfoList(doSys, SysInfoList, Weight);
    Info(APP_NAME, "Systematics::Weight-affecting %i", (int)SysInfoList_W.size() - 1);
    PrintSys(SysInfoList_W);

    SysInfoList_K = this->getSysInfoList(doSys, SysInfoList, Kinematic);
    Info(APP_NAME, "Systematics::Kinematics-affecting %i", (int)SysInfoList_K.size() - 1);
    PrintSys(SysInfoList_K);

    /// Initialize MCTruth Classifier
    Info(APP_NAME, "Initializing Tool: \t %s", "MCTruthClassifier");
    MCClassifier = new MCTruthClassifier("MCClassifier");
    MCClassifier->msg().setLevel(MSG::ERROR);
    ANA_CHECK(MCClassifier->initialize());
//////////////////////////////////////////////////////////////////////////////////////////////////////
   /// Trigger efficiency/scale factor CP tools for electrons and muons
    /// https://twiki.cern.ch/twiki/bin/view/Atlas/TrigGlobalEfficiencyCorrectionTool
    /// For property 'ListOfLegsPerTool':
    ToolHandleArray<CP::IMuonTriggerScaleFactors> muonTools;                //!
    ToolHandleArray<IAsgElectronEfficiencyCorrectionTool> electronEffTools; //!
    ToolHandleArray<IAsgElectronEfficiencyCorrectionTool> electronSFTools;  //!
    std::map<std::string, std::string> legsPerTool;
    enum
    {
        cLEGS,
        cKEY
    };

    Info(APP_NAME, "Configuring the Muon TriggerSF tools");
    std::string egMapFile = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/map3.txt";
    std::vector<std::array<std::string, 2>> toolConfigs =
        {
            {"e17_lhloose, e17_lhloose_nod0", "MULTI_L_2015_e17_lhloose_2016_2018_e17_lhloose_nod0"},
            {"e12_lhloose_L1EM10VH, e17_lhvloose_nod0, e24_lhvloose_nod0_L1EM20VH", "DI_E_2015_e12_lhloose_L1EM10VH_2016_e17_lhvloose_nod0_2017_2018_e24_lhvloose_nod0_L1EM20VH"},
        };
    std::string triggerEleIso = "FCLoose";
    for (auto &cfg : toolConfigs) /// one instance per trigger leg x working point
    {
        for (int j = 0; j < 2; ++j) /// two instances: 0 -> MC efficiencies, 1 -> SFs
        {
            std::string name = "AsgElectronEfficiencyCorrectionTool/" + ((j ? "ss3ldev_ElTrigEff_" : "ss3ldev_ElTrigSF_") + std::to_string(factoryEle.size() / 2));

            auto elEffTool = factoryEle.emplace(factoryEle.end(), name);
            elEffTool->setProperty("MapFilePath", egMapFile).ignore();
            elEffTool->setProperty("TriggerKey", std::string(j ? "" : "Eff_") + cfg[cKEY]).ignore();
            std::cout << cfg[cKEY] << std::endl;
            elEffTool->setProperty("IdKey", "Medium").ignore();
            elEffTool->setProperty("IsoKey", triggerEleIso).ignore();
            elEffTool->setProperty("CorrelationModel", "TOTAL").ignore();
            if (!isData)
                ANA_CHECK(elEffTool->setProperty("ForceDataType", (int)(data_type == PATCore::ParticleDataType::Fast) ? PATCore::ParticleDataType::Full : data_type));
            ANA_CHECK(elEffTool->setProperty("OutputLevel", MSG::ERROR));
            ANA_CHECK(elEffTool->initialize());

            auto &handles = (j ? electronSFTools : electronEffTools);
            handles.push_back(elEffTool->getHandle());
            name = handles[handles.size() - 1].name();
            std::cout << name << std::endl;
            legsPerTool[name] = cfg[cLEGS];
        }
    }

    Info(APP_NAME, "Configuring the Muon TriggerSF tools");
    muonTool.setTypeAndName("CP::MuonTriggerScaleFactors/ss3ldev_MuonTrigEff");
    muonTool.setProperty("MuonQuality", "Medium").ignore();
    ANA_CHECK(muonTool.setProperty("OutputLevel", MSG::ERROR));
    if (muonTool.initialize() != StatusCode::SUCCESS)
    {
        Error(APP_NAME, "Unable to initialize the muon CP tool!");
    }
    muonTools.push_back(muonTool.getHandle());
////////////////////////////////////////////////////


    Info(APP_NAME, "Configuring the global trigger SF tool");
    myTriggerSFTool.setProperty("ElectronEfficiencyTools", electronEffTools).ignore();
    myTriggerSFTool.setProperty("ElectronScaleFactorTools", electronSFTools).ignore();
    myTriggerSFTool.setProperty("MuonTools", muonTools).ignore();
    myTriggerSFTool.setProperty("TriggerCombination", triggers).ignore();
    myTriggerSFTool.setProperty("ListOfLegsPerTool", legsPerTool).ignore();
    ANA_CHECK(myTriggerSFTool.setProperty("OutputLevel", MSG::ERROR));
   
 
   if (myTriggerSFTool.initialize() != StatusCode::SUCCESS)
    {
        Error(APP_NAME, "Unable to initialize the TrigGlob tool!");
    }



    if (Debug)
        std::cout << "Done with trigger SFs" << std::endl
                  << std::endl;


	// Vgamma OR Tool
	//     ///https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/VGammaORTool
    std::vector<float> ph_pt; 
    ph_pt = std::vector<float>({7e3}); //https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/700xxx/700398/mc.Sh_2211_mumugamma.py#L54
    ANA_CHECK( m_VgammaORTool.setProperty("photon_pT_cuts",ph_pt) );
    ANA_CHECK( m_VgammaORTool.setProperty("OutputLevel", MSG::ERROR));
    ANA_CHECK( m_VgammaORTool.initialize() );

    /// Set Histograms
    if (!this->InitializeHistograms())
    {
        Error(APP_NAME, "Cannot initialize histograms. Exiting.");
        return EL::StatusCode::FAILURE;
    }

    /// Set Tree
    if (!this->InitializeTree())
    {
        Error(APP_NAME, "Cannot initialize TTree. Exiting.");
        return EL::StatusCode::FAILURE;
    }

    TDirectory *out_TDir = (TDirectory *)wk()->getOutputFile("output");
    hCutflow->SetDirectory(out_TDir);
    hEventsTotal->SetDirectory(out_TDir);

    Info(APP_NAME, "returning StatusCode::SUCCESS");
    return EL::StatusCode::SUCCESS;
}



/////////////////////////////////////////////////////////////////////////////////////
EL::StatusCode DiLepton::execute()
{
    const char *APP_NAME = "DiLepton::execute()";
    nLepCut = 2;
    /// print every 1000 events, so we know where we are:
    if ((m_entry % 1000) == 0)
        Info(APP_NAME, "Already processed %i events", m_entry);
    m_entry++;
    m_fileentry++;

    ///----------------------------
    /// Event information
    ///---------------------------
    const xAOD::EventInfo *eventInfo(0);
    if (!m_event->retrieve(eventInfo, "EventInfo").isSuccess())
    {
        Error(APP_NAME, "Failed to retrieve event info collection. Exiting.");
        return EL::StatusCode::FAILURE;
    }

    /// check if the event is data or MC
    if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION))
        isMC = true;
    else
        isMC = false;

    RunNumber = eventInfo->runNumber(); /// can be used to select year
    EventNumber = eventInfo->eventNumber();


    LB = eventInfo->lumiBlock();

    MCId = isMC ? eventInfo->mcChannelNumber() : 0;

    EventWeight = isMC ? (eventInfo->mcEventWeights()).at(0) : 1.;

    /// https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/SUSYTools/SUSYObjDef_xAOD.h

    ANA_MSG_DEBUG("PRW Weight = " << SusyObjTool->GetPileupWeight());
    // ANA_CHECK( SusyObjTool->ApplyPRWTool());
    ANA_MSG_DEBUG("PRW Weight = " << SusyObjTool->GetPileupWeight());

    /// PileUp Reweighting
    if (isMC)
    {

        ANA_CHECK(SusyObjTool->ApplyPRWTool());
        randomRN = SusyObjTool->GetRandomRunNumber();
    }
    else
    {
        randomRN = RunNumber;
    }

    PileUpWeight = isMC ? this->getPileupWeight() : 1.;
    float EventWeightPileUp = EventWeight * PileUpWeight;
    IntPerX_CorrectedScaled = SusyObjTool->GetCorrectedAverageInteractionsPerCrossing(/**bool includeDataSF=*/true); /// false by default; if true then it's also scaled
    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t "
                  << Form("EventNumber %ld \t MC Event RunNumber %ld \t MCId %i \t IntPerX_CorrectedScaled %f", EventNumber, RunNumber, MCId, IntPerX_CorrectedScaled) << std::endl;
    }

    /// take data year and the data period for 2016 trigger selection
    configYear = SusyObjTool->treatAsYear();
   //////////////////////// 
   int dataPeriod = 0;
    if (configYear == 2016)
    {
        if (randomRN > 296930 && randomRN < 302880)
            dataPeriod = 1;
        if (randomRN > 302881 && randomRN < 304010)
            dataPeriod = 2;
        if (randomRN > 304011)
            dataPeriod = 3;
    }
////////////////////////////
//https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LowestUnprescaled#Muon_AN4
//https://atlas-tagservices.cern.ch/tagservices/RunBrowser/runBrowserReport/rBR_Period_Report.php?fnt=data16_13TeV
    int data_period = 0;
    if (configYear == 2016){
    	if (randomRN >= 296939 && randomRN <= 300287){
           data_period = 1;
        }
	if (randomRN >= 300345 && randomRN <= 302872){
           data_period = 2;
        }
	//if (randomRN >= 302919 && randomRN <= 307861)
	if (randomRN >= 302919 ){
           data_period = 3;
        }
     }


    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t PileupReweightingTool::Pileupweight " << PileUpWeight << "  RandomRunNumber " << randomRN << "   RunNumber   " << RunNumber << "  configYear " << configYear << std::endl;
    }

    /// Sherpa reweighting
    bool doSherpaRW = false;
    if (isMC && doSherpaRW)
    {
        float sherpaSF = SusyObjTool->getSherpaVjetsNjetsWeight();
        EventWeightPileUp *= sherpaSF;
        if (Debug)
        {
            std::cout << APP_NAME << " DEBUG \t Sherpa weight " << sherpaSF << "\t weight*PileUp*sherpaSF " << EventWeightPileUp << std::endl;
        }
    }

    /// SUSY truth numbers
    if (isMC)
    {
        std::vector<int> pdgIDs = this->getSusyPDG(m_event);
        GLDec1 = pdgIDs[2];
        GLDec2 = pdgIDs[3];
    }

    /// Start loop on signal regions
    for (unsigned int sr(0); sr < NSr; ++sr)
    {
        SR SignalRegion = SignalRegions.at(sr);
        if (Debug)
            this->PrintSR(SignalRegion, "DEBUG");
        //        this->PrintSR(SignalRegion, "DEBUG");
        this->resetSystematics(0);
        for (unsigned int sys(0); sys < NSys; sys++)
        {
            this->applySystematic((CP::SystematicSet)SysInfoList_K[sys].systset, Debug);
            // std::cout << APP_NAME <<" MARINA sistematice "<<std::endl;
            
	    //if ( EventNumber != 35334996) continue;

	    /// Clear all variables
            weight = EventWeightPileUp;
            weightFakeMu_MCTemplate = weightFakeEl_MCTemplate = FakeLepMCTemplateWeight = 1.;
            uncFakeMu_MCTemplate = uncFakeEl_MCTemplate = FakeMuMCTemplateUnc = FakeEleMCTemplateUnc = 0.;
            SysWeights.clear();
            SysNames.clear();
            store.clear();

            muTLV.clear();
            elTLV.clear();
            lepTLV.clear();
            lepTLV_BL.clear();
            jetTLV.clear();
            jetBtag.clear();
            jetTLV_R1.clear();
            muCharges.clear();
            elCharges.clear();
            lepCharges.clear();
            jetCharges.clear();
            elQualities.clear();
            muQualities.clear();
            lepQualities.clear();
            jetQualities.clear();
            lepTruth.clear();

            muSF.clear();
            elSF.clear();
            jetDL1r.clear();
            jetJVT.clear();
            triggerInfo.clear();
            topoStr.Clear();

            EventTM = 0;
            SSChannel = 0;
            IntPerX_CorrectedScaled = 0;
            /// isZevent  = 0;
ptll =0;
ptllphi =0;
ptllx=0;
ptlly=0;
etx=0;
ety=0;
mll=0;
etll=0;
mtn=0;
m13=0;
m14=0;
m23=0;
m24=0;
mrel3=0;
mrel4=0;
            mu_1stPt = 0;
            mu_2ndPt = 0;
            mu_3rdPt = 0;
            mu_1stEta = 0;
            mu_2ndEta = 0;
            el_1stPt = 0;
            el_2ndPt = 0;
            el_3rdPt = 0;
            el_1stEta = 0;
            el_2ndEta = 0;
            lep_1stPt = 0;
            lep_2ndPt = 0;
            lep_3rdPt = 0;
            jet_1stPt = 0;
            jet_2ndPt = 0;
lep_1stPhi = 0;
            lep_2ndPhi = 0;
            //lep_3rdPhi = 0;
            jet_1stPhi = 0;
            jet_2ndPhi = 0;

             lep_1stEta = 0;
            lep_2ndEta = 0;
            //lep_3rdEta = 0;
            jet_1stEta = 0;
            jet_2ndEta = 0;
            EtMiss = 0;
            EtMissPhi = 0;
            EtMissSig = 0;
            meff = 0;
            minv = 0;
            mt = 0;
            ht = 0;
            mj = 0;
            mjj = 0;
            mjjdR = 0;
            mjRec = 0;
            truthHt = 0;
            truthMET = 0;
//////
c3= 0;
c4=0;
jet_1stEta = 0;
           jet_3thEta = 0;
            jet_4thEta = 0;
             jet_mulEta = 0;
            jet_2ndEta = 0;
 jet_1stPhi = 0;
          jet_diffPhi = 0;
            jet_diffEta = 0;
            jet_2ndPhi = 0;
            //////////
            mu_n_EWcombi = 0;
            el_n_EWcombi = 0;
            mu_n = 0;
            el_n = 0;
            lep_n = 0;
            jet_n = 0;
            b_n = 0;

            /// Create the baseline ele and mu containers -> do not store them
            xAOD::MuonContainer *muons_baseline = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
            xAOD::ElectronContainer *electrons_baseline = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);

            /// Create & store xAOD Containers
            xAOD::MuonContainer *GoodMuons = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);
            xAOD::ElectronContainer *GoodElectrons = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
            xAOD::JetContainer *GoodJets = new xAOD::JetContainer(SG::VIEW_ELEMENTS);

            xAOD::MissingETContainer *met = new xAOD::MissingETContainer;
            xAOD::MissingETAuxContainer *metAux = new xAOD::MissingETAuxContainer;
            met->setStore(metAux);
            ANA_CHECK(store.record(GoodMuons, "SelectedGoodMuons"));
            ANA_CHECK(store.record(GoodElectrons, "SelectedGoodElectrons"));
            ANA_CHECK(store.record(GoodJets, "SelectedGoodJets"));
            ANA_CHECK(store.record(met, "MET_RefFinal"));
            ANA_CHECK(store.record(metAux, "MET_RefFinalAux."));

            bool doSF = (isMC && applySF);

            /// Fill event properties
            FillHisto(hEvents[sr][sys], 0);
            FillHisto(hEvents[sr][sys], 1, EventWeight);
            FillHisto(hEventWeight[sr][sys], EventWeight);

            /// Fill Control Tree
            bool nom = (!sys && !sr);
            if (nom)
                this->FillTree(ControlTree, (bool)makeTree);
            float _nom = 0.;
            if (nom)
                _nom = 1.;

            /// Starting CutFlow
            Int_t CutFlowIndex(0);
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "all ev");
            CutFlowIndex++;

            /// Cut on truth variables (and to be able to merge some bkg samples)
            truthHt = isMC ? this->getTruthVar(m_event, 0) : 0.;
            truthMET = isMC ? this->getTruthVar(m_event, 1) : 0.;
            /*bool doTruthCut = false;
            if (doTruthCut && truthVeto(MCId, truthHt, truthMET))
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "TruthVeto");
            CutFlowIndex++;
*/
            /// GRL Cut
            bool doGRL = true;
            if (doGRL && !isMC && !GRLTool->passRunLB(RunNumber, eventInfo->lumiBlock()))
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "GRL");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after GRL" << std::endl;
            }
            /// Check for primary vertex
            const xAOD::Vertex *pv = SusyObjTool->GetPrimVtx();
            if (!pv)
                continue; /// if no PV, continue to avoid several crashes

            int Nvtx = 0;
            const xAOD::VertexContainer *vertices(0);
            if (m_event->retrieve(vertices, "PrimaryVertices").isSuccess())
            {
                for (const auto &vx : *vertices)
                {
                    Nvtx++;
                    /// if(vx->vertexType() == xAOD::VxType::PriVtx){ /// Found THE vertex
                    /// primvertex_z = vx->z();
                    /// }
                }
            }
            else
            {
                Error(APP_NAME, "Failed to retrieve PV container. Exiting.");
                return EL::StatusCode::FAILURE;
            }
  /*          if (Nvtx == 0)
                continue;

            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "PrimaryVertex");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after PV cut, Nvtx = " << Nvtx << std::endl;
            }
*/
            /// LAr errors global flags
  /*          bool isLArError = ((eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) || (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) || (eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)));
            if (!isMC && isLArError)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "Global Flags");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: Global Flags" << std::endl;
            }
*/
            /// https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/SUSYTools/SUSYObjDef_xAOD.h
            setROOTmsg(0);
            static int count = 0;
            bool displ = (count++ < 30);

            /// ***** Retrieving baseline muons *****
            xAOD::MuonContainer *muons_copy(nullptr);
            xAOD::ShallowAuxContainer *muons_copyAux(nullptr);
            ANA_CHECK(SusyObjTool->GetMuons(muons_copy, muons_copyAux, true, isPHYSLITE ? "AnalysisMuons" : "Muons"));

            /// ***** Retrieving baseline electrons *****
            xAOD::ElectronContainer *electrons_copy(0);
            xAOD::ShallowAuxContainer *electrons_copyAux(0);
            ANA_CHECK(SusyObjTool->GetElectrons(electrons_copy, electrons_copyAux, true, isPHYSLITE ? "AnalysisElectrons" : "Electrons"));

            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t No. of electrons in combi container \t" << (int)(electrons_copy)->size() << std::endl;
            }

            /// ***** Retrieving baseline photons ****
            xAOD::PhotonContainer *photons_baseline(0);
            xAOD::ShallowAuxContainer *photons_baselineAux(0);
            // if(usePhotons) ANA_CHECK( SusyObjTool->GetPhotons(photons_baseline, photons_baselineAux, true) );
            if (usePhotons)
                ANA_CHECK(SusyObjTool->GetPhotons(photons_baseline, photons_baselineAux, true, isPHYSLITE ? "AnalysisPhotons" : "Photons"));

            /// ***** Retrieving baseline jets *****
            xAOD::JetContainer *jets_baseline(0);
            xAOD::ShallowAuxContainer *jets_baselineAux(0);
            // ANA_CHECK( SusyObjTool->GetJets(jets_baseline, jets_baselineAux, true) ); /// AntiKt4EMPFlowJets_BTagging201903
            ANA_CHECK(SusyObjTool->GetJets(jets_baseline, jets_baselineAux, true, isPHYSLITE ? "AnalysisJets" : ""))

            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t No. of Jets in container \t" << (int)(jets_baseline)->size() << std::endl;
            }

            /// Missing Et
            if (!SusyObjTool->IsPFlowCrackVetoCleaning(electrons_copy, photons_baseline) && SusyObjTool->treatAsYear() < 2017)
            {
                ATH_MSG_VERBOSE("PFlow e/gamma MET crack veto failed; see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJetsR21#EGamma_Crack_Electron_topocluste ");
                continue; 
            }
            bool show_met_info = (EventNumber == 62533719);
            this->getMET(EtMiss, EtMissPhi, met, jets_baseline, electrons_copy, muons_copy, photons_baseline, true, true, show_met_info);
            double tem_metSig = 0;
            SusyObjTool->GetMETSig(*met, tem_metSig, true, true); /// last two terms are: bool doTST, bool doJVTCut;  Definition at /SUSYTools/Root/MET.cxx
            EtMissSig = tem_metSig;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Got the EtMiss " << EtMiss << "  EtMissPhi " << EtMissPhi << std::endl;
            }

// Check overlap between V+jets and V+gamma
      bool in_vy_overlap = false;
      if (isSherpaZllMC(eventInfo)){
 	ANA_CHECK((*m_VgammaORTool).inOverlap(in_vy_overlap));
       } 	
      if (in_vy_overlap) continue;
//


  /*          /// Pass trigger
            bool passTrigger = true;
            switch (configYear)
            {
            case 2015:
                if (!isTriggered(triggerList2015, triggerInfo))
                    passTrigger = false;
                if (!isTriggeredMet(triggerList2015, EtMiss, triggerInfo))
                    passTrigger = false;
                break;
            case 2016:
                if (!isTriggeredMulti(triggerList2016, triggerInfo, true, data_period))
                    passTrigger = false;
                if (!isTriggeredMet2016(triggerList2016, EtMiss, triggerInfo, dataPeriod))
                    passTrigger = false;
                break;
            case 2017:
                if (!isTriggered(triggerList2017, triggerInfo))
                    passTrigger = false;
                if (!isTriggeredMet(triggerList2017, EtMiss, triggerInfo))
                    passTrigger = false;
                break;
            case 2018:
                if (!isTriggered(triggerList2018, triggerInfo))
                    passTrigger = false;
                if (!isTriggeredMet(triggerList2018, EtMiss, triggerInfo))
                    passTrigger = false;
                break;
            default:
                break;
            }
         
	  if (Debug)
            {
                std::cout << "data_period" << data_period << std::endl;
            }

   
	 if (!passTrigger) 
		continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "Trigger");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: trigger selection" << std::endl;
            }
*/
            /// Overlap removal: take the decorations; iterators / containers not changed
            ANA_CHECK(SusyObjTool->OverlapRemoval(electrons_copy, muons_copy, jets_baseline));

            /// take decorations for photons
            if (usePhotons)
            {
                if (Debug)
                {
                    std::cout << APP_NAME << " DEBUG \t No. of Photons in container \t" << (int)(photons_baseline)->size() << std::endl;
                }
                for (const auto ph : *photons_baseline)
                {
                    PrintInfo(ph, APP_NAME, 0);
                }
            }

	    //fill in the muons_baseline collection
	    for (const auto& mu : *muons_copy) {
   	    	if (mu->auxdataConst<char>("baseline") == 1)
   			muons_baseline->push_back(mu);
   	    }
   	    if(Debug){std::cout << APP_NAME << " DEBUG \t No. of muons in baseline container \t" << (int)(muons_baseline)->size() << std::endl;}	

	    //fill in the electrons_baseline collection
	    for (const auto& ele : *electrons_copy) {
                if (ele->auxdataConst<char>("baseline") == 1)
                        electrons_baseline->push_back(ele);
            }
            if(Debug){std::cout << APP_NAME << " DEBUG \t No. of electrons in baseline container \t" << (int)(electrons_baseline)->size() << std::endl;}
            

/*	    // Bad muon veto
            bool badMuons = badMuonVeto(muons_baseline); /// the muon has to pass the baseline selection
            if (badMuons)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "badMuonsVeto");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after bad muon veto" << std::endl;
            }
*/
            /// Reset OR flag for non-BL objects
            this->checkOR(muons_baseline);
            this->checkOR(electrons_baseline);
            this->checkOR(jets_baseline);
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t After removing the bad baseline leptons" << std::endl;
            }

            /// signal jets, take SIG and SIGNAL decorations
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t No. of Jets in container \t" << (int)(jets_baseline)->size() << std::endl;
            }
            for (const auto &jet : *jets_baseline)
            {
                bool isSignal = this->isSignalJet(*jet);
                bool isSignal_SusyTools = jet->auxdataConst<char>("signal") == 1;
                PrintInfo(jet, APP_NAME, Debug);

                if (getQuality(*jet) > 1 && isSignal_SusyTools != isSignal)
                {
                    std::cout << "ERROR!! Working on run/evt: " << RunNumber << " / " << EventNumber << std::endl;
                    std::cout << "jet_pt = " << jet->pt() << ", jet_eta = " << jet->eta() << ", jvt = " << jet->auxdataConst<float>("Jvt")
                              << ", bad-jet = " << (bool)jet->auxdataConst<char>("bad") << ", jet quality = " << getQuality(*jet)
                              << ", isSignal_SusyTools = " << isSignal_SusyTools << ", isSignal = " << isSignal << std::endl;
                }
            }

            /// Loop on good jets to take the signal jets, and fill in several signal branches for jets
            for (const auto &jet : *jets_baseline)
            {
                if (getQuality(*jet) > 1)
                { /// here are jets after OR and SIG jets
                    GoodJets->push_back(jet);
                    jetTLV.push_back((jet)->p4());
                    jetCharges.push_back(0);
                    jetQualities.push_back(getQuality(*jet));
                    jetDL1r.push_back(getWeight(*jet, DL1r));
                    jetJVT.push_back(getWeight(*jet, JVT));
                }
            }

            /// One jet pass OR cut
  /*          bool jetPassOR = getNjets(jets_baseline, 1);
            if (doJetCut && !jetPassOR)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "xBaseJets");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after at least one jet cut" << std::endl;
            }

            /// Bad jet veto, after OR
            bool badJets = badJetVeto(jets_baseline);
            if (badJets)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "badJetVeto");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after bad jet veto" << std::endl;
            }

            /// One signal jet cut, again defined with SIG jets
            int n_signal_jets = getNjets(jets_baseline, 2);
            if (doJetCut && n_signal_jets < 1)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "xSigJets");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after at least one signal jet" << std::endl;
            }
*/
            /// take signal decorations for muons and electrons
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t No. of Muons in container \t" << (int)(muons_baseline)->size() << std::endl;
            }
            for (const auto &mu : *muons_baseline)
            {
                this->isSignalMuon(*mu);
                PrintInfo(mu, APP_NAME, Debug);
            }
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t No. of Electrons in container \t" << (int)(electrons_baseline)->size() << std::endl;
            }
            for (const auto &el : *electrons_baseline)
            {
                this->isSignalElectron(*el);
                PrintInfo(el, APP_NAME, Debug);
            }

            /// fill in variables for muons and electrons, after OR and fill in the GoodMuons and GoodElectrons also after signal defs
            for (const auto &mu : *muons_baseline)
            {
                if (getQuality(*mu) > 0)
                { /// after OR
                    muTLV.push_back((mu)->p4());
                    muCharges.push_back((mu)->charge());
                    muQualities.push_back(getQuality(*mu));
                }
                if (getQuality(*mu) > 1)
                { /// after SIG selection
                    GoodMuons->push_back(mu);
                }
            }
            for (const auto &el : *electrons_baseline)
            {
                if (getQuality(*el) > 0)
                { /// after OR
                    elTLV.push_back((el)->p4());
                    elCharges.push_back((el)->charge());
                    elQualities.push_back(getQuality(*el));
                }
                if (getQuality(*el) > 1)
                { /// after SIG selection
                    GoodElectrons->push_back(el);
                }
            }

            /// Check for cosmic muons ==> not activated, see the ini in the DiLepton.h file
            bool isCosmic = cosmicsVeto(muons_baseline);
            if (isCosmic) 
		continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "cosmicMuonVeto");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after cosmic muon veto -- should do nothing" << std::endl;
            }

            /// Merged lepton vector - baseline leptons
            lepTLV = muTLV;
            lepCharges = muCharges;
            lepQualities = muQualities;
            lepTLV.insert(lepTLV.end(), elTLV.begin(), elTLV.end());
            lepCharges.insert(lepCharges.end(), elCharges.begin(), elCharges.end());
            lepQualities.insert(lepQualities.end(), elQualities.begin(), elQualities.end());
            /// Copy of original jet TLVs
            std::vector<TLorentzVector> jetTLV_copy = jetTLV;

            /// Sort vectors
            sortPt(muTLV, muCharges, muQualities);
            sortPt(elTLV, elCharges, elQualities);
            sortPt(lepTLV, lepCharges, lepQualities);
            sortPt(jetTLV, jetCharges, jetQualities);
            sortPt(jetTLV_copy, jetDL1r, jetJVT);
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t After jet sorting: " << jetTLV.size() << std::endl;
            }

            /// Storde B-tag info (for each jet set to 0 or 1, depending on the b-tagging info)
            jetBtag = this->getBtags(jetTLV, GoodJets);
            if (jetBtag.size() != jetTLV.size())
            {
                std::cout << "VECTOR SIZES NOT THE SAME. FUUUUCCCKKKK!!!!" << std::endl;
            }
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t After jet btag requirement: " << jetTLV.size() << std::endl;
            }

            if (makeHist && !sys)
            { /// only for nominal tree
                /// Fill baseline histos
                float l_1stPt(-1.), l_1stEta(-99.);
                float l_2ndPt(-1.), l_2ndEta(-99.);
                if (lepTLV.size() > 0)
                {
                    l_1stPt = lepTLV.at(0).Pt();
                    l_1stEta = lepTLV.at(0).Eta();
                }
                if (lepTLV.size() > 1)
                {
                    l_2ndPt = lepTLV.at(1).Pt();
                    l_2ndEta = lepTLV.at(1).Eta();
                }

                FillHisto(BaselineMet[sr][sys], EtMiss);
                FillHisto(BaselineLepPt[sr][sys], l_1stPt, l_2ndPt);
                FillHisto(BaselineLepEta[sr][sys], l_1stEta, l_2ndEta);

                mj = jetTLV.size() > 0 ? (jetTLV.at(0)).M() : 0.;
                mjRec = jetTLV_R1.size() > 0 ? (jetTLV_R1.at(0)).M() : 0.;
                mjj = jetTLV.size() > 1 ? (jetTLV.at(0) + jetTLV.at(1)).M()/1000 : 0.;
                m13   = jetTLV.size()>2 ? (jetTLV.at(0) + jetTLV.at(2)).M()/1000 : 0.;
                m14   = jetTLV.size()>3 ? (jetTLV.at(0) + jetTLV.at(3)).M()/1000 : 0.;
                m23   = jetTLV.size()>2 ? (jetTLV.at(1) + jetTLV.at(2)).M()/1000 : 0.;
                m24   = jetTLV.size()>3 ? (jetTLV.at(1) + jetTLV.at(3)).M()/1000 : 0.;
 //               if (m13>m23) {mrel3= m23/mjj};else{mrel3= m13/mjj};
               // if (m14>m24) {mrel4= m24/mjj};else{mrel4= m14/mjj};
if (m14 > m23) {
    mrel3 = m23 / mjj;
} else {
    mrel3 = m13 / mjj;
}

                if (m14 > m24) {
    mrel4 = m24 / mjj;
} else {
    mrel4 = m14 / mjj;
}

  		mjjdR = this->MjjdRmin(jetTLV);
                if (Debug)
                {
                    std::cout << APP_NAME << " DEBUG \t " << Form("Mj=%.0f | Mjj=%.0f | MjjdR=%.0f | MjRec=%.0f", mj, mjj, mjjdR, mjRec) << std::endl;
                }

                FillHisto(BaselineJet_Mj[sr][sys], mj);
                FillHisto(BaselineJet_Mjj[sr][sys], mjj);
                FillHisto(BaselineJet_MjjdR[sr][sys], mjjdR);
                FillHisto(BaselineJet_MjRec[sr][sys], mjRec);
                for (unsigned int i(0); i < jetTLV.size(); i++)
                    FillHisto(BaselineJet_MjAll[sr][sys], (jetTLV.at(i)).M());
                for (unsigned int i(0); i < jetTLV_R1.size(); i++)
                    FillHisto(BaselineJet_MjRecAll[sr][sys], (jetTLV_R1.at(i)).M());
            }

            /// Cut on baseline leptons
            NlepBL = getLepNumber(muTLV, elTLV); /// = lepTLV_BL.size()
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t BaselineLep information for EventNumber " << EventNumber << Form("\t NlepBL = %i (%i for red.Eta)", NlepBL) << std::endl;
            }
  /*          if (NlepBL < 2)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "geq2BaseLept");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after at least two baseline leptons cut" << std::endl;
            }
*/
            /// Select signal leptons
            unsigned int Nlep = NSignalLeptons;
            this->getSignalLeptons("Muon", muTLV, muCharges, muQualities, Nlep);
            this->getSignalLeptons("Electron", elTLV, elCharges, elQualities, Nlep);
            this->getSignalLeptons("Lepton", lepTLV, lepCharges, lepQualities, Nlep);

            topoStr = Form("%im %ie %il %ij", (int)muTLV.size(), (int)elTLV.size(), (int)lepTLV.size(), (int)jetTLV.size());
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Topology before SR cuts (signal leptons/jets here) " << topoStr << std::endl;
            }

            if (makeHist && !sys)
            {
                /// leading (subleading) objects
                mu_1stPt = (muTLV.size() > 0) ? (muTLV.at(0).Pt()) : -1.;
                mu_2ndPt = (muTLV.size() > 1) ? (muTLV.at(1).Pt()) : -1.;
                mu_3rdPt = (muTLV.size() > 2) ? (muTLV.at(2).Pt()) : -1.;
                mu_1stEta = (muTLV.size() > 0) ? (muTLV.at(0).Eta()) : -99.;
                mu_2ndEta = (muTLV.size() > 1) ? (muTLV.at(1).Eta()) : -99.;
                el_1stPt = (elTLV.size() > 0) ? (elTLV.at(0).Pt()) : -1.;
                el_2ndPt = (elTLV.size() > 1) ? (elTLV.at(1).Pt()) : -1.;
                el_3rdPt = (elTLV.size() > 2) ? (elTLV.at(2).Pt()) : -1.;
                el_1stEta = (elTLV.size() > 0) ? (elTLV.at(0).Eta()) : -99.;
                el_2ndEta = (elTLV.size() > 1) ? (elTLV.at(1).Eta()) : -99.;
                lep_1stPt = (lepTLV.size() > 0) ? (lepTLV.at(0).Pt()/1000) : -1.;
                lep_2ndPt = (lepTLV.size() > 1) ? (lepTLV.at(1).Pt()/1000) : -1.;
                lep_3rdPt = (lepTLV.size() > 2) ? (lepTLV.at(2).Pt()) : -1.;
                jet_1stPt = (jetTLV.size() > 0) ? (jetTLV.at(0).Pt()/1000) : -1.;
                jet_2ndPt = (jetTLV.size() > 1) ? (jetTLV.at(1).Pt()/1000) : -1.;
 lep_1stEta = (lepTLV.size()>0) ? (lepTLV.at(0).Eta()) : -99.;
                lep_2ndEta = (lepTLV.size()>1) ? (lepTLV.at(1).Eta()) : -99.;
   meff = lep_1stPt+ lep_2ndPt + EtMiss/1000;
ptll= (lepTLV.size()>1) ? (lepTLV.at(0)+lepTLV.at(1)).Pt()/1000: -1.;
                ptllphi= (lepTLV.size()>1) ? (lepTLV.at(0)+lepTLV.at(1)).Phi(): -1.;
                ptllx= ptll*cos(ptllphi);
                ptlly= ptll*sin(ptllphi);
                etx=EtMiss*cos(EtMissPhi)/1000;
                ety=EtMiss*sin(EtMissPhi)/1000;
                mll= (lepTLV.size()>1) ? (lepTLV.at(0)+lepTLV.at(1)).M()/1000: -1.;
                etll= sqrt(ptll*ptll+mll*mll);
                mtn= sqrt(   (EtMiss/1000+etll)*(EtMiss/1000+etll)  - ((ptllx+etx)*(ptllx+etx)+ (ptlly+ety)*(ptlly+ety)) )    ;
//std::cout << "MLLLLL === " << mll << "ETTLLLL=== "<< etll <<  "MMMTTTNNNNN === " << mtn <<  std::endl;
                /////////
                 jet_1stEta = (jetTLV.size()>0) ? (jetTLV.at(0).Eta()) : -99.;
                jet_2ndEta = (jetTLV.size()>1) ? (jetTLV.at(1).Eta()) : -99.;
               jet_3thEta = (jetTLV.size()>2) ? (jetTLV.at(2).Eta()) : -99.;
                jet_4thEta = (jetTLV.size()>3) ? (jetTLV.at(3).Eta()) : -99.;
               jet_1stPhi = (jetTLV.size()>0) ? (jetTLV.at(0).Phi()) : -99.;
                jet_2ndPhi = (jetTLV.size()>1) ? (jetTLV.at(1).Phi()) : -99.;
                c3=  exp((-4*pow(( jet_3thEta - (jet_1stEta+jet_2ndEta)/2   ),2))/pow((jet_1stEta-jet_2ndEta),2));
                c4=  exp((-4*pow(( jet_4thEta - (jet_1stEta+jet_2ndEta)/2   ),2))/pow((jet_1stEta-jet_2ndEta),2));
                jet_diffEta = jet_1stEta-jet_2ndEta;
                jet_mulEta = jet_1stEta*jet_2ndEta;
                jet_diffPhi = jet_1stPhi-jet_2ndPhi;  	
   /// Fill trigger histos
                FillHisto(TrigMet[sr][sys], EtMiss);
                FillHisto(TrigJetPt[sr][sys], jet_1stPt, jet_2ndPt);
                FillHisto(TrigElPt[sr][sys], el_1stPt, el_2ndPt);
                FillHisto(TrigElEta[sr][sys], el_1stEta, el_2ndEta);
                FillHisto(TrigMuPt[sr][sys], mu_1stPt, mu_2ndPt);
                FillHisto(TrigMuEta[sr][sys], mu_1stEta, mu_2ndEta);
            }

FillHisto(Lep_1stPt[sr][sys], lep_1stPt, weight);
                FillHisto(Lep_2ndPt[sr][sys], lep_2ndPt, weight);
       // FillHisto(Lep_1stEta[sr][sys], lep_1stEta, weight);
            //    FillHisto(Lep_2ndEta[sr][sys], lep_2ndEta, weight);
            //    FillHisto(Lep_1stPhi[sr][sys], lep_1stPhi, weight);
            //    FillHisto(Lep_2ndPhi[sr][sys], lep_2ndPhi, weight);
FillHisto(Mll[sr][sys], mll, weight);
FillHisto(Mtn[sr][sys], mtn, weight);
FillHisto(Etll[sr][sys], etll, weight);

FillHisto(Meff[sr][sys],   meff,   weight);
                FillHisto(Jet_M13[sr][sys], m13, weight);
FillHisto(Jet_M23[sr][sys], m23, weight);
FillHisto(Jet_M24[sr][sys], m24, weight);
FillHisto(Jet_M14[sr][sys], m14, weight);
FillHisto(Jet_Mrel3[sr][sys], mrel3, weight);
FillHisto(Jet_Mrel4[sr][sys], mrel4, weight);
/*FillHisto(Mll[sr][sys], mll/1000, weight);
FillHisto(Mtn[sr][sys], mtn/1000, weight);
FillHisto(Etll[sr][sys], etll/1000, weight);

FillHisto(Meff[sr][sys],   meff/1000,   weight);
*/
//FillHisto(Mu_1stPt[sr][sys],mtn/1000 , weight);
                FillHisto(Lep_3rdPt[sr][sys], lep_3rdPt, weight);
                FillHisto(Jet_1stPt[sr][sys], jet_1stPt, weight);
                FillHisto(Jet_2ndPt[sr][sys], jet_2ndPt, weight);
         FillHisto(Jet_1stEta[sr][sys], jet_1stEta, weight);
                 FillHisto(Jet_mulEta[sr][sys], jet_mulEta, weight);
                FillHisto(Jet_2ndEta[sr][sys], jet_2ndEta, weight);
         FillHisto(Jet_3thEta[sr][sys], jet_3thEta, weight);
         FillHisto(Jet_4thEta[sr][sys], jet_4thEta, weight);
                FillHisto(Jet_1stPhi[sr][sys], jet_1stPhi, weight);
                FillHisto(Jet_2ndPhi[sr][sys], jet_2ndPhi, weight);
        FillHisto(Jet_diffPhi[sr][sys], jet_diffPhi, weight);
FillHisto(Jet_diffEta[sr][sys], jet_diffEta, weight);
FillHisto(C3[sr][sys], c3, weight);
FillHisto(C4[sr][sys], c4, weight);
// FillHisto(Mu_1stEta[sr][sys], mu_1stEt, weight);
FillHisto(Mu_1stEta[sr][sys], mu_1stEta, weight);
                FillHisto(Mu_2ndEta[sr][sys], mu_2ndEta, weight);
                FillHisto(El_1stEta[sr][sys], el_1stEta, weight);
                FillHisto(El_2ndEta[sr][sys], el_2ndEta, weight);
      FillHisto(NJet[sr][sys],  Njet25x, weight);
                FillHisto(NBJet[sr][sys], b_n,   weight);
 FillHisto(ETmiss[sr][sys], EtMiss/1000, weight);
//  FillHisto(ETmiss[sr][sys], EtMiss, weight);       
 FillHisto(Jet_Mj[sr][sys],   mj,    weight);
                FillHisto(Jet_Mjj[sr][sys],  mjj,   weight);

Njet25x  = this->getCutNumber(jetTLV, 25000.);
this->FillTree( DiLeptonTree[sr][sys], (bool)makeTree );
    
       /// Pass trigger
            bool passTrigger = true;
            switch (configYear)
            {
            case 2015:
                if (!isTriggered(triggerList2015, triggerInfo))
                    passTrigger = false;
                if (!isTriggeredMet(triggerList2015, EtMiss, triggerInfo))
                    passTrigger = false;
                break;
            case 2016:
                if (!isTriggeredMulti(triggerList2016, triggerInfo, true, data_period))
                    passTrigger = false;
                if (!isTriggeredMet2016(triggerList2016, EtMiss, triggerInfo, dataPeriod))
                    passTrigger = false;
                break;
            case 2017:
                if (!isTriggered(triggerList2017, triggerInfo))
                    passTrigger = false;
                if (!isTriggeredMet(triggerList2017, EtMiss, triggerInfo))
                    passTrigger = false;
                break;
            case 2018:
                if (!isTriggered(triggerList2018, triggerInfo))
                    passTrigger = false;
                if (!isTriggeredMet(triggerList2018, EtMiss, triggerInfo))
                    passTrigger = false;
                break;
            default:
                break;
            }

          if (Debug)
            {
                std::cout << "data_period" << data_period << std::endl;
            }


         if (!passTrigger)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "Trigger");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: trigger selection" << std::endl;
            }



/// Z boson reconstructed
/*
            /// Cut on signal leptons with certain pT cuts
            if (!hasLeptons(lepTLV, nLepCut))
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "geq2SigLept and  pT cut");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after SignalLep cut EventNumber " << EventNumber << Form("\t NlepS=%i", (int)lepTLV.size()) << std::endl;
            }
*/

            /// SS leptons selection
            bool SameSign = SSleptons(lepTLV, lepCharges);
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Lepton config = " << setLeptons << " \t found SS event = " << SameSign << std::endl;
            }
            if (!SameSign)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "geq2SSSigLep");
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after SS signal leptons cut" << std::endl;
            }

            /// Trigger matching
            bool trigMatched = this->isTrigMatch(configYear, GoodMuons, GoodElectrons, EtMiss, triggerInfo, true, dataPeriod, data_period,250000.);
            //if (!trigMatched || EtMiss<250000.)
            if (!trigMatched )
		continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "trigMatch");
            CutFlowIndex++;
            /// cutflow_file_SS << EventNumber << "\n";
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Passed Trigger Matching " << std::endl;
            }


	   /////////////////////////////////////////////////




        
            /// Apply trigger SF
            std::vector<float> triggerSFvec;
            if (isMC && trigMatched)
            {
                if (Debug)
                {
                    std::cout << APP_NAME << " DEBUG \t taking the trigger SF " << std::endl;
                }
                //triggerSFvec = this->getGlobTriggerSF(configYear, GoodMuons, GoodElectrons, EtMiss, triggerInfo, doSF, Debug);
                //triggerSF = triggerSFvec.at(0); /// only the nominal SF
	
	        //new tool
		triggerSF=SusyObjTool->GetTriggerGlobalEfficiencySF(*GoodElectrons, *GoodMuons, "diLepton");
		
            }
            else
            {
                triggerSF = 1;
            }
            if (triggerSF != 0)
                weight *= triggerSF;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Trigger SF " << triggerSF << "\t Event weight after trigger SF " << weight << std::endl;
            }

            /// Store truth info (actually a float saving if the lepton is real or not)
            lepTruth.resize(lepTLV.size());
            if (isMC)
            {
                lepTruth = this->getTruthInfo(lepTLV, GoodMuons, GoodElectrons);
                int lepsize = lepTruth.size();
                int match = 0;
                for (int i = 0; i < lepsize; i++)
                {
                    match += lepTruth[i];
                    if (Debug)
                    {
                        std::cout << APP_NAME << " DEBUG \t lepTruth[i] " << lepTruth[i] << std::endl;
                    }
                }
                if (match == lepsize)
                {
                    EventTM = 1;
                }
            }

            /// electrons, muons, b/jets weights
            ObjectSF = doSF ? this->getTotalSF(GoodMuons, GoodElectrons, GoodJets, /**theISOSFmu, theISOSFel, theECIDSSFel,*/ false) : 1.;
            if (ObjectSF != 0)
                weight *= ObjectSF;
            else
            {
                std::cout << APP_NAME << " ERROR \t Total SF of signal objects (electrons, muons, b/jets) is " << ObjectSF << "\t Event weight after SF is kept to " << weight << std::endl;
            }

            /// Store the nominal weights only for the nom TTree (it will be empty in the syst trees!)
            if (nom)
            {
                /// std::cout << APP_NAME << " DEBUG \t investigate the MCTemplate method SFs EventNumber "<< EventNumber << Form("\t NlepS=%i",(int)lepTLV.size()) << std::endl;
                muSF = getSF(GoodMuons, fakeLepCorr, MCId, weightFakeMu_MCTemplate, uncFakeMu_MCTemplate, doSF, false);
                elSF = getSF(GoodElectrons, fakeLepCorr, MCId, weightFakeEl_MCTemplate, uncFakeEl_MCTemplate /**, theECIDSSFel*/, doSF, false);
                FakeLepMCTemplateWeight = weightFakeMu_MCTemplate * weightFakeEl_MCTemplate;
                FakeMuMCTemplateUnc = uncFakeMu_MCTemplate;
                FakeEleMCTemplateUnc = uncFakeEl_MCTemplate;
                m_btagSFCentral = isMC ? SusyObjTool->BtagSF(GoodJets) : 1.;
                m_jvtSF = isMC ? SusyObjTool->JVT_SF(GoodJets) : 1.;
            }

            /// Set the weights for the syst
            if (doSys && doSF)
            {
                SysWeights = this->getSysWeights(GoodMuons, GoodElectrons, GoodJets, SysInfoList_W, SysNames, sys, false, data_period);
            }

            /// print some weights on the screen, to compare
            if (nom && (displ || Debug))
            {
                std::cout << APP_NAME << " DEBUG \t Printing Weights for: " << EventNumber << std::endl;
                std::cout << APP_NAME << " DEBUG \t Pileup: " << PileUpWeight << std::endl;
                std::cout << APP_NAME << " DEBUG \t Trig SF: " << triggerSF << std::endl;
                std::cout << APP_NAME << " DEBUG \t Lep SF: " << this->getTotalSF(GoodMuons, GoodElectrons, GoodJets /**, theISOSFmu, theISOSFel, theECIDSSFel*/) / (m_btagSFCentral * m_jvtSF) << std::endl;
                std::cout << APP_NAME << " DEBUG \t BtagSF: " << m_btagSFCentral << std::endl;
                std::cout << APP_NAME << " DEBUG \t jvtSF: " << m_jvtSF << std::endl;
                std::cout << APP_NAME << " DEBUG \t total fake Lep SFs: " << FakeLepMCTemplateWeight << " " << FakeMuMCTemplateUnc << " " << FakeEleMCTemplateUnc << std::endl;
                std::cout << APP_NAME << " DEBUG \t Total SF of signal objects " << ObjectSF << "\t Event weight after SF " << weight << std::endl;
            }

            /// Take the Good objects (signal = good => after baseline + signal)
            /// MUONS
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Final no. of good muons \t" << (int)muTLV.size() << std::endl;
            }
            float mu_sumPt(0.);
            TLorentzVector V_mu;
            for (std::vector<TLorentzVector>::iterator muItr = muTLV.begin(); muItr != muTLV.end(); muItr++)
            {
                if (Debug)
                    Printf("\t \t \t \t (Pt,Eta,Phi,E) = (%f, %f, %f, %f)", (*muItr).Pt(), (*muItr).Eta(), (*muItr).Phi(), (*muItr).E());
                mu_sumPt += (*muItr).Pt();
                V_mu += (*muItr);
                mu_n++;
            }
            /// ELECTRONS
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Final no.of good electrons \t" << (int)elTLV.size() << std::endl;
            }
            float el_sumPt(0.);
            TLorentzVector V_el;
            for (std::vector<TLorentzVector>::iterator elItr = elTLV.begin(); elItr != elTLV.end(); elItr++)
            {
                if (Debug)
                    Printf("\t \t \t \t (Pt,Eta,Phi,E) = (%f, %f, %f, %f)", (*elItr).Pt(), (*elItr).Eta(), (*elItr).Phi(), (*elItr).E());
                el_sumPt += (*elItr).Pt();
                V_el += (*elItr);
                el_n++;
            }
            /// LEPTONS
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Final no.of good leptons \t" << (int)lepTLV.size() << std::endl;
            }
            unsigned int Nl(0);
            float lep_sumPt(0.);
            for (std::vector<TLorentzVector>::iterator lepItr = lepTLV.begin(); lepItr != lepTLV.end(); lepItr++)
            {
                if (Debug)
                    Printf("\t \t \t \t (Pt,Eta,Phi,E) = (%f, %f, %f, %f) \t | truth(%i)", (*lepItr).Pt(), (*lepItr).Eta(), (*lepItr).Phi(), (*lepItr).E(), (int)lepTruth.at(Nl));
                lep_sumPt += (*lepItr).Pt();
                minv += (*lepItr).M(); /// all leptons mass
                lep_n++;
                Nl++;
            }
            /// JETS
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Final no. of good jets  \t" << (int)jetTLV.size() << std::endl;
            }
            unsigned int Nj(0);
            float jet_sumPt(0.);
            for (std::vector<TLorentzVector>::iterator jetItr = jetTLV.begin(); jetItr != jetTLV.end(); ++jetItr)
            {
                if (Debug)
                    Printf("\t \t \t \t (Pt,Eta,Phi,E) = (%f, %f, %f, %f) \t | isB(%i)", (*jetItr).Pt(), (*jetItr).Eta(), (*jetItr).Phi(), (*jetItr).E(), (int)jetBtag.at(Nj));
                bool passPtLF = (*jetItr).Pt() >= jet_PtCut[0];
                bool passPtHF = (*jetItr).Pt() >= jet_PtCut[1];
                if ((bool)jetBtag.at(Nj))
                {
                    if (passPtHF)
                        b_n++;
                    if (passPtLF)
                        jet_n++;
                }
                else
                {
                    if (passPtLF)
                        jet_n++;
                }
                jet_sumPt += (*jetItr).Pt();
                Nj++;
            }

            /// Set jet numbers
            Nbjet = b_n;

            Njet25 = this->getCutNumber(jetTLV, 25000.);
            Njet35 = this->getCutNumber(jetTLV, 35000.);
            Njet40 = this->getCutNumber(jetTLV, 40000.);
            Njet50 = this->getCutNumber(jetTLV, 50000.);
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t After jet pt requirement: " << Form("Njet25=%i, Njet35=%i, Njet40=%i, Njet50=%i, Nbjet=%i", Njet25, Njet35, Njet40, Njet50, Nbjet) << std::endl;
            }

            /// Computing kinematic quantities
            //meff = jet_sumPt + lep_sumPt + EtMiss;

            /// Cut on number of jets(bjets)
            int Njets = SignalRegion.JetPt == 50 ? Njet50 : (SignalRegion.JetPt == 40 ? Njet40 : (SignalRegion.JetPt == 35 ? Njet35 : Njet25));
            bool passJmin = Njets >= SignalRegion.Njet;
            bool passBmin = Nbjet >= SignalRegion.NBjet;
            bool passBmax = (SignalRegion.NBmax < 0 || Nbjet < SignalRegion.NBmax);

            /// ==========Cutflow - separating SS Channels========
            std::vector<TLorentzVector> SSTLV;
            SSTLV = !setLeptons ? SSpair(lepTLV, lepCharges) : (setLeptons == 1 ? SSpair(muTLV, muCharges) : SSpair(elTLV, elCharges));
            if (Debug && SameSign == true)
            {
                std::cout << APP_NAME << " DEBUG \t SS1(Pt,eta,phi,E): (" << SSTLV.at(0).Pt() << ", " << SSTLV.at(0).Eta() << ", " << SSTLV.at(0).Phi() << ", " << SSTLV.at(0).E() << ") \n"
                          << APP_NAME << " DEBUG \t SS2(Pt,eta,phi,E): (" << SSTLV.at(1).Pt() << ", " << SSTLV.at(1).Eta() << ", " << SSTLV.at(1).Phi() << ", " << SSTLV.at(1).E() << ")"
                          << std::endl;
            }
            SSChannel = (SameSign == true) ? this->getChannel(SSTLV, muTLV, elTLV, setLeptons) : 0;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t SSChannel = " << SSChannel << std::endl;
            }

            if (SSChannel == 1)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom);
                hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "chSSMuMu");
            }
            CutFlowIndex++;
            if (SSChannel == 1 && Nbjet > 0)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom); /**cutflow_file_mumu_bjet << EventNumber << "\n";*/
            }
            CutFlowIndex++;
            if (SSChannel == 1 && Nbjet > 0 && Njet50 > 3)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom); /**cutflow_file_mumu_jet << EventNumber << "\n";*/
            }
            CutFlowIndex++;
            if (SSChannel == 1 && Nbjet > 0 && Njet50 > 3 && EtMiss > 125000)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom); /**cutflow_file_mumu_MET << EventNumber << "\n";*/
            }
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after at least one b-jet cut" << std::endl;
            }

            if (SSChannel == 2)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom);
                /// cutflow_file_ee << EventNumber << "\n";
            }
            CutFlowIndex++;
            if (SSChannel == 2 && Nbjet > 0)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom); /**cutflow_file_ee_bjet << EventNumber << "\n";*/
            }
            CutFlowIndex++;
            if (SSChannel == 2 && Nbjet > 0 && Njet50 > 3)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom); /**cutflow_file_ee_jet << EventNumber << "\n";*/
            }
            CutFlowIndex++;
            if (SSChannel == 2 && Nbjet > 0 && Njet50 > 3 && EtMiss > 125000)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom); /**cutflow_file_ee_MET << EventNumber << "\n"; */
            }
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after b-jet and jet cuts" << std::endl;
            }

            if (SSChannel == 3)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom);
                /// cutflow_file_emu << EventNumber << "\n";
            }
            CutFlowIndex++;
            if (SSChannel == 3 && Nbjet > 0)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom); /**cutflow_file_emu_bjet << EventNumber << "\n";*/
            }
            CutFlowIndex++;
            if (SSChannel == 3 && Nbjet > 0 && Njet50 > 3)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom); /**cutflow_file_emu_jet << EventNumber << "\n";*/
            }
            CutFlowIndex++;
            if (SSChannel == 3 && Nbjet > 0 && Njet50 > 3 && EtMiss > 125000)
            {
                FillHisto(hCutflow, CutFlowIndex, _nom); /**cutflow_file_emu_MET << EventNumber << "\n";*/
            }
            CutFlowIndex++;
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Cutflow:: after b-jet, jet and met cuts" << std::endl;
            }
            if (!passJmin || !passBmin || !passBmax)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            hCutflow->GetXaxis()->SetBinLabel(CutFlowIndex + 1, "pass Jmin, Bmin, Bmax");
            CutFlowIndex++; /// index 28 here

            /// MET,Meff Cut
            float MeffCut = SignalRegion.MeffCut;
            float EtMissCut = SignalRegion.EtMissCut;
            if (EtMiss < EtMissCut)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            CutFlowIndex++; /// index 29 here
            if (meff < MeffCut)
                continue;
            FillHisto(hCutflow, CutFlowIndex, _nom);
            CutFlowIndex++; /// index 30 here

            topoStr = Form("%im_%ie_%il_%ij(20)_%ib(20)", mu_n, el_n, lep_n, jet_n, b_n);

            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t Event reaches SR " << SignalRegion.Name << " | Topology after cuts " << topoStr << std::endl;
            }
/*
            /// Filling histos
            if (makeHist && !sys)
            {
                ht = jet_sumPt;
                mt = this->getMt(lepTLV, EtMiss, EtMissPhi);
                if (Debug)
                {
                    std::cout << APP_NAME << " DEBUG \t " << Form("EtMiss= %.0f | Ht= %.0f | Meff= %.0f | Mt= %.0f", EtMiss, ht, meff, mt) << std::endl;
                }

                FillHisto(hEventWeightSF[sr][sys], weight);
                FillHisto(NMu[sr][sys], mu_n, weight);
                FillHisto(NMu_EWcombi[sr][sys], mu_n_EWcombi, weight);
                FillHisto(NEl[sr][sys], el_n, weight);
                FillHisto(NEl_EWcombi[sr][sys], el_n_EWcombi, weight);
                FillHisto(NJet[sr][sys], jet_n, weight);
                FillHisto(NBJet[sr][sys], b_n, weight);

                FillHisto(Mu_1stPt[sr][sys], mu_1stPt, weight);
                FillHisto(Mu_2ndPt[sr][sys], mu_2ndPt, weight);
                FillHisto(Mu_3rdPt[sr][sys], mu_3rdPt, weight);
                FillHisto(El_1stPt[sr][sys], el_1stPt, weight);
                FillHisto(El_2ndPt[sr][sys], el_2ndPt, weight);
                FillHisto(El_3rdPt[sr][sys], el_3rdPt, weight);
                FillHisto(Lep_1stPt[sr][sys], lep_1stPt, weight);
                FillHisto(Lep_2ndPt[sr][sys], lep_2ndPt, weight);
                FillHisto(Lep_3rdPt[sr][sys], lep_3rdPt, weight);
                FillHisto(Jet_1stPt[sr][sys], jet_1stPt, weight);
                FillHisto(Jet_2ndPt[sr][sys], jet_2ndPt, weight);
                FillHisto(Mu_1stEta[sr][sys], mu_1stEta, weight);
                FillHisto(Mu_2ndEta[sr][sys], mu_2ndEta, weight);
                FillHisto(El_1stEta[sr][sys], el_1stEta, weight);
                FillHisto(El_2ndEta[sr][sys], el_2ndEta, weight);

                FillHisto(Jet_Mj[sr][sys], mj, weight);
                FillHisto(Jet_Mjj[sr][sys], mjj, weight);
                FillHisto(Jet_MjjdR[sr][sys], mjjdR, weight);
                FillHisto(Jet_MjRec[sr][sys], mjRec, weight);
                for (unsigned int i(0); i < jetTLV.size(); i++)
                    FillHisto(Jet_MjAll[sr][sys], (jetTLV.at(i)).M(), weight);
                for (unsigned int i(0); i < jetTLV_R1.size(); i++)
                    FillHisto(Jet_MjRecAll[sr][sys], (jetTLV_R1.at(i)).M(), weight);

                FillHisto(Meff[sr][sys], meff, weight);
                FillHisto(Minv[sr][sys], minv, weight);
                FillHisto(MT[sr][sys], mt, weight);
                FillHisto(HT[sr][sys], ht, weight);
                FillHisto(ETmiss[sr][sys], EtMiss, weight);

                FillHisto(Mu_SumPt[sr][sys], mu_sumPt, weight);
                FillHisto(El_SumPt[sr][sys], el_sumPt, weight);
                FillHisto(Jet_SumPt[sr][sys], jet_sumPt, weight);
            }
*/
            /// Fill TTree
            this->FillTree(DiLeptonTree[sr][sys], (bool)makeTree);

            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t End SYS loop" << std::endl;
            }
        }
        if (Debug)
        {
            std::cout << APP_NAME << " DEBUG \t End SR loop" << std::endl;
        }
    }

    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t End execute() event" << std::endl;
    }
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiLepton::postExecute()
{

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiLepton::finalize()
{
    const char *APP_NAME = "finalize()";

    Info(APP_NAME, "Printing Cutflow");
    for (unsigned int sr(0); sr < NSr; ++sr)
    {
        std::cout << "Signal Region " << SignalRegions.at(sr).Name << std::endl;
        PrintCutflow(hCutflow);
    }
    /*
    cutflow_file_trig.close();
    cutflow_file_baseline.close();
    cutflow_file_all.close();
    cutflow_file_EWcombi.close();
    cutflow_file_SS.close();
    cutflow_file_mumu.close();
    cutflow_file_ee.close();
    cutflow_file_emu.close();
    cutflow_file_badmu.close();
    cutflow_file_pvt.close();
    cutflow_file_jetOR.close();
    cutflow_file_badjet.close();
    cutflow_file_sjet.close();
    cutflow_file_grl.close();
    cutflow_file_flags.close();
    cutflow_file_ee_bjet.close();
    cutflow_file_emu_bjet.close();
    cutflow_file_mumu_bjet.close();
    cutflow_file_ee_jet.close();
    cutflow_file_emu_jet.close();
    cutflow_file_mumu_jet.close();
    cutflow_file_ee_MET.close();
    cutflow_file_emu_MET.close();
    cutflow_file_mumu_MET.close();
    */
    Info(APP_NAME, "Deleting all tools");
    if (MCClassifier)
    {
        delete MCClassifier;
        MCClassifier = 0;
    }

    Info(APP_NAME, "All done, bye!");
    if (false)
    {
        int Nsr = 1.;
        int Nsys = 0;
        for (int sr = 0; sr < Nsr; sr++)
            for (int sys = 0; sys < 1; sys++)
                DiLeptonTree[sr][sys]->Print();
    }

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiLepton::histFinalize()
{
    return EL::StatusCode::SUCCESS;
}

void DiLepton::FillTree(TTree *t, bool fill)
{
    if (!fill || !t)
        return;
    if (Debug)
    {
        std::cout << "DiLepton::FillTree()"
                  << " DEBUG \t filling TTree " << t->GetName() << std::endl;
    }
    t->Fill();

    return;
}
///////////////////////////////////////////////////

  std::vector<std::string> DiLepton::split_elements(const std::string& s, const std::string& delim) {
  assert(delim.length() == 1);
  std::vector<std::string> retval;
  retval.reserve(std::count(s.begin(), s.end(), delim[0]) + 1);

  if (s.find(delim)==std::string::npos) {
     retval.emplace_back(s);
     return retval;
  }
  
  size_t last = 0;
  size_t next = 0;
  bool gothere=false;
  while ((next = s.find(delim, last)) != std::string::npos) {
    retval.emplace_back(s.substr(last, next - last));
    last = next + delim.length();
    gothere=true;
  }
  if(gothere)
    retval.emplace_back(s.substr(last));

  return retval;
}







//////////////////////////////////////////////////////
void DiLepton::FillHisto(TH1 *h, float x, float w, bool fill)
{
    if (!fill)
        return;
    if (h)
        h->Fill(x, w);

    return;
}

void DiLepton::FillHisto(TH2 *h, float x, float y, float w, bool fill)
{
    if (!fill)
        return;
    if (h)
        h->Fill(x, y, w);

    return;
}

void DiLepton::setROOTmsg(int level)
{
    switch (level)
    {
    case 0:
        gErrorIgnoreLevel = kFatal;
        break;
    case 1:
        gErrorIgnoreLevel = kUnset;
        break;
    default:
        break;
    }

    return;
}

bool DiLepton::containStr(std::vector<std::string> v, std::string name)
{
    bool cont(false);
    for (std::vector<std::string>::iterator it = v.begin(); it != v.end(); it++)
    {
        if (*it == name)
            cont = true;
    }

    return cont;
}

std::vector<std::string> DiLepton::tokenize(std::string s)
{
    std::vector<std::string> v;
    if (!(bool)s.length())
        return v;

    TObjArray *strings = ((TString)s).Tokenize(",");
    for (int i(0); i < strings->GetEntries(); i++)
    {
        //   TString part = ((TObjString*)strings->At(i))->GetString();
        TString part = ((TSubString *)strings->At(i))->String();
        part.ReplaceAll(" ", "");
        v.push_back((std::string)part);
    }

    return v;
}

bool DiLepton::match(TLorentzVector v1, TLorentzVector v2)
{
    if (v1.Pt() < 1E-4 || v2.Pt() < 1E-4)
        return false;
    if (dR(v1, v2) < 1E-4 && TMath::Abs(v1.Pt() - v2.Pt()) < 1E-4)
        return true;

    else
        return false;
}

int DiLepton::getCutNumber(std::vector<TLorentzVector> particles, float ptcut)
{
    int n(0);
    std::vector<TLorentzVector>::iterator p = particles.begin();
    std::vector<TLorentzVector>::iterator E = particles.end();
    for (; p != E; ++p)
    {
        if ((*p).Pt() > ptcut)
            n++;
    }

    return n;
}

bool DiLepton::addSR(TString name, int jetmin, int bmin, int bmax, float jetpt, float metmin, float meffmin, TString pairing)
{
    SR sr;
    sr.Name = name;
    sr.Njet = jetmin;
    sr.NBjet = bmin;
    sr.NBmax = bmax;
    sr.JetPt = jetpt;
    sr.EtMissCut = metmin;
    sr.MeffCut = meffmin;
    sr.Pairing = pairing;

    SignalRegions.push_back(sr);
    this->PrintSR(sr, "addSR()");

    return true;
}

void DiLepton::PrintSR(SR sr, const char *APP_NAME)
{
    std::cout << "DiLepton::PrintSR() " << APP_NAME << "\t SR::"
              << Form("Name %s, Njet %i, NBjet %i, NBmax %i, JetPt %.0f, EtMissCut %.0f, MeffCut %.0f, Pairing %s",
                      sr.Name.Data(), sr.Njet, sr.NBjet, sr.NBmax, sr.JetPt, sr.EtMissCut, sr.MeffCut, sr.Pairing.Data())
              << std::endl;

    return;
}

void DiLepton::PrintCutflow(TH1 *hCut)
{
    std::cout << " Cutflow summary: " << std::endl;
    std::cout << "============================" << std::endl;
    std::cout << " Events Total   \t" << (int)hCut->GetBinContent(1) << std::endl;
    std::cout << " MC Truth veto  \t" << (int)hCut->GetBinContent(2) << std::endl;
    std::cout << " GoodRunList    \t" << (int)hCut->GetBinContent(3) << std::endl;
    std::cout << " Primary vertex \t" << (int)hCut->GetBinContent(4) << std::endl;
    std::cout << " Global flags   \t" << (int)hCut->GetBinContent(5) << std::endl;
    std::cout << " Trigger        \t" << (int)hCut->GetBinContent(6) << std::endl;
    std::cout << " Bad muons veto \t" << (int)hCut->GetBinContent(7) << std::endl;
    std::cout << " 1 jet pass OR  \t" << (int)hCut->GetBinContent(8) << std::endl;
    std::cout << " Bad jet veto   \t" << (int)hCut->GetBinContent(9) << std::endl;
    std::cout << " 1 signal jet   \t" << (int)hCut->GetBinContent(10) << std::endl;
    std::cout << " Cosmics veto   \t" << (int)hCut->GetBinContent(11) << std::endl;
    std::cout << " >=2 baseline lep.\t" << (int)hCut->GetBinContent(12) << std::endl;
    std::cout << " >=2 signal lep.  \t" << (int)hCut->GetBinContent(13) << std::endl;
    // std::cout << " >=2 EW combi lep.\t"<< (int)hCut->GetBinContent(14) << std::endl;
    if (doBJetVeto != 0)
        std::cout << " b-jet veto applied starting with this step! " << std::endl;
    std::cout << " SS leptons     \t" << (int)hCut->GetBinContent(14) << std::endl;
    std::cout << " Trigger match  \t" << (int)hCut->GetBinContent(15) << std::endl;
    std::cout << " mumu channel   \t" << (int)hCut->GetBinContent(16) << std::endl;
    std::cout << " mumu NBjets>0  \t" << (int)hCut->GetBinContent(17) << std::endl;
    std::cout << " mumu NJets>3   \t" << (int)hCut->GetBinContent(18) << std::endl;
    std::cout << " mumu MET>125   \t" << (int)hCut->GetBinContent(19) << std::endl;
    std::cout << " ee channel     \t" << (int)hCut->GetBinContent(20) << std::endl;
    std::cout << " ee NBjets>0    \t" << (int)hCut->GetBinContent(21) << std::endl;
    std::cout << " ee NJets>3     \t" << (int)hCut->GetBinContent(22) << std::endl;
    std::cout << " ee MET>125     \t" << (int)hCut->GetBinContent(23) << std::endl;
    std::cout << " emu channel    \t" << (int)hCut->GetBinContent(24) << std::endl;
    std::cout << " emu NBjets>0   \t" << (int)hCut->GetBinContent(25) << std::endl;
    std::cout << " emu NJets>3    \t" << (int)hCut->GetBinContent(26) << std::endl;
    std::cout << " emu MET>125    \t" << (int)hCut->GetBinContent(27) << std::endl;
    std::cout << "============================" << std::endl;
    std::cout << std::endl;
}

void DiLepton::PrintInfo(const xAOD::Muon *mu, const char *APP_NAME, bool msg)
{
    if (!msg)
        return;
    if (!mu)
    {
        Info(APP_NAME, "WARNING: No PrintInfo() for NullPtr");
        return;
    }
    try
    {
        std::cout << APP_NAME << " DEBUG "
                  << Form("\t Muon Pt|Eta|Phi|C \t %.2f | %.3f | %.3f | %.0f", mu->pt(), mu->eta(), mu->phi(), mu->charge())
                  << Form("\t isBL|OR|isSig|OK %i | %i | %i | %i", (int)mu->auxdataConst<char>("baseline"), (int)mu->auxdataConst<char>("passOR"), (int)mu->auxdataConst<char>("signal"), (int)mu->auxdataConst<char>("SIG"))
                  << Form("\t isCosmic|isBad %i | %i", (int)mu->auxdataConst<char>("cosmic"), (int)mu->auxdataConst<char>("bad")) << std::endl;
    }
    catch (SG::ExcBadAuxVar &)
    {
        Info(APP_NAME, "WARNING: Incomplete decoration of muon");
    }

    return;
}

void DiLepton::PrintInfo(const xAOD::Electron *el, const char *APP_NAME, bool msg)
{
    if (!msg)
        return;
    if (!el)
    {
        Info(APP_NAME, "WARNING: No PrintInfo() for NullPtr");
        return;
    }
    try
    {
        std::cout << APP_NAME << " DEBUG "
                  << Form("\t Electron Pt|Eta|Phi|C \t %.2f | %.3f | %.3f | %.0f", el->pt(), el->eta(), el->phi(), el->charge())
                  << Form("\t isBL|OR|isSig|OK %i | %i | %i | %i", (int)el->auxdataConst<char>("baseline"), (int)el->auxdataConst<char>("passOR"), (int)el->auxdataConst<char>("signal"), (int)el->auxdataConst<char>("SIG"))
                  << std::endl;
    }
    catch (SG::ExcBadAuxVar &)
    {
        Info(APP_NAME, "WARNING: Incomplete decoration of electron");
    }

    return;
}

void DiLepton::PrintInfo(const xAOD::Jet *jet, const char *APP_NAME, bool msg)
{
    if (!msg)
        return;
    if (!jet)
    {
        Info(APP_NAME, "WARNING: No PrintInfo() for NullPtr");
        return;
    }
    try
    {
        std::cout << APP_NAME << " DEBUG "
                  << Form("\t Jet Pt|Eta|Phi \t %.2f | %.3f | %.3f", jet->pt(), jet->eta(), jet->phi())
                  << Form("\t isBL|OR|isSig|OK %i | %i | %i | %i", (int)jet->auxdataConst<char>("baseline"), (int)jet->auxdataConst<char>("passOR"), (int)jet->auxdataConst<char>("signal"), (int)jet->auxdataConst<char>("SIG"))
                  << Form("\t isB|isBad|Jvt %i | %i | %.2f", (int)jet->auxdataConst<char>("bjet"), (int)jet->auxdataConst<char>("bad"), (float)jet->auxdataConst<float>("Jvt")) << std::endl;
    }
    catch (SG::ExcBadAuxVar &)
    {
        Info(APP_NAME, "WARNING: Incomplete decoration of jet");
    }

    return;
}

void DiLepton::PrintInfo(const xAOD::Photon *ph, const char *APP_NAME, bool msg)
{
    if (!msg)
        return;
    if (!ph)
    {
        Info(APP_NAME, "WARNING: No PrintInfo() for NullPtr");
        return;
    }
    try
    {
        std::cout << APP_NAME << " DEBUG "
                  << Form("\t Photon Pt|Eta|Phi \t %.2f | %.3f | %.3f", ph->pt(), ph->eta(), ph->phi()) << std::endl;
    }
    catch (SG::ExcBadAuxVar &)
    {
        Info(APP_NAME, "WARNING: Incomplete decoration of photon");
    }

    return;
}

bool DiLepton::setTriggers(std::vector<std::string> &trigList, std::string trigname, int option)
{
    const char *APP_NAME = "setTrigger()";

    // if(isMC) ANA_CHECK( SusyObjTool->ApplyPRWTool() );
    if (isMC && !isPHYSLITE)
        ANA_CHECK(SusyObjTool->ApplyPRWTool());

    if (Debug)
    {
        std::cout << "Trigname: " << trigname << std::endl;
    }

    /// No trigger
    if (!trigname.length() || !option)
    {
        Info(APP_NAME, "No trigger or option selected, returning 1");
        return true;
    }

    /// Set only one trigger
    if (((TString)trigname).Contains("HLT_"))
    {
        trigList.push_back(trigname);
        Info(APP_NAME, "Asking only = %s", trigname.c_str());
        return true;
    }

    /// Set DiLepton trigger
    if (trigname == "DILEPTON")
    {
        switch (option)
        {
        case 2015:
            trigList.push_back("HLT_mu18_mu8noL1");
	    trigList.push_back("HLT_2mu10");
            trigList.push_back("HLT_2e12_lhloose_L12EM10VH");
            trigList.push_back("HLT_e17_lhloose_mu14");
            break;
        case 2016:
            trigList.push_back("HLT_mu22_mu8noL1");
	    trigList.push_back("HLT_2mu14");
	    //trigList.push_back("HLT_2mu14_nomucomb");
	    trigList.push_back("HLT_mu20_mu8noL1");
	    trigList.push_back("HLT_2mu10");
            trigList.push_back("HLT_2e17_lhvloose_nod0");
            trigList.push_back("HLT_e17_lhloose_nod0_mu14");
            break;
        case 2017:
            trigList.push_back("HLT_2e24_lhvloose_nod0");
            trigList.push_back("HLT_e17_lhloose_nod0_mu14");
            trigList.push_back("HLT_2mu14");
            trigList.push_back("HLT_mu22_mu8noL1");
	    break;
        case 2018:
            trigList.push_back("HLT_2e24_lhvloose_nod0");
            trigList.push_back("HLT_e17_lhloose_nod0_mu14");
            trigList.push_back("HLT_2mu14");
	    trigList.push_back("HLT_mu22_mu8noL1");
            break;
        default:
            break;
        }
    }

    /// Set DiLepton+MET trigger - use when L1_2EM15VHI is not unprescaled
    if (trigname == "DILEPTON_MET")
    {
        switch (option)
        {
        case 2015:
            trigList.push_back("HLT_xe70");
            trigList.push_back("HLT_mu18_mu8noL1");
            trigList.push_back("HLT_2mu10");
	    trigList.push_back("HLT_2e12_lhloose_L12EM10VH");
            trigList.push_back("HLT_e17_lhloose_mu14");
            break;
        case 2016:
            trigList.push_back("HLT_xe90_mht_L1XE50");
            trigList.push_back("HLT_xe100_mht_L1XE50");
            trigList.push_back("HLT_xe110_mht_L1XE50");
            trigList.push_back("HLT_mu22_mu8noL1");
	    trigList.push_back("HLT_2mu14");
	    trigList.push_back("HLT_2mu10");
            //trigList.push_back("HLT_2mu14_nomucomb");
            trigList.push_back("HLT_mu20_mu8noL1");
            trigList.push_back("HLT_2e17_lhvloose_nod0");
            trigList.push_back("HLT_e17_lhloose_nod0_mu14");
            break;
        case 2017:
            trigList.push_back("HLT_xe110_pufit_L1XE55");
            trigList.push_back("HLT_2e24_lhvloose_nod0");
            trigList.push_back("HLT_e17_lhloose_nod0_mu14");
            trigList.push_back("HLT_mu22_mu8noL1");
	    trigList.push_back("HLT_2mu14");
            break;
        case 2018:
            trigList.push_back("HLT_xe110_pufit_xe70_L1XE50");
            trigList.push_back("HLT_2e24_lhvloose_nod0");
            trigList.push_back("HLT_e17_lhloose_nod0_mu14");
            trigList.push_back("HLT_2mu14");
	    trigList.push_back("HLT_mu22_mu8noL1");
            break;
        default:
            break;
        }
    }

    Info(APP_NAME, "Configured trigger option %i", option);
    for (unsigned int t(0); t < trigList.size(); t++)
        Info(APP_NAME, "Asking trigger = %s", (trigList.at(t)).c_str());

    return true;
}

bool DiLepton::isTriggered(std::vector<std::string> trigList, std::vector<std::string> &trigInfo, bool enabled)
{
    if (!enabled || trigList.empty())
        return true;
    bool trigPass(false);
    for (const auto &trigger : trigList)
    {
        if (SusyObjTool->IsTrigPassed(trigger))
        {
            if (Debug)
            {
                std::cout << "DiLepton::Trigger()"
                          << " DEBUG \t Event passed trigger = " << trigger << std::endl;
            }
            trigInfo.push_back(trigger);
            trigPass = true;
        }
    }

    return trigPass;
}



std::vector<std::string> DiLepton::Period_List_Vectors(int  data_period){
    std::vector<std::string> trigList ; 
    switch (data_period)
        {
        case 1:
            trigList.push_back("HLT_mu20_mu8noL1");
            trigList.push_back("HLT_2mu10");
            break;
        case 2:
            trigList.push_back("HLT_mu20_mu8noL1");
            trigList.push_back("HLT_2mu14");
	    break;
	case 3:
            trigList.push_back("HLT_mu22_mu8noL1");
            trigList.push_back("HLT_2mu14");
 	     break;
        default:
            break;
        }
    return trigList;
}



std::array<float,3> pt_2016_array(int  data_period){
    std::array<float,3> pt_array;
    for (int i=0;i<pt_array.size();i++){
       pt_array[i]=0;	
    }
    switch (data_period)
        {
        case 1:
           // trigList.push_back("HLT_mu20_mu8noL1");
           // trigList.push_back("HLT_2mu10");
            pt_array[0]=21000.;
            pt_array[1]=10000.;
	    pt_array[2]=11000.;
	    break;
        case 2:
            //trigList.push_back("HLT_mu20_mu8noL1");
            //trigList.push_back("HLT_2mu14");
	    pt_array[0]=21000.;
            pt_array[1]=10000.;
            pt_array[2]=15000.;
	    break;
        case 3:
            //trigList.push_back("HLT_mu22_mu8noL1");
            //trigList.push_back("HLT_2mu14");
            pt_array[0]=23000.;
            pt_array[1]=10000.;
            pt_array[2]=15000.;
	    break;
        default:
            break;
        }
    return pt_array;
}




bool DiLepton::HLT_mu_Trigg(std::string trigInfo)
{
    std::string metKey1("HLT_mu");
    std::string metKey2("HLT_2mu");
    bool nTrig;
    nTrig=false;

    std::size_t found1 = trigInfo.find(metKey1);
    std::size_t found2 = trigInfo.find(metKey2);

        if (found1 != std::string::npos || found2 != std::string::npos)
            nTrig=true;
   
    return nTrig;
}




bool DiLepton::isTriggeredMulti(std::vector<std::string> trigList, std::vector<std::string> &trigInfo, bool enabled, int data_period)
{
    if (!enabled || trigList.empty())
        return true;
   bool trigPass(false);
   
   std::vector<std::string> lista_trig = Period_List_Vectors(data_period);

   for (const auto &trigger : trigList){
    if(HLT_mu_Trigg(trigger)){
    	for (const auto &trig : lista_trig){
           if( trig.compare(trigger)==0){   
	   	if (SusyObjTool->IsTrigPassed(trigger)){
		    trigInfo.push_back(trigger);
        	    trigPass = true;
                    if (Debug)
                    {
                      std::cout << "DiLepton::Trigger()" << " DEBUG \t Event passed trigger = " << trigger << std::endl;
                    }
                }
	   }
	}
    }
    else{
	if (SusyObjTool->IsTrigPassed(trigger)){
                    trigInfo.push_back(trigger);
                    trigPass = true;
                    if (Debug)
                    {
                      std::cout << "DiLepton::Trigger()" << " DEBUG \t Event passed trigger = " << trigger << std::endl;
                    }
                }

  	}
    
}
    return trigPass;
}

bool DiLepton::isTriggeredMet(std::vector<std::string> trigList, float met, std::vector<std::string> trigInfo, float metcut)
{
    if (trigList.empty())
        return true;
    if (trigInfo.empty())
        return false;
    if (met > metcut)
        return true;

    if ((int)trigInfo.size() == this->nMetTrigger(trigInfo))
    {
        if (Debug)
        {
            std::cout << "DiLepton::Trigger()"
                      << " DEBUG \t " << Form("EtMiss < %.0f and only MET triggers are fired, return %i", metcut, 0) << std::endl;
        }
        return false;
    }

    return true;
}

bool DiLepton::isTriggeredMet2016(std::vector<std::string> trigList, float met, std::vector<std::string> trigInfo, int period, float metcut)
{
    if (trigList.empty())
        return true;
    if (trigInfo.empty())
        return false;

    int totalMETPass = this->nMetTrigger_xe90(trigInfo) + this->nMetTrigger_xe100(trigInfo) + this->nMetTrigger_xe110(trigInfo);
    if (totalMETPass == (int)trigInfo.size() && met < metcut)
        return false;
    if ((int)trigInfo.size() > totalMETPass)
        return true;

    if (Debug)
    {
        std::cout << "Still in MET Trigger" << std::endl;
    }

    if (period == 1 && met > metcut && this->nMetTrigger_xe90(trigInfo) > 0)
    {
        if (Debug)
        {
            std::cout << "DiLepton::Trigger()"
                      << " DEBUG \t " << Form("EtMiss > %.0f and MET trigger are fired, return %i", metcut, 0) << " - Period 1" << std::endl;
        }
        return true;
    }
    if (period == 2 && met > metcut && this->nMetTrigger_xe100(trigInfo) > 0)
    {
        if (Debug)
        {
            std::cout << "DiLepton::Trigger()"
                      << " DEBUG \t " << Form("EtMiss > %.0f and MET trigger are fired, return %i", metcut, 0) << " - Period 2" << std::endl;
        }
        return true;
    }
    if (period == 3 && met > metcut && this->nMetTrigger_xe110(trigInfo) > 0)
    {
        if (Debug)
        {
            std::cout << "DiLepton::Trigger()"
                      << " DEBUG \t " << Form("EtMiss > %.0f and MET trigger are fired, return %i", metcut, 0) << " - Period 3" << std::endl;
        }
        return true;
    }

    return false;
}

void DiLepton::setTrigItem(std::vector<std::string> &trigList, std::string trigItem, unsigned int it)
{
    if (trigList.size() < it + 1)
        return;
    trigList[it] = trigItem;

    return;
}

int DiLepton::nMetTrigger(std::vector<std::string> trigInfo)
{
    std::string metKey("HLT_xe");
    unsigned int nTrig(0);
    for (unsigned int tr(0); tr < trigInfo.size(); tr++)
    {
        if ((trigInfo.at(tr)).find(metKey) != std::string::npos)
            nTrig++;
    }

    return (int)nTrig;
}

int DiLepton::nMetTrigger_xe90(std::vector<std::string> trigInfo)
{
    std::string metKey("HLT_xe90");
    unsigned int nTrig(0);
    for (unsigned int tr(0); tr < trigInfo.size(); tr++)
    {
        if ((trigInfo.at(tr)).find(metKey) != std::string::npos)
            nTrig++;
    }

    return (int)nTrig;
}

int DiLepton::nMetTrigger_xe100(std::vector<std::string> trigInfo)
{
    std::string metKey("HLT_xe100");
    unsigned int nTrig(0);
    for (unsigned int tr(0); tr < trigInfo.size(); tr++)
    {
        if ((trigInfo.at(tr)).find(metKey) != std::string::npos)
            nTrig++;
    }

    return (int)nTrig;
}

int DiLepton::nMetTrigger_xe110(std::vector<std::string> trigInfo)
{
    std::string metKey("HLT_xe110");
    unsigned int nTrig(0);
    for (unsigned int tr(0); tr < trigInfo.size(); tr++)
    {
        if ((trigInfo.at(tr)).find(metKey) != std::string::npos)
            nTrig++;
    }

    return (int)nTrig;
}

bool DiLepton::isTrigMatch(int yearOpt, xAOD::MuonContainer *muons, xAOD::ElectronContainer *electrons, float met, std::vector<std::string> trigInfo, bool doMatch, int period, int data_period, float metcut)
{
    const char *APP_NAME = "DiLepton::TriggerMatch()";

    bool pass(true);
    if (!doMatch)
        return pass;
    std::vector<std::string> trigList;
    std::string MuMuItem,MuMuItema, ElElItem, ElMuItem, ElElItem2;
    float ptCutMuMu(0),ptCutMuMua(0), ptCutElEl(0), ptCutElMu(0), ptCutMuMu2(0);


    switch (yearOpt)
    {
    case 2015:
        trigList = triggerList2015;
        MuMuItem = "HLT_mu18_mu8noL1";
     	MuMuItema = "HLT_2mu10";   
	ElElItem = "HLT_2e12_lhloose_L12EM10VH";
        ElMuItem = "HLT_e17_lhloose_mu14";
        ptCutMuMu = 20000.;
	ptCutMuMua = 11000.;
        ptCutMuMu2 = 10000.;
        ptCutElEl = 20000.;
        ptCutElMu = 20000.;
        if (met > metcut && this->nMetTrigger(trigInfo))
        {
            if (Debug)
            {
                std::cout << APP_NAME << Form(" DEBUG \t EtMiss > %.0f and event is triggered by %i EtMiss trigger, return %i", metcut, this->nMetTrigger(trigInfo), pass) << std::endl;
            }
            return pass;
        }
        break;
   /* case 2016:
        trigList = triggerList2016;
        MuMuItem = "HLT_mu22_mu8noL1";
        ElElItem = "HLT_2e17_lhvloose_nod0";
        ElMuItem = "HLT_e17_lhloose_nod0_mu14";
        ptCutMuMu = 23000.;
        ptCutMuMu2 = 10000.;
        ptCutElEl = 20000.;
        ptCutElMu = 20000.;
        if (period == 1 && met > metcut && this->nMetTrigger_xe90(trigInfo))
        {
            if (Debug)
            {
                std::cout << APP_NAME << Form(" DEBUG \t EtMiss > %.0f and event is triggered by %i EtMiss trigger, return %i", metcut, this->nMetTrigger_xe90(trigInfo), pass) << std::endl;
            }
            return pass;
        }
        if (period == 2 && met > metcut && this->nMetTrigger_xe100(trigInfo))
        {
            if (Debug)
            {
                std::cout << APP_NAME << Form(" DEBUG \t EtMiss > %.0f and event is triggered by %i EtMiss trigger, return %i", metcut, this->nMetTrigger_xe100(trigInfo), pass) << std::endl;
            }
            return pass;
        }
        if (period == 3 && met > metcut && this->nMetTrigger_xe110(trigInfo))
        {
            if (Debug)
            {
                std::cout << APP_NAME << Form(" DEBUG \t EtMiss > %.0f and event is triggered by %i EtMiss trigger, return %i", metcut, this->nMetTrigger_xe110(trigInfo), pass) << std::endl;
            }
            return pass;
        }
        break;
*/    

    case 2016:
        trigList = triggerList2016;
        MuMuItem = Period_List_Vectors(data_period)[0];
	MuMuItema = Period_List_Vectors(data_period)[1];
        std::cout << "MuMuItem: "<<MuMuItem<<std::endl;
	std::cout << "MuMuItema: "<<MuMuItema<<std::endl;
	ElElItem = "HLT_2e17_lhvloose_nod0";
        ElMuItem = "HLT_e17_lhloose_nod0_mu14";
        ptCutMuMu =  pt_2016_array(data_period)[0];
	ptCutMuMua =  pt_2016_array(data_period)[2];
        ptCutMuMu2 =  pt_2016_array(data_period)[1];
        ptCutElEl = 20000.;
        ptCutElMu = 20000.;
        if (period == 1 && met > metcut && this->nMetTrigger_xe90(trigInfo))
        {
            if (Debug)
            {
                std::cout << APP_NAME << Form(" DEBUG \t EtMiss > %.0f and event is triggered by %i EtMiss trigger, return %i", metcut, this->nMetTrigger_xe90(trigInfo), pass) << std::endl;
            }
            return pass;
        }
        if (period == 2 && met > metcut && this->nMetTrigger_xe100(trigInfo))
        {
            if (Debug)
            {
                std::cout << APP_NAME << Form(" DEBUG \t EtMiss > %.0f and event is triggered by %i EtMiss trigger, return %i", metcut, this->nMetTrigger_xe100(trigInfo), pass) << std::endl;
            }
            return pass;
        }
        if (period == 3 && met > metcut && this->nMetTrigger_xe110(trigInfo))
        {
            if (Debug)
            {
                std::cout << APP_NAME << Form(" DEBUG \t EtMiss > %.0f and event is triggered by %i EtMiss trigger, return %i", metcut, this->nMetTrigger_xe110(trigInfo), pass) << std::endl;
            }
            return pass;
        }
        break;

    

    case 2017:
        trigList = triggerList2017;
        MuMuItem = "HLT_mu22_mu8noL1";
	MuMuItema = "HLT_2mu14";
        ElElItem = "HLT_2e24_lhvloose_nod0";
        ElMuItem = "HLT_e17_lhloose_nod0_mu14";
       
/*MuMuItem = "HLT_mu22_mu8noL1"; 
	MuMuItema  = "HLT_2mu4_invm1_j20_xe60_pufit_2dphi10_L12MU4_J40_XE50";
            ElElItem = "HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1XE60 ";
        //    ElMuItem = "HLT_e5_lhloose_nod0_j40_xe70_pufit_2dphi10_L1XE60" ;
//ElElItem  = "HLT_2e24_lhvloose_nod0";
  //          ElMuItem  = "HLT_e17_lhloose_nod0_mu14";
          //  ElMuItem1  = "HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_XE60";
 ElMuItem = "HLT_e17_lhloose_nod0_mu14";
*/
    	    ptCutMuMu = 23000.;
	ptCutMuMua = 15000.;
        ptCutMuMu2 = 10000.;
        ptCutElEl = 25000.;
        ptCutElMu = 20000.;
        if (met > metcut && this->nMetTrigger(trigInfo))
        {
            if (Debug)
            {
                std::cout << APP_NAME << Form(" DEBUG \t EtMiss > %.0f and event is triggered by %i EtMiss trigger, return %i", metcut, this->nMetTrigger(trigInfo), pass) << std::endl;
            }
            return pass;
        }
        break;
    case 2018:
        trigList = triggerList2018;
        ElElItem = "HLT_2e24_lhvloose_nod0";
        ElMuItem = "HLT_e17_lhloose_nod0_mu14";
        MuMuItema = "HLT_2mu14";
	MuMuItem = "HLT_mu22_mu8noL1";
        ptCutMuMu = 23000.;
        ptCutMuMu2 = 10000.;
	ptCutMuMua = 15000.;
        ptCutElEl = 25000.;
        ptCutElMu = 20000.;
        if (met > metcut && this->nMetTrigger(trigInfo))
        {
            if (Debug)
            {
                std::cout << APP_NAME << Form(" DEBUG \t EtMiss > %.0f and event is triggered by %i EtMiss trigger, return %i", metcut, this->nMetTrigger(trigInfo), pass) << std::endl;
            }
            return pass;
        }
        break;
    default:
        break;
    }

    int matchMuMu(0), matchMuMua(0),matchElEl(0), matchElMu(0);

    for (const auto mu1 : *muons)
    {
	 if (Debug && data_period!=0)
            {
                std::cout << "Trigger:"<<Period_List_Vectors(data_period)[0]<<"Pt:"<< pt_2016_array(data_period)[0]<< pt_2016_array(data_period)[1] << std::endl;
		std::cout << "Trigger:"<<Period_List_Vectors(data_period)[1]<<"Pt:"<< pt_2016_array(data_period)[2] << std::endl;
            }

        if (mu1->pt() > ptCutElMu && SusyObjTool->IsTrigMatched(mu1, ElMuItem))
            matchElMu = 1;
        for (const auto mu2 : *muons)
        {
            if (mu1 != mu2 && (mu1->pt() > ptCutMuMu && mu2->pt() > ptCutMuMu2) && SusyObjTool->IsTrigMatched(mu1, mu2, MuMuItem))
                matchMuMu = 2;
        }
	if ( mu1->pt() > ptCutMuMua  && SusyObjTool->IsTrigMatched(mu1,  MuMuItema))
            matchMuMua ++;

    }

    for (const auto el : *electrons)
    {
        if (el->pt() > ptCutElEl && SusyObjTool->IsTrigMatched(el, ElElItem))
            matchElEl++;
        if (el->pt() > ptCutElMu && SusyObjTool->IsTrigMatched(el, ElMuItem) && matchElMu == 1)
            matchElMu = 2;
    }

    if (Debug)
    {
        std::cout << APP_NAME << Form(" DEBUG \t TrigConfig(%i) %s|%s|%s|%s = [%i|%i|%i|%i]", yearOpt, MuMuItem.c_str(),MuMuItema.c_str(), ElElItem.c_str(), ElMuItem.c_str(), matchMuMu, matchMuMua, matchElEl, matchElMu) << std::endl;
    }

    bool MM = (matchMuMu > 1 && containStr(trigInfo, MuMuItem));
    bool MMa = (matchMuMua > 1 && containStr(trigInfo, MuMuItema));
    bool EE = (matchElEl > 1 && containStr(trigInfo, ElElItem));
    bool EM = (matchElMu > 1 && containStr(trigInfo, ElMuItem));

    pass = (MM || EE || EM || MMa);
    return pass;
}

bool DiLepton::isResonance(std::vector<TLorentzVector> vectors, std::vector<float> charges, int channel)
{
    bool res(false);
    bool OS[6] = {0, 0, 0, 0, 0, 0};
    float M[6] = {0., 0., 0., 0., 0., 0.};
    float MZlow(81200.), MZup(101200.);

    if (vectors.size() < 2)
        return res;
    if (vectors.size() == 2 && channel == 3)
        return res;

    if (vectors.size() == 2)
    {
        M[0] = (vectors.at(0) + vectors.at(1)).M();
        if (charges.at(0) != charges.at(1))
            OS[0] = true;
    }
    else if (vectors.size() == 3)
    {
        M[0] = (vectors.at(0) + vectors.at(1)).M();
        M[1] = (vectors.at(1) + vectors.at(2)).M();
        M[2] = (vectors.at(0) + vectors.at(2)).M();
        if (charges.at(0) != charges.at(1))
            OS[0] = true;
        if (charges.at(1) != charges.at(2))
            OS[1] = true;
        if (charges.at(0) != charges.at(2))
            OS[2] = true;
    }
    else
    {
        M[0] = (vectors.at(0) + vectors.at(1)).M();
        M[1] = (vectors.at(1) + vectors.at(2)).M();
        M[2] = (vectors.at(0) + vectors.at(2)).M();
        M[3] = (vectors.at(3) + vectors.at(1)).M();
        M[4] = (vectors.at(3) + vectors.at(2)).M();
        M[5] = (vectors.at(3) + vectors.at(0)).M();
        if (charges.at(0) != charges.at(1))
            OS[0] = true;
        if (charges.at(1) != charges.at(2))
            OS[1] = true;
        if (charges.at(0) != charges.at(2))
            OS[2] = true;
        if (charges.at(3) != charges.at(1))
            OS[3] = true;
        if (charges.at(3) != charges.at(2))
            OS[4] = true;
        if (charges.at(3) != charges.at(0))
            OS[5] = true;
    }

    if ((OS[0] && (M[0] > MZlow && M[0] < MZup)) || (OS[1] && (M[1] > MZlow && M[1] < MZup)) || (OS[2] && (M[2] > MZlow && M[2] < MZup)) || (OS[3] && (M[3] > MZlow && M[3] < MZup)) || (OS[4] && (M[4] > MZlow && M[4] < MZup)) || (OS[5] && (M[5] > MZlow && M[5] < MZup)))
        res = true;

    if (Debug)
    {
        std::cout << "DiLepton::ResVeto()"
                  << " DEBUG \t Found Z resonance:  " << res << std::endl;
    }
    return res;
}

bool DiLepton::getMET(float &met, float &metphi, xAOD::MissingETContainer *MET,
                      const xAOD::JetContainer *jet, const xAOD::ElectronContainer *el, const xAOD::MuonContainer *mu,
                      const xAOD::PhotonContainer *ph, bool doTST, bool doJVT, bool info)
{
    const char *APP_NAME = "DiLepton::getMET()";
    bool Debug = false;

    int Nmu = mu ? mu->size() : 0;
    int Nel = el ? el->size() : 0;
    int Nph = ph ? ph->size() : 0;
    int Nj = jet ? jet->size() : 0;
    if (Debug || info)
    {
        std::cout << APP_NAME << Form(" DEBUG \t BL objects for MET: %i mu, %i el, %i jets, %i ph [doJVT=%i|doTST=%i]", Nmu, Nel, Nj, Nph, doTST, doJVT) << std::endl;
    }
    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t Muons in container" << std::endl;
        for (int imu = 0; imu < Nmu; imu++)
        {
            std::cout << APP_NAME << " DEBUG \t Pt: " << mu->at(imu)->pt() << "\t Eta: " << mu->at(imu)->eta() << "\t Phi: " << mu->at(imu)->phi() << std::endl;
        }
        std::cout << APP_NAME << " DEBUG \t Electrons in container" << std::endl;
        for (int iel = 0; iel < Nel; iel++)
        {
            std::cout << APP_NAME << " DEBUG \t Pt: " << el->at(iel)->pt() << "\t Eta: " << el->at(iel)->eta() << "\t Phi: " << el->at(iel)->phi() << std::endl;
            std::cout << APP_NAME << " DEBUG \t Eta(Cluster): " << el->at(iel)->caloCluster()->eta() << "\t Eta(Cluster->etaBE(2)): " << el->at(iel)->caloCluster()->etaBE(2) << "\t Eta(Track): " << el->at(iel)->trackParticle()->eta() << std::endl;
        }
        std::cout << APP_NAME << " DEBUG \t Photons in container" << std::endl;
        for (int iph = 0; iph < Nph; iph++)
        {
            std::cout << APP_NAME << " DEBUG \t Pt: " << ph->at(iph)->pt() << "\t Eta: " << ph->at(iph)->eta() << "\t Phi: " << ph->at(iph)->phi() << std::endl;
        }
        std::cout << APP_NAME << " DEBUG \t Jets in container" << std::endl;
        for (int ijet = 0; ijet < Nj; ijet++)
        {
            std::cout << APP_NAME << " DEBUG \t Pt: " << jet->at(ijet)->pt() << "\t Eta: " << jet->at(ijet)->eta() << "\t Phi: " << jet->at(ijet)->phi() << std::endl;
        }
    }

    ANA_CHECK(SusyObjTool->GetMET(*MET, jet, el, mu, ph, NULL, doTST, doJVT, 0));

    const auto it = MET->find("Final");
    if (it == MET->end())
    {
        Error(APP_NAME, "No RefFinal inside MET container");
        met = -1;
        metphi = -1;
    }
    else
    {
        met = TMath::Sqrt((*it)->mpx() * (*it)->mpx() + (*it)->mpy() * (*it)->mpy());
        metphi = (*it)->phi();
        if (Debug || info)
        {
            std::cout << APP_NAME << Form(" DEBUG \t mpx: %f mpy: %f metphi: %f met: %f", (float)(*it)->mpx(), (float)(*it)->mpy(), metphi, met) << std::endl;
        }
    }

    return true;
}

float DiLepton::getMt(std::vector<TLorentzVector> leptons, float met, float metPhi)
{
    if (leptons.empty())
        return 0;

    TVector3 lepVec(0., 0., 0.);
    lepVec.SetPtEtaPhi(leptons.at(0).Pt(), 0., leptons.at(0).Phi());
    TLorentzVector Vt(0., 0., 0., 0.);
    Vt.SetPxPyPzE(met * TMath::Cos(metPhi) + lepVec.Px(), met * TMath::Sin(metPhi) + lepVec.Py(), 0., met + lepVec.Pt());

    return (float)Vt.M();
}

float DiLepton::getPileupWeight()
{
    float w(1.);
    if (!doPileup || isData)
        return w;
    // w *= 1;
    w *= SusyObjTool->GetPileupWeight();
    return w;
}

bool DiLepton::cosmicsVeto(const xAOD::MuonContainer *muons, bool enabled)
{
    if (!enabled)
        return false;
    bool cosmics(false);
    for (const auto mu : *muons)
    {
        if (mu->auxdataConst<char>("passOR") == 1 && mu->auxdataConst<char>("cosmic") != 0)
            cosmics = true;
    }

    return cosmics;
}

bool DiLepton::badMuonVeto(const xAOD::MuonContainer *muons, bool enabled)
{
    if (!enabled)
        return false;
    bool badMuon(false);
    for (const auto mu : *muons)
    {
        if (mu->auxdataConst<char>("baseline") == 1 && mu->auxdataConst<char>("bad") != 0)
            badMuon = true;
    }

    return badMuon;
}

bool DiLepton::badJetVeto(const xAOD::JetContainer *jets, bool enabled)
{
    if (!enabled)
        return false;
    bool badjet(false);
    for (const auto jet : *jets)
    {
        if (jet->auxdataConst<char>("passOR") == 1 && jet->auxdataConst<char>("bad") != 0)
            badjet = true;
    }

    return badjet;
}

bool DiLepton::badJvt(const xAOD::Jet &jet, float ptCut, float jvtCut, float jvtCut_HightEta)
{
    bool isBadJvt(false);
    float jvt = jet.isAvailable<float>("Jvt") ? jet.auxdataConst<float>("Jvt") : 0.;
    float jet_DetectorEta = jet.auxdataConst<float>("DetectorEta");
    const char *APP_NAME = "DiLepton::badJvt()";

    if (TMath::Abs(jet_DetectorEta) < 2.4)
    {
        if (jet.pt() < ptCut && jvt < jvtCut)
        {
            if (Debug)
            {
                std::cout << "DiLepton::badJvt()"
                          << " DEBUG is Bad Jvt, eta < 2.4 " << Form("Pt=%.1f, Eta=%.3f, DetectorEta=%.3f, Jvt=%.3f", jet.pt(), jet.eta(), jet_DetectorEta, jvt) << std::endl;
            }
            isBadJvt = true;
        }
    }
    else if (TMath::Abs(jet_DetectorEta) < 2.5)
    {
        if (jet.pt() < ptCut && jvt < jvtCut_HightEta)
        {
            if (Debug)
            {
                std::cout << "DiLepton::badJvt()"
                          << " DEBUG is BadJvt, eta between 2.4 and 2.5" << Form("Pt=%.1f, Eta=%.3f, DetectorEta=%.3f, Jvt=%.3f", jet.pt(), jet.eta(), jet_DetectorEta, jvt) << std::endl;
            }
            isBadJvt = false; /// no JVT cut for pflow jets when eta > 2.4
        }
    }
    else
        isBadJvt = false;

    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t jet info: " << Form("Pt=%.1f, Eta=%.3f, DetectorEta=%3.f, Jvt=%.2f, isBadJvt=%i", jet.pt(), jet.eta(), jet_DetectorEta, jvt, isBadJvt) << std::endl;
    }
    return isBadJvt;
}

bool DiLepton::inCrackRegion(xAOD::Electron *el)
{
    float crack[2] = {1.37, 1.52};
    if (Debug)
    {
        std::cout << "====================In Crack - CaloCluster Eta: " << TMath::Abs(el->caloCluster()->etaBE(2)) << std::endl;
    }
    if (TMath::Abs(el->caloCluster()->etaBE(2)) > crack[0] && TMath::Abs(el->caloCluster()->etaBE(2)) < crack[1])
        return true;

    return false;
}

int DiLepton::getQuality(const xAOD::Muon &mu)
{
    int type(0);
    if (mu.auxdataConst<char>("passOR") == 1)
        type++;
    if (type > 0 && mu.auxdataConst<char>("SIG") == 1)
        type++;

    return type;
}

int DiLepton::getQuality(const xAOD::Electron &el)
{
    int type(0);
    if (el.auxdataConst<char>("passOR") == 1)
        type++;
    if (type > 0 && el.auxdataConst<char>("SIG") == 1)
        type++;

    return type;
}

int DiLepton::getQuality(const xAOD::Jet &jet)
{
    int type(0);
    if (jet.auxdataConst<char>("passOR") == 1)
        type++;
    if (type > 0 && jet.auxdataConst<char>("SIG") == 1)
        type++; /// JVT cut applied here
    return type;
}

void DiLepton::checkOR(const xAOD::IParticleContainer *particles)
{
    for (auto p : *particles)
        if (p->auxdataConst<char>("baseline") == 0 || p->auxdataConst<char>("passOR") == 0)
            p->auxdecor<char>("passOR") = 0;
}

bool DiLepton::isSignalMuon(const xAOD::Muon &mu)
{
    const char *APP_NAME = "DiLepton::isSignalMuon()";
    static SG::AuxElement::Decorator<char> dec_Sig("SIG");
    bool pass(false);
    if (mu.auxdataConst<char>("passOR") == 1 && mu.auxdataConst<char>("signal") == 1)
        pass = true;
    dec_Sig(mu) = pass;
    return pass;
}

bool DiLepton::isSignalElectron(const xAOD::Electron &el, float etamax)
{
    const char *APP_NAME = "DiLepton::isSignalElectron()";
    static SG::AuxElement::Decorator<char> dec_Sig("SIG");
    bool pass(false);
    if(!isPHYSLITE)
      {
	if (el.auxdataConst<char>("passOR") == 1 && el.auxdataConst<char>("signal") == 1 /**&& isSig==1*/ && TMath::Abs(el.trackParticle()->eta()) < etamax && el.auxdataConst<uint8_t>("ambiguityType")==0 && el.auxdataConst<int>("DFCommonAddAmbiguity")<=0 )
        pass = true;
       }
    else{
	if (el.auxdataConst<char>("passOR") == 1 && el.auxdataConst<char>("signal") == 1 /**&& isSig==1*/ && TMath::Abs(el.trackParticle()->eta()) < etamax && el.auxdataConst<uint8_t>("ambiguityType")==0 )
        pass = true;

   }
    dec_Sig(el) = pass;
    /// https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ElectronChargeFlipTaggerTool
    return pass;
}

void DiLepton::getSignalLeptons(const char *type, std::vector<TLorentzVector> &leptons, std::vector<float> &charges, std::vector<float> &qual, unsigned int Nlep)
{
    std::vector<TLorentzVector> l_out;
    std::vector<float> c_out;
    std::vector<float> q_out;

    int qCut(2);
    for (unsigned int i(0); i < leptons.size(); i++)
    {
        if (Nlep == 0)
            qCut = 1;
        if (Nlep == 1 && l_out.size() > 0)
            qCut = 1;
        if (Nlep == 2 && l_out.size() > 1)
            qCut = 1;

        if ((int)qual.at(i) >= qCut) /// after OR
        {
            l_out.push_back(leptons.at(i));
            c_out.push_back(charges.at(i));
            q_out.push_back(qual.at(i));
            if (Debug)
            {
                std::cout << "DiLepton::getSignalLeptons()"
                          << " DEBUG \t Select " << type << " " << i + 1 << " with quality " << (int)qual.at(i) << std::endl;
            }
        }
    }
    leptons.clear();
    charges.clear();
    qual.clear();

    leptons = l_out;
    charges = c_out;
    qual = q_out;
    return;
}

bool DiLepton::isSignalJet(const xAOD::Jet &jet, float ptcut, float EtaMax, float EtaMaxB)
{
    const char *APP_NAME = "DiLepton::isSignalJet()";
    static SG::AuxElement::Decorator<char> dec_Sig("SIG");
    bool isSignal(false);

    bool isB = jet.auxdataConst<char>("bjet");

    float etacut = isB ? EtaMaxB : EtaMax;
    //if (jet.auxdataConst<char>("passOR") == 1 && jet.auxdataConst<char>("signal") == 1 && TMath::Abs(jet.eta()) < etacut && jet.pt() > ptcut && !badJvt(jet))
    if (jet.auxdataConst<char>("passOR") == 1 && jet.auxdataConst<char>("signal") == 1 && TMath::Abs(jet.eta()) < etacut && jet.pt() > ptcut )        
	isSignal = true;
    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t isSignal: " << isSignal << std::endl;
    }

    dec_Sig(jet) = isSignal;
    return isSignal;
}

std::vector<float> DiLepton::getBtags(std::vector<TLorentzVector> jetV, const xAOD::JetContainer *jets)
{
    std::vector<float> bVec(0);
    if (!jets)
        return bVec;
    int isB = 0;

    for (const auto &jIt : jetV)
    {
        for (const auto jet : *jets)
        {
            isB = jet->auxdataConst<char>("bjet");
            if (this->match(jIt, jet->p4()))
            {
                bVec.push_back(isB);
            }
        }
    }

    return bVec;
}

int DiLepton::getNjets(const xAOD::JetContainer *Jets, int seli)
{
    int N(0);
    switch (seli)
    {
    case 1:
        for (const auto jet : *Jets)
        {
            if (jet->auxdataConst<char>("passOR") == 1)
                N++;
        }
        break;
    case 2:
        for (const auto jet : *Jets)
        {
            if (jet->auxdataConst<char>("SIG") == 1)
                N++;
        }
        break;
    }
    return N;
}

bool DiLepton::hasLeptons(std::vector<TLorentzVector> leptons, unsigned int nLepCut)
{
    if (leptons.size() < nLepCut)
        return false;
    if (leptons.at(0).Pt() < lep_PtCut[0] || leptons.at(1).Pt() < lep_PtCut[1])
        return false;
    if (nLepCut >= 3)
    {
        if (leptons.at(2).Pt() < lep_PtCut[2])
            return false;
        else
            return true;
    }
    return true;
}

bool DiLepton::SSleptons(std::vector<TLorentzVector> lepTLV, std::vector<float> lepCharges)
{
    bool passPt(false), passCharges(false);
    unsigned int LepSize = lepTLV.size();

    if (Debug)
    {
        std::cout << "DiLepton::SSleptons() - in loop" << std::endl;
    }
    if (Debug)
    {
        std::cout << "LepSize: " << LepSize << std::endl;
    }

    if (LepSize < 2)
    {
        if (Debug)
        {
            std::cout << "DiLepton::SSlepton() - didn't find 2leptons" << std::endl;
        }
        return false;
    }
    if (LepSize == 2)
    {
        passPt = lepTLV.at(0).Pt() > lep_PtCut[0] && lepTLV.at(1).Pt() > lep_PtCut[1];
        passCharges = lepCharges[0] * lepCharges[1] > 0;
        if (Debug)
        {
            std::cout << "LepCharge[0]: " << lepCharges[0] << ", LepCharge[1]: " << lepCharges[1] << std::endl;
        }
    }
    if (LepSize > 2)
    {
        passPt = lepTLV.at(0).Pt() > lep_PtCut[0] && lepTLV.at(1).Pt() > lep_PtCut[1] && lepTLV.at(2).Pt() > lep_PtCut[2];
        passCharges = true;
    }

    bool selected = (passPt && passCharges);
    return selected;
}

std::vector<TLorentzVector> DiLepton::SSpair(std::vector<TLorentzVector> leptons, std::vector<float> charges)
{ /// !!! ask Julien about the SS pair selection (but it should be OK)
    std::vector<TLorentzVector> SSpair;
    unsigned int N = leptons.size();
    if (N == 2)
    {
        if (charges[0] * charges[1] > 0)
        {
            SSpair.push_back(lepTLV.at(0));
            SSpair.push_back(lepTLV.at(1));
            return SSpair;
        }
    }
    if (N == 3)
    {
        if (charges[0] * charges[1] > 0)
        {
            SSpair.push_back(lepTLV.at(0));
            SSpair.push_back(lepTLV.at(1));
            return SSpair;
        }
        if (charges[0] * charges[2] > 0)
        {
            SSpair.push_back(lepTLV.at(0));
            SSpair.push_back(lepTLV.at(2));
            return SSpair;
        }
        if (charges[1] * charges[2] > 0)
        {
            SSpair.push_back(lepTLV.at(1));
            SSpair.push_back(lepTLV.at(2));
            return SSpair;
        }
    }
    if (N > 3)
    {
        if (charges[0] * charges[1] > 0)
        {
            SSpair.push_back(lepTLV.at(0));
            SSpair.push_back(lepTLV.at(1));
            return SSpair;
        }
        if (charges[0] * charges[2] > 0)
        {
            SSpair.push_back(lepTLV.at(0));
            SSpair.push_back(lepTLV.at(2));
            return SSpair;
        }
        if (charges[0] * charges[3] > 0)
        {
            SSpair.push_back(lepTLV.at(0));
            SSpair.push_back(lepTLV.at(3));
            return SSpair;
        }
        if (charges[1] * charges[2] > 0)
        {
            SSpair.push_back(lepTLV.at(1));
            SSpair.push_back(lepTLV.at(2));
            return SSpair;
        }
        if (charges[1] * charges[3] > 0)
        {
            SSpair.push_back(lepTLV.at(1));
            SSpair.push_back(lepTLV.at(3));
            return SSpair;
        }
        if (charges[2] * charges[3] > 0)
        {
            SSpair.push_back(lepTLV.at(2));
            SSpair.push_back(lepTLV.at(3));
            return SSpair;
        }
    }
    return SSpair;
}

int DiLepton::getChannel(std::vector<TLorentzVector> ss, std::vector<TLorentzVector> muons, std::vector<TLorentzVector> electrons, int lepConfig)
{
    if (lepConfig > 0)
    {
        if (Debug)
        {
            std::cout << "DiLepton::Channel()"
                      << " DEBUG \t setLeptons = " << lepConfig << " returning this" << std::endl;
        }
        return lepConfig;
    }

    int m(0), e(0);
    for (std::vector<TLorentzVector>::iterator mu = muons.begin(); mu != muons.end(); mu++)
    {
        if (match(ss.at(0), *mu))
            m++;
        if (match(ss.at(1), *mu))
            m++;
    }
    for (std::vector<TLorentzVector>::iterator el = electrons.begin(); el != electrons.end(); el++)
    {
        if (match(ss.at(0), *el))
            e++;
        if (match(ss.at(1), *el))
            e++;
    }
    int ch = (m == 1 && e == 1) ? 3 : (m > 1 ? 1 : (e > 1 ? 2 : 0));

    if (Debug)
    {
        std::cout << "DiLepton::Channel()"
                  << " DEBUG \t Lepton channel " << (TString)Form("%im%ie", m, e) << " -> returning " << ch << std::endl;
    }
    return ch;
}

int DiLepton::getLepNumber(std::vector<TLorentzVector> mu, std::vector<TLorentzVector> el, bool noEtaCut, float ele_TrackEtaCut)
{
    const char *APP_NAME = "DiLepton::getLepNumber()";
    int n(0);

    for (std::vector<TLorentzVector>::iterator m = mu.begin(); m != mu.end(); m++)
    {
        if (Debug)
        {
            std::cout << APP_NAME << " DEBUG \t muons: Pt|Eta  " << (*m).Pt() << " | " << (*m).Eta() << std::endl;
        }
        n++;
    }

    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t nmu=: " << n << std::endl;
    }
    for (std::vector<TLorentzVector>::iterator e = el.begin(); e != el.end(); e++)
    {
        if (noEtaCut)
        {
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t electrons, no eta cut applied : Pt|Eta: " << (*e).Pt() << " | " << (*e).Eta() << std::endl;
            }
            n++;
        }
        else if (noEtaCut == false && TMath::Abs((*e).Eta()) < ele_TrackEtaCut)
        {
            if (Debug)
            {
                std::cout << APP_NAME << " DEBUG \t electrons, with eta cut applied : Pt|Eta: " << (*e).Pt() << " | " << (*e).Eta() << std::endl;
            }
            n++;
        }
    }

    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t nmu && nele= " << n << std::endl;
    }
    return n;
}

void DiLepton::sortPt(std::vector<TLorentzVector> &v, std::vector<float> &c, std::vector<float> &q)
{
    typedef std::tuple<TLorentzVector, float, float> lep_triplet;
    std::vector<lep_triplet> temporary_vec;
    for (size_t i = 0; i < v.size(); i++)
    {
        temporary_vec.emplace_back(v.at(i), c.at(i), q.at(i));
    }
    std::sort(std::begin(temporary_vec), std::end(temporary_vec), [](const lep_triplet &left, const lep_triplet &right)
              { return std::get<0>(left).Pt() > std::get<0>(right).Pt(); });
    v.clear();
    c.clear();
    q.clear();

    for (auto &trip : temporary_vec)
    {
        v.push_back(std::get<0>(trip));
        c.push_back(std::get<1>(trip));
        q.push_back(std::get<2>(trip));
    }

    return;
}

float DiLepton::dR(float eta1, float phi1, float eta2, float phi2)
{
    float deta = TMath::Abs(eta1 - eta2);
    float dphi = TMath::Abs(phi1 - phi2) < TMath::Pi() ? TMath::Abs(phi1 - phi2) : 2 * TMath::Pi() - TMath::Abs(phi1 - phi2);

    return TMath::Sqrt(deta * deta + dphi * dphi);
}

float DiLepton::dR(TLorentzVector v1, TLorentzVector v2)
{
    return dR(v1.Eta(), v1.Phi(), v2.Eta(), v2.Phi());
}

float DiLepton::MjjdRmin(std::vector<TLorentzVector> jets)
{
    if (jets.size() == 0)
        return 0;
    if (jets.size() == 1)
        return (jets.at(0)).M();
    if (jets.size() == 2)
        return (jets.at(0) + jets.at(1)).M();

    int it1(0), it2(0);
    float dR(99.);
    for (unsigned int I(0); I < jets.size() - 1; I++)
    {
        for (unsigned int i(I + 1); i < jets.size(); i++)
        {
            float dr = (jets.at(I)).DeltaR(jets.at(i));
            if (dr < dR)
            {
                dR = dr;
                it1 = I;
                it2 = i;
            }
        }
    }

    if (Debug)
    {
        std::cout << "DiLepton::MjjdRmin()"
                  << " DEBUG \t Min dR between jets " << it1 << " and " << it2 << " dR: " << dR << std::endl;
    }
    return (jets.at(it1) + jets.at(it2)).M();
}

float DiLepton::getWeight(const xAOD::Jet &jet, DiLepton::JetWeight type, bool msg)
{
    const char *APP_NAME = "DiLepton::getWeight()";
    bool info = Debug ? msg : false;

    TString name("");
    double weight(0.);
    switch (type)
    {
    case DL1r:
        name = "DL1dv01";
        weight = jet.auxdataConst<double>("btag_weight");
        break;
    case JVT:
        name = "JVT";
        if (jet.isAvailable<float>("Jvt"))
            weight = jet.auxdataConst<float>("Jvt");
        break;
    default:
        Error("DiLepton::getWeight() \t Weight type unknown: Returning ", 0);
    }

    if (info)
    {
        std::cout << APP_NAME << " DEBUG \t " << Form("Jet(pt,eta) (%.2f,%.3f)", jet.pt(), jet.eta()) << Form("\t weight %s = %.3f", name.Data(), weight) << std::endl;
    }
    return weight;
}

std::vector<float> DiLepton::getGlobTriggerSF(int yearOpt, const xAOD::MuonContainer *Muons, const xAOD::ElectronContainer *Electrons,
                                              float met, std::vector<std::string> triggerInfo, bool doSF, bool info, float metcut)
{
    const char *APP_NAME = "DiLepton::getGlobTriggerSF()";

    std::vector<float> sf(3);
    for (unsigned int i(0); i < sf.size(); i++)
        sf[i] = 1.;

    if (!doSF)
        return sf;
    if (Muons->empty() && Electrons->empty())
        return sf;

    std::vector<std::string> trigList;
    switch (yearOpt)
    {
    case 2015:
        trigList = triggerList2015;
        break;
    case 2016:
        trigList = triggerList2016;
        break;
    case 2017:
        trigList = triggerList2017;
        break;
    case 2018:
        trigList = triggerList2018;
        break;
    default:
        break;
    }
    if (trigList.empty())
        return sf;
    if (Debug)
    {
        for (unsigned int t(0); t < trigList.size(); t++)
            Info(APP_NAME, "List of triggers = %s", (trigList.at(t)).c_str());
        std::cout << APP_NAME << " DEBUG \t MET " << met << std::endl;
    }

    if (met > metcut && this->nMetTrigger(triggerInfo))
    {
        if (Debug)
        {
            std::cout << APP_NAME << Form(" DEBUG \t Event is triggered by %i EtMiss trigger, return 1", this->nMetTrigger(triggerInfo)) << std::endl;
        }
        return sf;
    }

    double nom(1.);
    std::vector<const xAOD::Muon *> trigMu(0);
    std::vector<const xAOD::Electron *> trigEl(0);
    for (const auto mu : *Muons)
        trigMu.push_back(mu);
    for (const auto el : *Electrons)
        trigEl.push_back(el);
    if (info && false)
    {
        std::cout << APP_NAME << Form(" DEBUG \t Trigger SF for %im%ie = %.6f", (int)trigMu.size(), (int)trigEl.size(), nom) << std::endl;
    }

    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t About to get SFs " << std::endl;
    }
    /// https://twiki.cern.ch/twiki/bin/view/Atlas/TrigGlobalEfficiencyCorrectionTool
    auto cc1 = (myTriggerSFTool->getEfficiencyScaleFactor(trigEl, trigMu, nom) == CP::CorrectionCode::Ok);
    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t Got SFs " << std::endl;
    }
    if (!cc1)
    {
        Warning(APP_NAME, "SF evaluation failed [%i], returning %.1f", cc1, sf[0]);
        return sf;
    }
    if (info)
    {
        std::cout << APP_NAME << Form(" DEBUG \t Trigger SF for %im%ie = %.6f", (int)trigMu.size(), (int)trigEl.size(), nom) << std::endl;
    }

    sf[0] = (float)nom;
    return sf;
}

std::vector<float> DiLepton::getSF(const xAOD::MuonContainer *muons, MCTemplateCorr fakeCorr, int mcChNumber, float &wFakeMu_MCTemplate, float &uncFakeMu_MCTemplate, bool doSF, bool info)
{
    if (!isMC)
        return {1.f};

    if (Debug)
    {
        std::cout << "==========================" << std::endl;
        std::cout << "Getting SF for muons using ST, no trigger SFs "
                  << ", EventNumber = " << EventNumber << std::endl;
    }

    wFakeMu_MCTemplate = 1.;
    uncFakeMu_MCTemplate = 0.;
    bool RecoSF(1), IsoSF(1);
    std::vector<float> SF;
    if (!doSF)
        return SF;
    for (const auto mu : *muons)
    {
        /// take the fake lepton weight after applying the fake lepton SF obtained with the MCTemplate method
        if (isFakeMC)
        {
            int type = mu->auxdataConst<int>("truthType"), origin = mu->auxdataConst<int>("truthOrigin");
            float Fallback_dR = mu->isAvailable<float>("TruthClassifierFallback_dR") ? mu->auxdataConst<float>("TruthClassifierFallback_dR") : -1.f;
            int fallback_type = Fallback_dR < 0.05 ? mu->auxdataConst<int>("TruthClassifierFallback_truthType") : -1.f;
            int fallback_origin = Fallback_dR < 0.05 ? mu->auxdataConst<int>("TruthClassifierFallback_truthOrigin") : -1.f;
            /// float fallBack_Dr = mu->auxdataConst<float>("TruthClassifierFallback_dR"); /// not used, but kept for debugging
            fakeCorr.AddMuon(mcChNumber, mu->pt(), mu->charge(), type, fallback_type, origin, fallback_origin);
            wFakeMu_MCTemplate = fakeCorr.GetFakeCorrection(uncFakeMu_MCTemplate);
        }

        /// take the reco, ID, etc SFs
        float sfTot = SusyObjTool->GetSignalMuonSF(*mu, RecoSF, IsoSF);
        SF.push_back(sfTot);

        if (Debug || info)
        {
            float sfRec = SusyObjTool->GetSignalMuonSF(*mu, RecoSF, 0);
            float sfIso = SusyObjTool->GetSignalMuonSF(*mu, 0, IsoSF);
            Info("DiLepton::getSF()", "Muon(pt=%.1f) SF %f \t RecoSF %f \t IsoSF %f", mu->pt(), sfTot, sfRec, sfIso);
        }
    }
    return SF;
}

std::vector<float> DiLepton::getSF(const xAOD::ElectronContainer *electrons, MCTemplateCorr fakeCorr, int mcChNumber, float &wFakeEl_MCTemplate, float &uncFakeEl_MCTemplate /**, float el_ECIDS*/, bool doSF, bool info)
{
    if (!isMC)
        return {1.f};

    if (Debug || info)
    {
        std::cout << "==========================" << std::endl;
        std::cout << "Getting SF for electrons using ST, no ECIDS SFs in, no trigger SFs "
                  << ", EventNumber = " << EventNumber << std::endl;
    }

    wFakeEl_MCTemplate = 1.;
    uncFakeEl_MCTemplate = 0.;
    bool RecoSF(1), IsoSF(1), IdSF(1), ECIDSSF(1);
    ;
    std::vector<float> SF;
    if (!doSF)
        return SF;
    for (const auto el : *electrons)
    {
        /// take the fake lepton weight after applying the fake lepton SF obtained with the MCTemplate method
        if (isFakeMC)
        {
            int type = el->auxdataConst<int>("truthType"), origin = el->auxdataConst<int>("truthOrigin"), pdgid = el->auxdataConst<int>("truthPdgId");
            int firstmum_type = el->auxdataConst<int>("firstEgMotherTruthType"), firstmum_origin = el->auxdataConst<int>("firstEgMotherTruthOrigin"), firstmum_pdgid = el->auxdataConst<int>("firstEgMotherPdgId");
            /// std::cout << "!!!firstmum_pdgid = " << firstmum_pdgid << std::endl;
            float Fallback_dR = el->isAvailable<float>("TruthClassifierFallback_dR") ? el->auxdataConst<float>("TruthClassifierFallback_dR") : -1.f;
            int fallback_type = Fallback_dR < 0.05 ? el->auxdataConst<int>("TruthClassifierFallback_truthType") : -1.f;
            int fallback_origin = Fallback_dR < 0.05 ? el->auxdataConst<int>("TruthClassifierFallback_truthOrigin") : -1.f;
            fakeCorr.AddElectron(mcChNumber, el->pt(), el->charge(), type, firstmum_type, fallback_type, origin, firstmum_origin, fallback_origin, pdgid, firstmum_pdgid);
            wFakeEl_MCTemplate = fakeCorr.GetFakeCorrection(uncFakeEl_MCTemplate);
        }

        /// take the reco, ID, etc SFs
        /// GetSignalElecSF(const xAOD::Electron& el,const bool recoSF, const bool idSF, const bool triggerSF, const bool isoSF, const std::string& trigExpr, const bool ecidsSF, const bool cidSF )
        float sfTot = SusyObjTool->GetSignalElecSF(*el, RecoSF, IdSF, 0, IsoSF, "", ECIDSSF, false); /// no ECIDS SFs here
        SF.push_back(sfTot);

        if (Debug || info)
        {
            float sfRec = SusyObjTool->GetSignalElecSF(*el, RecoSF, 0, 0, 0, "", false, false);
            float sfPID = SusyObjTool->GetSignalElecSF(*el, 0, IdSF, 0, 0, "", false, false);
            float sfIso = SusyObjTool->GetSignalElecSF(*el, 0, 0, 0, IsoSF, "", false, false);
            float sfECIDS = SusyObjTool->GetSignalElecSF(*el, 0, 0, 0, 0, "", ECIDSSF, false);
            Info("DiLepton::getSF()", "Electron(pt=%.1f) SF %f \t Reco*PidSF %f (RecoSF %f , PidSF %f) \t IsoSF %f ECIDS %f / %f", el->pt(), sfTot, sfRec * sfPID, sfRec, sfPID, sfIso /**, el_ECIDS*/, sfECIDS);
        }
    }

    return SF;
}

float DiLepton::getTotalSF(const xAOD::MuonContainer *mu, const xAOD::ElectronContainer *el, const xAOD::JetContainer *je, /**float muon_iso, float ele_iso, float cftSF,*/ bool _debug)
{
    const char *APP_NAME = "DiLepton::getTotalSF()";
    float sf(1.);
    bool RecoSF(1), IdSF(1), IsoSF(1), ECIDSSF(1);
    if (mu)
    {
        sf *= SusyObjTool->GetTotalMuonSF(*mu, RecoSF, 0, "");
        sf *= SusyObjTool->GetTotalMuonSF(*mu, 0, IsoSF, "");
        /// float muon_iso_ST = (float) SusyObjTool->GetTotalMuonSF(*mu, 0, IsoSF, "");
        if (Debug || _debug)
        {
            std::cout << APP_NAME << " DEBUG \t muSF Reco, from ST: " << SusyObjTool->GetTotalMuonSF(*mu, RecoSF, 0, "") << std::endl;
            std::cout << APP_NAME << " DEBUG \t muSF ISO, from ST " << SusyObjTool->GetTotalMuonSF(*mu, 0, IsoSF, "") /** << ", from me = " << muon_iso*/ << std::endl;
            std::cout << APP_NAME << " DEBUG \t muSF Total, from ST: " << SusyObjTool->GetTotalMuonSF(*mu, RecoSF, IsoSF, "") << std::endl;
        }
    }
    if (el)
    {
        /// GetTotalElectronSF(const xAOD::ElectronContainer& electrons, const bool recoSF, const bool idSF, const bool triggerSF, const bool isoSF, const std::string& trigExpr, const bool ecidsSF, const bool cidSF)
        ///  sf *= cftSF;
        sf *= SusyObjTool->GetTotalElectronSF(*el, RecoSF, false, false, false, "", false, false);
        sf *= SusyObjTool->GetTotalElectronSF(*el, false, IdSF, false, false, "", false, false);
        sf *= SusyObjTool->GetTotalElectronSF(*el, false, false, false, IsoSF, "", false, false);
        sf *= SusyObjTool->GetTotalElectronSF(*el, false, false, false, false, "", ECIDSSF, false);

        if (Debug || _debug)
        {
            std::cout << APP_NAME << " DEBUG \t elSF Total, ST: " << SusyObjTool->GetTotalElectronSF(*el, RecoSF, IdSF, 0, IsoSF, "", ECIDSSF, false) << std::endl;
            std::cout << APP_NAME << " DEBUG \t elSF Reco, from ST: " << SusyObjTool->GetTotalElectronSF(*el, RecoSF, false, false, false, "", false, false) << std::endl;
            std::cout << APP_NAME << " DEBUG \t elSF ID, from ST: " << SusyObjTool->GetTotalElectronSF(*el, false, IdSF, false, false, "", false, false) << std::endl;
            std::cout << APP_NAME << " DEBUG \t elSF ISO, from ST: " << SusyObjTool->GetTotalElectronSF(*el, false, false, false, IsoSF, "", false, false) /** << " (my SFs = " << ele_iso<< ")"*/ << std::endl;
            std::cout << APP_NAME << " DEBUG \t elSF CFT from ST: " << SusyObjTool->GetTotalElectronSF(*el, false, false, false, false, "", ECIDSSF, false) /**<< " (my SFs = " << cftSF*/ << std::endl;
        }
    }
    if (Debug || _debug)
    {
        std::cout << APP_NAME << " DEBUG \t Total LepSF: " << sf << ", EventNumber = " << EventNumber << std::endl;
    }
    if (je)
    {
        sf *= SusyObjTool->BtagSF(je);
        sf *= SusyObjTool->JVT_SF(je);

        if (Debug || _debug)
        {
            std::cout << APP_NAME << " DEBUG \t Total jvtSF: " << SusyObjTool->JVT_SF(je) << ", EventNumber = " << EventNumber << std::endl;
            std::cout << APP_NAME << " DEBUG \t Total btagSF: " << SusyObjTool->BtagSF(je) << ", EventNumber = " << EventNumber << std::endl;
        }
    }
    if (Debug || _debug)
    {
        std::cout << APP_NAME << " DEBUG \t Total ObjectSF: " << sf << ", EventNumber = " << EventNumber << std::endl;
    }
    return sf;
}

bool DiLepton::resetSystematics(bool info)
{
    const char *APP_NAME = "Systematics()";
    //  if( SusyObjTool->resetSystematics() != CP::SystematicCode::Ok ){
    if (SusyObjTool->resetSystematics() != StatusCode::SUCCESS)
    {
        Error(APP_NAME, "Cannot reset SUSYTools systematics");
        return false;
    }
    else
    {
        if (info)
            Info(APP_NAME, "Systematics reset successfully");
    }

    return true;
}

ST::SystInfo DiLepton::emptySys()
{
    ST::SystInfo sys;
    sys.affectsKinematics = false;
    sys.affectsWeights = false;
    sys.affectsType = ST::Unknown;
    return sys;
}

bool DiLepton::applySystematic(CP::SystematicSet sys, bool info)
{
    const char *APP_NAME = "Systematics()";
    // if( SusyObjTool->applySystematicVariation(sys) != CP::SystematicCode::Ok ){
    if (SusyObjTool->applySystematicVariation(sys) != StatusCode::SUCCESS)
    {
        Error(APP_NAME, "Cannot configure SUSYTools for systematic var. %s", sys.name().c_str());
        return false;
    }
    else
    {
        if (info)
            Info(APP_NAME, "Variation \"%s\" configured...", sys.name().c_str());
    }

    return true;
}

void DiLepton::PrintSys(std::vector<ST::SystInfo> syslist)
{
    const char *APP_NAME = "Systematics()";
    for (const auto &sysInfo : syslist)
    {
        const CP::SystematicSet &sys = sysInfo.systset;
        Info(APP_NAME, "Variation \"%s\" configured...", sys.name().c_str());
    }

    return;
}

void DiLepton::checkNames(std::vector<ST::SystInfo> &syslist, std::vector<std::string> names)
{
    const char *APP_NAME = "Systematics()";
    if (names.empty())
        return;
    if (names.size() == 1 && !names.front().length())
        return;

    Info(APP_NAME, "Checking systematics by names");
    std::vector<ST::SystInfo> output;
    output.push_back(emptySys());

    for (const auto &sysInfo : syslist)
    {
        const CP::SystematicSet &sys = sysInfo.systset;
        for (unsigned int i(0); i < names.size(); i++)
        {
            if (names.at(i) == sys.name())
            {
                output.push_back(sysInfo);
                continue;
            }
        }
    }
    if (output.empty())
    {
        Error(APP_NAME, "No systematics found... returning full list");
        return;
    }

    syslist.clear();
    syslist = output;

    return;
}

void DiLepton::checkNames_ToRemoveSyst(std::vector<ST::SystInfo> &syslist, std::vector<std::string> names_to_remove)
{
    const char *APP_NAME = "Systematics()";
    if (names_to_remove.empty())
        return;

    Info(APP_NAME, "Checking systematics by names, to remove the unwanted ones");
    std::vector<ST::SystInfo> output;
    output.push_back(emptySys());
    auto end = names_to_remove.cend();
    for (const auto &sysInfo : syslist)
    {
        const CP::SystematicSet &sys = sysInfo.systset;
        if (std::find(names_to_remove.cbegin(), end, sys.name()) == end)
        {
            output.push_back(sysInfo);
        }
        else
            std::cout << sys.name() << " syst removed!" << std::endl;
    }
    if (output.empty())
    {
        Error(APP_NAME, "No systematics found... returning full list");
        return;
    }

    syslist.clear();
    syslist = output;

    return;
}

void DiLepton::checkNames(std::vector<SR> &srs, std::vector<std::string> names)
{
    const char *APP_NAME = "SignalRegion()";
    if (names.empty())
        return;
    if (names.size() == 1 && !names.front().length())
        return;

    Info(APP_NAME, "Checking Signal Regions by names");
    std::vector<SR> output;
    for (const auto &sr : srs)
    {
        for (unsigned int i(0); i < names.size(); i++)
        {
            TString srname = names.at(i);
            if (srname == sr.Name)
            {
                output.push_back(sr);
                continue;
            }
        }
    }
    if (output.empty())
    {
        Error(APP_NAME, "No systematics found... returning full list");
        return;
    }

    srs.clear();
    srs = output;
    return;
}

CP::SystematicSet DiLepton::getSys(std::string name, std::vector<ST::SystInfo> syslist)
{
    CP::SystematicSet sys;
    for (const auto &sysInfo : syslist)
    {
        sys = sysInfo.systset;
        if (sys.name() == name)
            return sys;
    }
    Error("getSys()", "Systematic %s not found, returning", name.c_str());

    return sys;
}

std::vector<ST::SystInfo> DiLepton::getSysInfoList(bool fill, std::vector<ST::SystInfo> syslist, DiLepton::SysType type, bool useTau, bool usePhoton)
{
    const char *APP_NAME = "Systematics()";
    std::vector<ST::SystInfo> output;
    output.push_back(emptySys());
    if (!fill)
        return output;

    for (const auto &sysInfo : syslist)
    {
        if (!useTau && ST::testAffectsObject(xAOD::Type::Tau, sysInfo.affectsType))
            continue;
        if (!usePhoton && (ST::testAffectsObject(xAOD::Type::Photon, sysInfo.affectsType) && !ST::testAffectsObject(xAOD::Type::Electron, sysInfo.affectsType)))
            continue;

        switch (type)
        {
        case Weight:
            if (sysInfo.affectsWeights)
                output.push_back(sysInfo);
            break;
        case Kinematic:
            if (sysInfo.affectsKinematics)
                output.push_back(sysInfo);
            break;
        default:
            Error(APP_NAME, "Unknown type of systematics error");
        }
    }
    return output;
}

std::vector<std::string> DiLepton::getSysNames(std::vector<ST::SystInfo> syslist)
{
    std::vector<std::string> SysNames;
    for (const auto &sysInfo : syslist)
    {
        const CP::SystematicSet &sys = sysInfo.systset;
        SysNames.push_back(sys.name());
    }

    return SysNames;
}

std::vector<float> DiLepton::getSysWeights(const xAOD::MuonContainer *Muons, const xAOD::ElectronContainer *Electrons, const xAOD::JetContainer *Jets,
                                           std::vector<ST::SystInfo> syslist, std::vector<std::string> &names, unsigned int nsys, bool _debug, unsigned int dataperiod)
{
    const char *APP_NAME = "DiLepton::getSysWeights()";
    std::vector<float> output;
    names.clear();
    bool RecoSF(1), IsoSF(1), IdSF(1), ECIDSSF(1);
    std::string TrigExpr = "";
    float muSF(1.), elSF(1.), jetSF(1.);

    this->resetSystematics(Debug);
    if (Muons)
    {
        muSF *= SusyObjTool->GetTotalMuonSF(*Muons, RecoSF, 0, "");
        muSF *= SusyObjTool->GetTotalMuonSF(*Muons, 0, IsoSF, "");
    }
    if (Electrons)
    {
        elSF *= SusyObjTool->GetTotalElectronSF(*Electrons, RecoSF, false, false, false, "", false, false);
        elSF *= SusyObjTool->GetTotalElectronSF(*Electrons, false, IdSF, false, false, "", false, false);
        elSF *= SusyObjTool->GetTotalElectronSF(*Electrons, false, false, false, IsoSF, "", false, false);
        elSF *= SusyObjTool->GetTotalElectronSF(*Electrons, false, false, false, false, "", ECIDSSF, false);
    }
    if (Jets)
    {
        jetSF *= SusyObjTool->BtagSF(Jets);
        jetSF *= SusyObjTool->JVT_SF(Jets);
    }
    float prwSF = this->getPileupWeight();

    output.push_back(muSF);
    output.push_back(elSF);
    output.push_back(jetSF);
    output.push_back(prwSF);

    names.push_back("MU_SF_NOM");
    names.push_back("EL_SF_NOM");
    names.push_back("JET_SF_NOM");
    names.push_back("PILEUP_NOM");
    if (Debug || _debug)
    {
        std::cout << APP_NAME << " DEBUG \t Nominal values " << Form("(muSF,elSF,jetSF,prwSF) = (%.4f, %.4f, %.4f, %.4f)", muSF, elSF, jetSF, prwSF) << ", EventNumber = " << EventNumber << std::endl;
    }

    std::vector<const xAOD::Muon *> trigMu(0);
    std::vector<const xAOD::Electron *> trigEl(0);
    for (const auto mu : *Muons)
        trigMu.push_back(mu);
    for (const auto el : *Electrons)
        trigEl.push_back(el);
    double trigSys(1.0);

    for (const auto &sysInfo : syslist)
    {
        const CP::SystematicSet &sys = sysInfo.systset;
        float sf(1.);
        std::cout << "match test before: "<< sys.name() <<"tool: " << SusyObjTool->GetTriggerGlobalEfficiencySFsys(*Electrons,*Muons, sys,"diLepton") <<"factorscala "<< sf << std::endl;

	this->applySystematic(sys, 0);
        bool sysMu = ST::testAffectsObject(xAOD::Type::Muon, sysInfo.affectsType);
        bool sysEl = ST::testAffectsObject(xAOD::Type::Electron, sysInfo.affectsType);
        bool sysJet = (ST::testAffectsObject(xAOD::Type::Jet, sysInfo.affectsType) || ST::testAffectsObject(xAOD::Type::BTag, sysInfo.affectsType));
        bool sysPRW = (ST::testAffectsObject(sysInfo.affectsType) == "EventWeight");

        if (!sysMu && !sysEl && !sysJet && !sysPRW)
            continue;

        if (sysMu)
        {
            if (sys.name().find("Trig") != std::string::npos)
            {
                /// This weighted systematics are affecting triggers
                /// Here re-run the calculation of the trigger SFs
                auto cc1 = (myTriggerSFTool->getEfficiencyScaleFactor(trigEl, trigMu, trigSys) == CP::CorrectionCode::Ok);
                

		//trigSys=SusyObjTool->GetTriggerGlobalEfficiencySF(*Electrons,*Muons,"diLepton");
                
		sf *= trigSys;
		
	  // std::cout << "match test sys: "<< sys.name() <<"tool: " << SusyObjTool->GetTriggerGlobalEfficiencySFsys(*Electrons,*Muons, sys,"diLepton") <<" "<< trigSys << std::endl;
	     std::cout << "match test sys: "<< sys.name() <<"tool: " << SusyObjTool->GetTriggerGlobalEfficiencySF(*Electrons,*Muons,"diLepton") <<" factorscala:"<< sf << std::endl;

            
	     }
            else
            {
                sf *= SusyObjTool->GetTotalMuonSF(*Muons, RecoSF, 0, "");
                sf *= SusyObjTool->GetTotalMuonSF(*Muons, 0, IsoSF, "");
	//	sf *= SusyObjTool->GetTotalMuonSFsys(*Muons, sys, RecoSF, 0, "");
        //        sf *= SusyObjTool->GetTotalMuonSFsys(*Muons, sys, 0, IsoSF, "");

            }
            if (false && _debug)
            {
                std::cout << APP_NAME << " Mu reco SF = " << SusyObjTool->GetTotalMuonSFsys(*Muons, sys, RecoSF, 0, "") << std::endl;
                std::cout << APP_NAME << " Mu iso SF = " << SusyObjTool->GetTotalMuonSFsys(*Muons, sys, 0, IsoSF, "") << std::endl;
            }
        }
        if (sysEl)
        {
            if (sys.name().find("Trig") != std::string::npos)
            {
                /// This weighted systematics are affecting triggers
                /// Here re-run the calculation of the trigger SFs
                auto cc1 = (myTriggerSFTool->getEfficiencyScaleFactor(trigEl, trigMu, trigSys) == CP::CorrectionCode::Ok);
                sf *= trigSys;
            
		//std::cout << "match test sys: "<< sys.name() <<"tool: "  << SusyObjTool->GetTriggerGlobalEfficiencySFsys(*Electrons,*Muons, sys,"diLepton") <<  " " << trigSys << std::endl;
		std::cout << "match test sys: "<< sys.name() <<"tool: " << SusyObjTool->GetTriggerGlobalEfficiencySF(*Electrons,*Muons,"diLepton") <<"factorscala:"<< sf << std::endl;

	    }
            else
            {
                sf *= SusyObjTool->GetTotalElectronSF(*Electrons, RecoSF, false, false, false, "", false, false);
                sf *= SusyObjTool->GetTotalElectronSF(*Electrons, false, IdSF, false, false, "", false, false);
                sf *= SusyObjTool->GetTotalElectronSF(*Electrons, false, false, false, IsoSF, "", false, false);
                sf *= SusyObjTool->GetTotalElectronSF(*Electrons, false, false, false, false, "", ECIDSSF, false);
		//sf *= SusyObjTool->GetTotalElectronSFsys(*Electrons, sys, RecoSF, false, false, false, "", false, false);
                //sf *= SusyObjTool->GetTotalElectronSFsys(*Electrons, sys, false, IdSF, false, false, "", false, false);
                //sf *= SusyObjTool->GetTotalElectronSFsys(*Electrons, sys, false, false, false, IsoSF, "", false, false);
                //sf *= SusyObjTool->GetTotalElectronSFsys(*Electrons, sys, false, false, false, false, "", ECIDSSF, false);           
 }
            if (false && _debug)
            {
                std::cout << APP_NAME << " El reco SF = " << SusyObjTool->GetTotalElectronSFsys(*Electrons, sys, RecoSF, false, false, false, "", false, false) << std::endl;
                std::cout << APP_NAME << " El id SF = " << SusyObjTool->GetTotalElectronSFsys(*Electrons, sys, false, IdSF, false, false, "", false, false) << std::endl;
                std::cout << APP_NAME << " El iso SF = " << SusyObjTool->GetTotalElectronSFsys(*Electrons, sys, false, false, false, IsoSF, "", false, false) << std::endl;
                std::cout << APP_NAME << " El ecids SF = " << SusyObjTool->GetTotalElectronSFsys(*Electrons, sys, false, false, false, false, "", ECIDSSF, false) << std::endl;
            }
        }
        if (sysJet)
        {
            sf *= SusyObjTool->BtagSF(Jets);
            sf *= SusyObjTool->JVT_SF(Jets);
            if (false && _debug)
            {
                std::cout << APP_NAME << " bjet SF = " << SusyObjTool->BtagSF(Jets) << std::endl;
                std::cout << APP_NAME << " jvt SF = " << SusyObjTool->JVT_SF(Jets) << std::endl;
            }
        }
        if (sysPRW)
            sf = this->getPileupWeight();

        if (Debug || _debug)
        {
            std::cout << APP_NAME << " DEBUG \t " << Form("SF %.4f \t [MU|EL|JET|PRW] = [%i|%i|%i|%i]", sf, sysMu, sysEl, sysJet, sysPRW) << "\t Syst: " << sys.name() << ", EventNumber = " << EventNumber << std::endl;
        }

        names.push_back(sys.name());
        output.push_back(sf);
    }
    if (nsys)
        this->applySystematic((CP::SystematicSet)SysInfoList_K[nsys].systset, Debug);
    else
        this->resetSystematics(Debug);


	 std::cout << "match test final: "<< trigSys << std::endl;


    return output;
}

float DiLepton::getTruthVar(xAOD::TEvent *event, int type)
{
    const char *APP_NAME = "DiLepton::getTruthVar()";
    float out(0);
    TString var("");
    const xAOD::EventInfo *evt(0);
    if (!event->retrieve(evt, "EventInfo").isSuccess())
    {
        Error(APP_NAME, "Error retrieving EventInfo");
        return out;
    }
    try
    {
        switch (type)
        {
        case 0:
            out = evt->isAvailable<float>("GenFiltHT") ? evt->auxdataConst<float>("GenFiltHT") : 0.;
            var = "HT";
            break;
        case 1:
            out = evt->isAvailable<float>("GenFiltMET") ? evt->auxdataConst<float>("GenFiltMET") : 0.;
            var = "MET";
            break;
        default:
            break;
        }
    }
    catch (SG::ExcBadAuxVar &)
    {
        Info(APP_NAME, "WARNING: Truth metadata is not available");
    }

    if (Debug && false)
    {
        std::cout << APP_NAME << " DEBUG \t TruthVar " << var << "::" << out << std::endl;
    }
    return out;
}

bool DiLepton::truthVeto(int MCId, float trHt, float trMET)
{
    if (!MCId)
        return false;

    bool combHt(true), combMET(false);
    if (combMET)
    {
        if (MCId == 407345 && (trMET < 200000. || trMET > 300000.))
            return true;
        if (MCId == 407346 && (trMET < 300000. || trMET > 400000.))
            return true;
        if (MCId == 407347 && trMET < 400000.)
            return true;
        if (MCId == 410470 && trMET > 200000.)
            return true;
    }
    if (combHt)
    {
        if (MCId == 407344 && (trHt < 600000. || trHt > 1000000.))
            return true;
        if (MCId == 407343 && (trHt < 1000000. || trHt > 1500000.))
            return true;
        if (MCId == 407342 && trHt < 1500000.)
            return true;
        if (MCId == 410470 && trHt > 600000.)
            return true;
    }
    return false;
}

int DiLepton::truthID(const xAOD::Jet *jet, bool isMC)
{
    int id(0);
    if (!isMC || !jet)
        return id;
    if (jet->auxdataConst<char>("isW"))
        id = 1;
    if (jet->auxdataConst<char>("isZ"))
        id = id + 1E1;
    if (jet->auxdataConst<char>("isLQ"))
        id = id + 1E2;
    if (jet->auxdataConst<char>("isTop"))
        id = id + 1E3;
    if (jet->auxdataConst<char>("isLepPh"))
        id = id + 1E4;
    if (jet->auxdataConst<char>("isMeson"))
        id = id + 1E5;

    return id;
}

/// not all crazy ele from FSR ISR included here -- much safer for the 3L SS SRs
std::vector<int> DiLepton::getTruthInfo(std::vector<TLorentzVector> lep, const xAOD::MuonContainer *muons, const xAOD::ElectronContainer *electrons)
{

    const char *APP_NAME = "DiLepton::getTruthInfo()";
    std::vector<int> truthInfo(0);
    for (const auto &it : lep)
    {
        for (const auto mu : *muons)
        {
            int type = mu->auxdataConst<int>("truthType"), origin = mu->auxdataConst<int>("truthOrigin");
            int truth = (type == 6);
            if (this->match(it, mu->p4()))
            {
                if (Debug)
                {
                    std::cout << APP_NAME << " DEBUG \t "
                              << Form("Mu(pt,eta) (%.2f,%.3f): truthType|Origin = (%i|%i)", mu->pt(), mu->eta(), type, origin) << std::endl;
                }
                truthInfo.push_back(truth);
                break;
            }
        }
        for (const auto el : *electrons)
        {
            int type = el->auxdataConst<int>("truthType"), origin = el->auxdataConst<int>("truthOrigin");
            int truth = (type == 2 || (origin == 5 && fabs(el->auxdataConst<int>("firstEgMotherPdgId")) == 11 && (el->charge() * el->auxdataConst<int>("firstEgMotherPdgId")) < 0));
            if (this->match(it, el->p4()))
            {
                if (Debug)
                {
                    std::cout << APP_NAME << " DEBUG \t "
                              << Form("El(pt,eta) (%.2f,%.3f): truthType|Origin = (%i|%i)", el->pt(), el->eta(), type, origin) << std::endl;
                }
                truthInfo.push_back(truth);
                break;
            }
        }
    }
    return truthInfo;
}

bool DiLepton::getJetOrigin(const xAOD::JetContainer *Jets, xAOD::TEvent *event, bool isMC, bool msg)
{
    bool info = Debug ? msg : false;
    const char *APP_NAME = "DiLepton::getJetOrigin()";
    if (!isMC || !Jets)
        return true;

    for (const auto jet : *Jets)
    {
        if (!this->getMotherParticle(jet, event, info))
        {
            Error(APP_NAME, "Failed to get mother particle of jet");
            return false;
        }

        if (info)
        {
            std::cout << APP_NAME << " DEBUG \t " << Form("Jet (Pt=%.1f|Eta=%.3f|Phi=%.3f|M=%.1f) \t", jet->pt(), jet->eta(), jet->phi(), (jet->p4()).M())
                      << Form("( W=%i | Z=%i | LQ=%i | Top=%i | LepPh=%i | Meson=%i ) ID=%i",
                              (int)jet->auxdataConst<char>("isW"), (int)jet->auxdataConst<char>("isZ"), (int)jet->auxdataConst<char>("isLQ"),
                              (int)jet->auxdataConst<char>("isTop"), (int)jet->auxdataConst<char>("isLepPh"),
                              (int)jet->auxdataConst<char>("isMeson"), (int)truthID(jet, isMC))
                      << std::endl;
        }
    }
    return true;
}

bool DiLepton::getMotherParticle(const xAOD::Jet *jet, xAOD::TEvent *event, bool msg)
{
    bool info = Debug ? msg : false;
    const char *APP_NAME = "DiLepton::getMotherParticle()";

    static SG::AuxElement::Decorator<char> dec_W("isW");
    static SG::AuxElement::Decorator<char> dec_Z("isZ");
    static SG::AuxElement::Decorator<char> dec_LQ("isLQ");
    static SG::AuxElement::Decorator<char> dec_Top("isTop");
    static SG::AuxElement::Decorator<char> dec_LepPh("isLepPh");
    static SG::AuxElement::Decorator<char> dec_Meson("isMeson");

    bool isW(0), isZ(0), isLQ(0), isTop(0), isLepPh(0), isMeson(0);
    float ptmin(10000.), dRmax(0.5);

    const xAOD::TruthParticleContainer *truthP(0);
    if (!evtStore()->contains<xAOD::TruthParticleContainer>("TruthParticles"))
    {
        Error(APP_NAME, "Failed to retrieve truth-info Marina");
        return false;
    }
    if (!evtStore()->retrieve(truthP, "TruthParticles").isSuccess())
    {
        Error(APP_NAME, "Failed to retrieve truth-info");
        return false;
    }
    if (!event->retrieve(truthP, "TruthParticles").isSuccess())
    {
        Error(APP_NAME, "Failed to retrieve truth-info");
        return false;
    }
    for (const auto tp : *truthP)
    {
        if (tp->pt() < ptmin)
            continue;
        float dr = this->dR(jet->p4(), tp->p4());
        if (dr > dRmax)
            continue;

        if (tp->isW())
            isW = true;
        if (tp->isZ())
            isZ = true;
        if (tp->isQuark())
            isLQ = true;
        if (tp->isTop())
            isTop = true;
        if (tp->isMeson())
            isMeson = true;
        if (tp->isLepton() || tp->isPhoton())
            isLepPh = true;
        if (info)
        {
            std::cout << APP_NAME << " DEBUG \t "
                      << Form("Jet (Pt=%.1f|Eta=%.3f|Phi=%.3f|M=%.1f) \t", jet->pt(), jet->eta(), jet->phi(), (jet->p4()).M())
                      << Form("Found truthParticle (Pt=%.1f|Eta=%.3f|Phi=%.3f) dR=%.3f \t (PDG=%i|W=%i|Z=%i|LQ=%i|Top=%i|LepPh=%i|Meson=%i)",
                              tp->pt(), tp->eta(), tp->phi(), dr, tp->pdgId(), isW, isZ, isLQ, isTop, isLepPh, isMeson)
                      << std::endl;
        }

        this->quarkFromWdR(tp, info);

        if (!this->getParents(tp, isW, isZ, isLQ, isTop, isLepPh, isMeson, info))
        {
            std::cout << APP_NAME << " DEBUG \t "
                      << "Cannot retrieve parent particles " << std::endl;
        }
    }

    dec_W(*jet) = isW;
    dec_Z(*jet) = isZ;
    dec_LQ(*jet) = isLQ;
    dec_Top(*jet) = isTop;
    dec_LepPh(*jet) = isLepPh;
    dec_Meson(*jet) = isMeson;

    return true;
}

bool DiLepton::getParents(const xAOD::TruthParticle *tp, bool &isW, bool &isZ, bool &isLQ, bool &isTop, bool &isLepPh, bool &isMeson, bool msg)
{
    bool info = Debug ? msg : false;
    const char *APP_NAME = "DiLepton::getParents()";

    for (size_t i(0); i < tp->nParents(); i++)
    {
        const xAOD::TruthParticle *par = tp->parent(i);

        if (!isW)
            this->quarkFromWdR(par, info);

        if (par->isW())
            isW = true;
        if (par->isZ())
            isZ = true;
        if (par->isQuark())
            isLQ = true;
        if (par->isTop())
            isTop = true;
        if (par->isMeson())
            isMeson = true;
        if (par->isLepton() || par->isPhoton())
            isLepPh = true;
        if (info)
        {
            std::cout << APP_NAME << " DEBUG \t " << Form("\t Parent no %i (PDG=%i|W=%i|Z=%i|LQ=%i|Top=%i|LepPh=%i|Meson=%i)", (int)i, par->pdgId(), isW, isZ, isLQ, isTop, isLepPh, isMeson) << std::endl;
        }
    }
    return true;
}

float DiLepton::quarkFromWdR(const xAOD::TruthParticle *tp, bool msg)
{
    bool info = Debug ? msg : false;
    const char *APP_NAME = "DiLepton::quarkFromWdR()";

    float dr(0.);
    if (!tp)
        return dr;
    if (!tp->isW())
        return dr;

    std::vector<TLorentzVector> quarks;
    std::vector<float> charge, pdgIDs;
    for (size_t i(0); i < tp->nChildren(); i++)
    {
        const xAOD::TruthParticle *ch = tp->child(i);
        if (!ch->isQuark())
            continue;
        if (ch->pt() < 1E3)
            continue;
        quarks.push_back(ch->p4());
        charge.push_back(ch->charge());
        pdgIDs.push_back(ch->pdgId());
    }
    if (quarks.size() < 2)
        return dr;

    sortPt(quarks, charge, pdgIDs);
    dr = this->dR(quarks[0], quarks[1]);
    if (info)
    {
        std::cout << APP_NAME << " DEBUG \t "
                  << Form("W(pt=%.1f)::Q1[%i](pt=%.1f|eta=%.3f|phi=%.3f)  Q2[%i](pt=%.1f|eta=%.3f|phi=%.3f) dR=%.3f", tp->pt(),
                          (int)pdgIDs[0], quarks[0].Pt(), quarks[0].Eta(), quarks[0].Phi(), (int)pdgIDs[1], quarks[1].Pt(), quarks[1].Eta(), quarks[1].Phi(), dr)
                  << std::endl;
    }

    return dr;
}

std::vector<int> DiLepton::getSusyPDG(xAOD::TEvent *event)
{
    const char *APP_NAME = "DiLepton::getSusyPDG()";
    std::vector<int> pdgs(4);

    const xAOD::EventInfo *evt(0);
    if (!event->retrieve(evt, "EventInfo").isSuccess())
    {
        Error(APP_NAME, "Error retrieving EventInfo");
        return pdgs;
    }

    int /**pdgid1(0), pdgid2(0),*/ gluDecay1(0), gluDecay2(0);
    const xAOD::TruthParticleContainer *truthP(0);
    if (!event->retrieve(truthP, "TruthParticles").isSuccess())
    {
        Error(APP_NAME, "Failed to retrieve truth-info");
        return pdgs;
    }

    for (const auto &tp : *truthP)
    {
        if (tp->pdgId() == 1000021)
        {
            int _ch[2] = {0};
            int ich(0);
            bool foundSUSY(false);
            if (tp->nChildren() == 3)
            {
                for (unsigned int ch(0); ch < tp->nChildren(); ch++)
                {
                    for (const auto &tpC : *truthP)
                    {
                        if (tpC->barcode() == tp->child(ch)->barcode())
                        {
                            if ((abs(tpC->pdgId()) > 1000020 && abs(tpC->pdgId()) < 1000040))
                            {
                                foundSUSY = true;
                            }
                            else
                            {
                                _ch[ich] = tpC->pdgId();
                                ich++;
                            }
                        }
                    }
                }
            }
            if (foundSUSY && _ch[0] == -1 * _ch[1])
            {
                if (gluDecay1 == 0)
                    gluDecay1 = abs(_ch[0]);
                else
                    gluDecay2 = abs(_ch[0]);
            }
        }
    }

    if (Debug)
    {
        std::cout << APP_NAME << " DEBUG \t " << Form("GLDec1=%i | GLDec2=%i", gluDecay1, gluDecay2) << std::endl;
    }
    pdgs[0] = 0; /// pdgid1;
    pdgs[1] = 0; /// pdgid2;
    pdgs[2] = gluDecay1;
    pdgs[3] = gluDecay2;

    return pdgs;
}

bool DiLepton::isSherpaZllMC(const xAOD::EventInfo* eventInfo) {

  unsigned int datasetID = eventInfo->mcChannelNumber();

  if( (datasetID >= 700320) && (datasetID <= 700325) )
    return true;
  if( (datasetID >= 700335) && (datasetID <= 700337) )
    return true;
  if( (datasetID >= 700467) && (datasetID <= 700472) )
    return true;
  //if( (datasetID >= 700358) && (datasetID <= 700364) )
    //return true;

  return false;
}

