import os
import sys
import ROOT
import shutil
import logging
import datetime
#import commands
#####
import subprocess
from subprocess import call, Popen
from glob import glob
from argparse import ArgumentParser
from pathlib import Path

logging.basicConfig(level=logging.INFO)

now = datetime.datetime.now()

prefix = "Vjets_ABp13" # put a this prefix also in launch_jobs.py!
# SystABp197 and SystABp197V3 for prompt with SYST on
# SystABp197NMCSFs -> to have the New MCTemplate SFs (measured in February)

if not os.environ.get('BASEDIR'):
    print ("BASEDIR is not set. Please set it"); sys.exit(1)
if not os.environ.get('WORKDIR'):
    print ("WORKDIR is not set. Please set it"); sys.exit(1)

LocalInputsDir=  "{0}".format(os.environ['WORKDIR'])+"/INPUTFILES"
print(LocalInputsDir)
if not os.path.exists(LocalInputsDir):
    print ("LocalInputDir does not exist, creating"); os.makedirs(LocalInputsDir)
    print ("Move the data in this folder")

LocalOutputDir = "{0}".format(os.environ['WORKDIR'])+"/OUTPUTFILES/"+prefix+"/"+"TASK_" +now.strftime('%b%d_%Hh%Mm%Ss')
print(LocalOutputDir)
if not os.path.exists(LocalOutputDir):
    print ("LocalOutputDir does not exist, creating"); os.makedirs(LocalOutputDir)


command = "voms-proxy-info -all | grep nickname"
#output = commands.getoutput(command)
subprocess.call(["ls", "-l"])
#subprocess.Popen(command)
#output = subprocess.Popen(["voms-proxy-info", "all", "|", "grep", "nickname"])
#cernname = output.split()[4]

cernname=os.getlogin()
parser = ArgumentParser(description="Runs job locally or creates prun command for grid submission")
parser.add_argument("--submitDir", help="dir to store the output", default=LocalOutputDir)
parser.add_argument("--inputDS",   help="input DS files, used for GRID mode",       default="none")
parser.add_argument("--inputAllDS", help="input DS when isAllSamples is used", default="none")
parser.add_argument("--outputDS", help="output DS name", default="default")
parser.add_argument("--driver",    help="select where to run",     choices=("LOCAL", "PROOF", "GRID"), default="LOCAL")
#parser.add_argument("--InPath", help="path to input dir, used for local mode", default=LocalInputsDir)
#parser.add_argument("--LocalSample", help="input sample used for local mode", default="mc20_13TeV_AF3")
  
parser.add_argument("--InPath", help="path to input dir, used for local mode", default="/eos/user/b/bngair/hpp_ml/INPUTFILES/")
#parser.add_argument("--LocalSample", help="input sample used for local mode", default="Signal_HNL_p4149v")
parser.add_argument("--LocalSample", help="input sample used for local mode", default="Signal_HNL_p41490")
#parser.add_argument("--LocalSample", help="input sample used for local mode", default="Signal_HNL_pr22")
#parser.add_argument("--LocalSample", help="input sample used for local mode", default="mc20_13TeV_p5855")
#parser.add_argument("--LocalSample", help="input sample used for local mode", default="mc20_13TeV_eos")
parser.add_argument("--Debug",       type=int, help="Print debug messages", default=0) # 1 = MSG::ERROR, 2 = MSG::DEBUG, 3 = MSG::VERBOSE, default MSG::INFO for SUSY tools
parser.add_argument("--nevents",     type=int, help="number of events to process for all the datasets")
parser.add_argument("--skip-events", type=int, help="skip the first n events")
parser.add_argument("--isAllSamples", help="isAllSamples flag [0/1]", default=0)
parser.add_argument("--overwrite", action='store_true', default=True, help="overwrite previous submitDir")
parser.add_argument("--isData",    type=int, help="Data flag [0/1]",    default=0)
parser.add_argument("--makeHist",  type=int, help="Add Histos to output  [0/1]", default=1)
parser.add_argument("--makeTree",  type=int, help="Add TTree to output [1/0/2]", default=1)
parser.add_argument("--R1Jets",    type=int, help="Add R1Jets to output[No=0/FatJets=1/RecJets=2]",default=0) # make sure the info in the .conf file is set (using deactivated by default, to not store the syst)
parser.add_argument("--setLeptons",type=int, help="Leptons for SS 0=Leptons, 1=Muons, 2=Electrons", default=0)
parser.add_argument("--doSys",     type=int, help="Enables systematic variations", default=0)
parser.add_argument("--isFakeMC",     type=int, help="Enables the MC corrections for fake leptons [0/1]", default=0)
parser.add_argument("--Trigger",   help="Set trigger name",  default="DILEPTON_MET")
parser.add_argument("--SysList",   help="Comma-separated list of systematics", default="")
parser.add_argument("--SignalRegion", help="Comma-separated list of Signal Regions", default="SRall")
parser.add_argument("--NSignalLeptons", type=int, help="How many leptons have to be signal type, >=3 means all of them should be signal", default=3)
parser.add_argument("--doBJetVeto",     type=int, help="Applies the b-jet veto", default=0) # 0 or 1
parser.add_argument("--PileupReweighting", type=int, help="Do PileupReweighting for MC [1/0]", default=0)
parser.add_argument("--useSite", help="Site name where jobs are sent", default="")
parser.add_argument("--nGBPerJob", type=int, help="Number of GB per job", default=0)
parser.add_argument("--excludedSite", help="List of sites which are not used for site section", default="ANALY_MWT2_VP, MWT2")
parser.add_argument("--nFilesPerJob", type=int, help="Number of files per job", default=5)
parser.add_argument("--isPHYSLITE",    type=int, help="PHYSLITE flag [0/1]",    default=0)

options = parser.parse_args()

import atexit
@atexit.register
def quite_exit():
    ROOT.gSystem.Exit(0)

if options.overwrite:
    logging.info("overwrite directory %s", options.submitDir)
    shutil.rmtree(options.submitDir, True)

#Set up the job for xAOD access:
ROOT.xAOD.Init().ignore();

# create a new sample handler to describe the data files we use
logging.info("creating new sample handler")
sh_all = ROOT.SH.SampleHandler()

if options.inputDS != "none" or options.inputAllDS != "none": # run on the grid
  if int(options.isAllSamples)==0:
    ROOT.SH.scanRucio (sh_all, options.inputDS);
       
    current=-1
    for z in range(1,5):
      current = (options.inputDS).find(".",current+1)
      if z==1: first= current+1
      if z==3: last = current
    short_name = options.inputDS[first:last]
    print ("Short Name: ", short_name)
    if short_name.find("*")>-1: short_name = short_name.replace("*","")
    logging.info("Outname %s", short_name)
    SampleName = options.inputDS
    print ("SampleName: ", SampleName)

  else:
    ROOT.SH.addGridCombinedFromFile(sh_all, 'AllSamplesCombined', options.inputAllDS)
    SampleName = 'AllSamplesCombined'

else :
  LocalInputDir = "{0}/{1}".format(options.InPath, options.LocalSample)
  print(LocalInputDir)
  if not os.path.exists(LocalInputDir):
      print ("LocalInputDir does not exist, aborting"); sys.exit(1)
   
  logging.info("LocalInputDir  %s", LocalInputDir)
  logging.info("LocalOutputDir %s", LocalOutputDir)

  search_directories = []
  search_directories = (LocalInputDir,)

  # scan for datasets in the given directories
  short_name = "output"
  for directory in search_directories:
    ROOT.SH.ScanDir().scan(sh_all,directory)
    SampleName = glob(LocalInputDir+"/*")[-1]


# print out the samples we found
logging.info("%d different datasets found scanning all directories", len(sh_all))
print ("SampleName: ", SampleName)

if(SampleName.find("user.")>-1) and int(options.isAllSamples)==0:
    print (SampleName)
    runnumber  = SampleName.split(".")[2]
    mc20v      = SampleName.split(".")[6]
    SampleName = SampleName.split(".")[3]    
elif int(options.isAllSamples)==0:
    runnumber  = SampleName.split("/")[-1].split(".")[1]
    mc20v      = SampleName.split("/")[-1].split(".")[5]
    SampleName = SampleName.split("/")[-1].split(".")[2]
else:
    SampleName = "isAllSamples"+prefix
    runnumber = "many_DIDS"
    mc20v = "will be taken from the list name"
        
logging.info("Sample Name: %s", SampleName)
logging.info("Run Number: %s", runnumber)
logging.info("MC20 version: %s", mc20v)

outname = "user.{0}.{1}_{2}.{3}.{4}/".format(cernname,prefix,runnumber,SampleName,mc20v)
if int(options.isAllSamples)==0:
    logging.info("--OutDS = %s", outname)
    options.outputDS = outname

# set the name of the tree in our files
sh_all.setMetaString("nc_tree", "CollectionTree")

# this is the basic description of our job
logging.info("creating new job")
job = ROOT.EL.Job()
job.sampleHandler(sh_all)

if options.nevents:
    logging.info("processing only %d events", options.nevents)
    job.options().setDouble(ROOT.EL.Job.optMaxEvents, options.nevents)

if options.skip_events:
    logging.info("skipping first %d events", options.skip_events)
    job.options().setDouble(ROOT.EL.Job.optSkipEvents, options.skip_events)

if len(options.useSite):
    logging.info("send jobs to %s", options.useSite)
    job.options().setString(ROOT.EL.Job.optGridSite, options.useSite)

if len(options.excludedSite):
    logging.info("exclude sites %s", options.excludedSite)
    job.options().setString(ROOT.EL.Job.optGridExcludedSite, options.excludedSite)

if options.nGBPerJob:
    logging.info("%i GB per job",options.nGBPerJob)
    job.options().setDouble(ROOT.EL.Job.optGridNGBPerJob, options.nGBPerJob)

if options.nFilesPerJob:
    logging.info("%i GB per job",options.nFilesPerJob)
    job.options().setDouble(ROOT.EL.Job.optGridNFilesPerJob, options.nFilesPerJob)
# add our algorithm to the job
logging.info("creating algorithms")
alg = ROOT.DiLepton()
alg.Debug = options.Debug
alg.isAllSamples = int(options.isAllSamples)
alg.isData = options.isData
alg.SysList = options.SysList
alg.NSignalLeptons = options.NSignalLeptons;
alg.doBJetVeto = options.doBJetVeto;
alg.doSys = options.doSys
alg.isFakeMC = options.isFakeMC
alg.makeHist = options.makeHist
alg.makeTree = options.makeTree
alg.setLeptons = options.setLeptons
alg.TriggerName = options.Trigger
alg.SigRegion = options.SignalRegion
alg.PileupReweighting = options.PileupReweighting
alg.inputAllDS = options.inputAllDS
alg.isPHYSLITE = options.isPHYSLITE


logging.info("adding algorithms")
job.algsAdd(alg)

# make the driver we want to use:
logging.info("creating driver")
driver = None
if (options.driver == "LOCAL"):
    logging.info("running on direct")
    driver = ROOT.EL.DirectDriver()
    logging.info("submit job")
    driver.submit(job, options.submitDir)
elif (options.driver == "PROOF"):
    logging.info("running on prooflite")
    driver = ROOT.EL.ProofDriver()
    logging.info("submit job")
    driver.submit(job, options.submitDir)
elif (options.driver == "GRID"):
    logging.info("running on Grid")
    driver = ROOT.EL.PrunDriver()
    #options.submitDir = MyLOGDir + 'TASK_' +now.strftime('%b%d_%Hh%Mm%Ss')
    #options.submitDir = $LogsDir + 'TASK_' +now.strftime('%b%d_%Hh%Mm%Ss')
    logging.info("submitDir = %s ", options.submitDir)
    logging.info("CERN user = %s", cernname)
    logging.info("--outputDS = %s", options.outputDS)
    driver.options().setString("nc_outputSampleName", options.outputDS)
    driver.options().setDouble("nc_disableAutoRetry", 0)
    driver.options().setDouble("nc_nFilesPerJob", 1)
    # driver.options().setDouble(ROOT.EL.Job.optGridMergeOutput, 1)
    driver.options().setString (ROOT.EL.Job.optSubmitFlags, '--addNthFieldOfInDSToLFN=2,3,6 --noEmail') # --osMatching when runing from sl7, --forceStaged --allowTaskDuplication 
	# If willing to specify an output Storage element for your job
    # driver.options().setString("nc_destSE", "RO-02-NIPNE_LOCALGROUPDISK")
    # driver.options().setString("nc_destSE", "RO-07-NIPNE_LOCALGROUPDISK")
    logging.info("submit job")
    driver.submitOnly(job, options.submitDir)

if(options.driver == "LOCAL"):
    logging.info("sent output to %s", LocalOutputDir)

#f.close()
print ("...done")
