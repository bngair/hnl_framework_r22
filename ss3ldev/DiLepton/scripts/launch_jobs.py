import os
import re
import sys
import datetime
#import commands
import subprocess
import time
from glob import glob
from argparse import ArgumentParser
import re, pickle

def runCommand(cmd, verbose = False):
    if verbose:
        print ("  Will run the following command: %s" % cmd)
    cmdResult = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return cmdResult.stdout
def printSamplesDict(s):
    row_format ="{0:<15}{1:^30}{2:^30}{3:<120}"
    print (row_format.format("=== DSID ===", "=== AOD ===", "=== DAOD (AOD) ===", "=== DAOD dataset name ==="))
    for ds in s:
        dsName = s[ds]["DAOD"]
        if dsName == " N/A ":
            dsName = "(AOD: " + s[ds]["AOD"] + ")"
        print(row_format.format(ds, s[ds]["AODstatus"], s[ds]["DAODstatus"], dsName))


prefix = "NoSystABp112"
command = "voms-proxy-info -all | grep nickname"
# queryPattern = pattern + defaultRecoTagFilteringPattern + "*"
#    cmd = "rucio ls --short --filter type=CONTAINER %s | sort -r " % (queryPattern) # sort -r prioritizes higher e-tags
#    queryResult = runCommand(cmd, verbose)
output=runCommand(command)

#output = commands.getoutput(command)
#lines = [line.rstrip() for line in output.readlines()]
#for line in lines:
#   print (line)
#cernname = output.split()[4]

cernname=os.getlogin()
parser = ArgumentParser(description="Script for mutiple grid job submission")
parser.add_argument("--Submit",  type=int, help="Launch submission command [1/0]", default=1)
parser.add_argument("--DQ2",     help="List of datasets to submitt", default="none")
parser.add_argument("--inputAllDS", help="input DS when isAllSamples is used", default="none")
parser.add_argument("--doSys",    type=int, help="Enables systematic variantions [0/1]", default=0)
parser.add_argument("--isFakeMC", type=int, help="Enables the MC corrections for fake leptons [0/1]", default=0)
parser.add_argument("--Trigger",  help="Trigger application. Default dilepton+EtMiss",   default="DILEPTON_MET")
parser.add_argument("--SysList",  help="Comma separated systematic variations", default="")
parser.add_argument("--makeHist", type=int, help="Add Histos to output [0/1]",   default=1)
parser.add_argument("--makeTree", type=int, help="Add TTree to output  [1/0/2]", default=1)
parser.add_argument("--R1Jets",   type=int, help="Add R1Jets to output [No=0/FatJets=1/RecJets=2]", default=0)
parser.add_argument("--setLeptons",     type=int, help="lepton merging option", default=0)
parser.add_argument("--NSignalLeptons", type=int, help="How many signal leptons are needed, >=3 means all of them should be signal", default=3)
parser.add_argument("--doBJetVeto",     type=int, help="Applies the b-jet veto", default=0) # 0 or 1
parser.add_argument("--SignalRegion",   help="Process only one Signal Region", default="SRall")
parser.add_argument("--nGBPerJob", type=int, help="Number of GB per job", default=0)
parser.add_argument("--nFilesPerJob", type=int, help="Number of files per job", default=5)
options = parser.parse_args()

print(options.DQ2)

#if options.DQ2 == "none" and options.inputAllDS == "none":
#    print "please specify a file with the list of samples"; sys.exit(1)

#if not os.environ.get('BASEDIR'):
#    print "BASEDIR is not set. Please set it"; sys.exit(1)

os.chdir(os.environ["BASEDIR"]+"/ss3ldev/Utils/")


NJobs = 0
if options.inputAllDS == "none":
   print(options.DQ2)
   print("One sample per JEDI task")
  
   table = "{0}/ss3ldev/Utils/List_of_Files/{1}".format(os.environ["BASEDIR"],options.DQ2)
   print(table)
   print("Looking for datasets in {0}".format(table))
   
   if not len(table) or not os.path.exists(table):
       print(os.path.exists("{0}/ss3ldev/Utils/List_of_Files/submit_cutflow-mc16a.txt".format(os.environ["BASEDIR"])))
       print("Not len(table) Please specify a DQ2 file"); sys.exit(1)
   
   
   for line in open(table):
       dataset = line.split()[0]
       if dataset[0]=="#": continue
       if dataset.find(":")>-1: dataset = dataset.split(":")[1]
       print(dataset)
       if dataset.find("user.")>-1: MCId = dataset.split(".")[2]
       else: MCId = dataset.split(".")[1]
       isData     = 0
       if dataset.find("data")>-1: isData = 1
       print("MCId ", MCId, " isData ", isData, " isAtlfast Will not longer be judged before submitting to the grid")
   
       command = "python {15}/ss3ldev/DiLepton/scripts/run.py"\
           " --inputDS={0} --isData={1}  --doSys={2}"\
           " --isFakeMC={3} --Trigger={4} --SysList={5} --makeHist={6} --makeTree={7} --R1Jets={8}"\
           " --setLeptons={9} --NSignalLeptons={10} --doBJetVeto={11} --SignalRegion={12} --nGBPerJob={13} --nFilesPerJob={14} --driver=GRID".format(dataset, isData, options.doSys, options.isFakeMC, options.Trigger, options.SysList, options.makeHist, options.makeTree, options.R1Jets, options.setLeptons, options.NSignalLeptons, options.doBJetVeto, options.SignalRegion, options.nGBPerJob,options.nFilesPerJob,os.environ["BASEDIR"])
       print(command)
   
       if options.Submit:
         os.system(command)
         NJobs = NJobs+1
         time.sleep(1)
  
    
if options.DQ2 == "none":
   print(options.inputAllDS)
   print("Many samples per JEDI task (not supported for Data!!)")

   Flist = "{1}/ss3ldev/Utils/List_of_Files/{0}".format(options.inputAllDS,os.environ["BASEDIR"])
   print(Flist)
   print("Will consider all the datasets in {0}".format(Flist))
   
   if not len(Flist) or not os.path.exists(Flist):
       print(os.path.exists("{0}/ss3ldev/Utils/List_of_Files/submit_cutflow-mc16a.txt".format(os.environ["BASEDIR"])))
       print("Not len(Flist) Please specify a list of files!"); sys.exit(1)  
   
   _name = (options.inputAllDS).replace('.txt', '')
   name = _name.replace('submit_','')
   outds = 'user.'+cernname+'.'+prefix+'.'+name
   print("outds : ", outds) 
   

   command = "python {0}/ss3ldev/DiLepton/scripts/run.py"\
       " --isAllSamples=1 --isData=0 --doSys={1}"\
       " --isFakeMC={2} --Trigger={3} --SysList={4} --makeHist={5} --makeTree={6} --R1Jets={7}"\
       " --setLeptons={8} --NSignalLeptons={9} --doBJetVeto={10} --SignalRegion={11} --nGBPerJob={12} --nFilesPerJob={13}  --inputAllDS={14} --outputDS={15} --driver=GRID ".format(os.environ["BASEDIR"], options.doSys, options.isFakeMC, options.Trigger, options.SysList, options.makeHist, options.makeTree, options.R1Jets, options.setLeptons, options.NSignalLeptons, options.doBJetVeto, options.SignalRegion, options.nGBPerJob,options.nFilesPerJob, Flist, outds)
   print(command)
   
   if options.Submit:
       os.system(command)
       NJobs = NJobs+1
       time.sleep(1)
   

   
print
time.sleep(1)

if options.Submit:
    print("... submitted {0} jobs".format(NJobs))

print("...done")
