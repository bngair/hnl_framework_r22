/*****************************************************************************
File Name        : FakeLepMCTemplate.cxx                             
Author           : Othmane Rifki			                            
Email            : othmane.rifki@cern.ch			                    
Description      : Source file for the MCTemplateCorr class          
                                                                     
Example of usage:                                                    
  https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYSameSignLeptonsJetsRun2#MC_template_method_not_up_to_dat
  https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/FakeObjectBgEstimation#The_MC_template_Method
  
  
  
  In your analysis code, include the header: include "MCTemplateCorr.h"
  Before the event loop, declare:  MCTemplateCorr fakeCorr;            
																		 
  In the event loop, for each event add the collection of signal leptons
  The truth variables you need to pass are:                            
	For electrons: El_truthType, El_firstEgMotherTruthType, El_TruthClassifierFallback_truthType, El_truthOrigin, El_firstEgMotherTruthOrigin, El_TruthClassifierFallback_truthOrigin, El_truthPdgId, El_firstEgMotherPdgId
	For muons: Mu_type, Mu_TruthClassifierFallback_truthType, Mu_origin, Mu_TruthClassifierFallback_truthOrigin                                        
  Lepton pT is in MeV
  Do not pass absolute values!

  float fakeW=1;
  // fake correction factors
  for (int lepi = 0; lepi < lep_signal.num_leptons; lepi++)
  {
	if(lep_signal.is_electron[lepi]) 
		fakeCorr.AddElectron(evt.ChannelNumber, lep_signal.pT[lepi], lep_signal.charge[lepi], evt.El_truthType[lep_signal.index[lepi]],evt.El_firstEgMotherTruthType[lep_signal.index[lepi]],evt.El_TruthClassifierFallback_truthType[lep_signal.index[lepi]], evt.El_truthOrigin[lep_signal.index[lepi]],evt.El_firstEgMotherTruthOrigin[lep_signal.index[lepi]],evt.El_TruthClassifierFallback_truthOrigin[lep_signal.index[lepi]], evt.El_truthPdgId[lep_signal.index[lepi]],evt.El_firstEgMotherPdgId[lep_signal.index[lepi]]);
	else 
		fakeCorr.AddMuon(evt.ChannelNumber, lep_signal.pT[lepi], lep_signal.charge[lepi], evt.Mu_type[lep_signal.index[lepi]],evt.Mu_TruthClassifierFallback_truthType[lep_signal.index[lepi]],  evt.Mu_origin[lep_signal.index[lepi]], evt.Mu_TruthClassifierFallback_truthOrigin[lep_signal.index[lepi]]);
  }
  float uncertainty;
  fakeW = fakeCorr.GetFakeCorrection(uncertainty);	  
  
  GetFakeCorrection returns a weight that you need to apply to your MC (of ttbar,Z+jets,W+jets) to get the fake contribution. 
  You can also get an uncertainty on the correction which can be used to determine the uncertainty on the background estimate.
  
  
  
  Few additional remarks:
    It is important to use a consistent set of MC shower since the corrections were derived with this configuration: either Powheg or Sherpa for ttbar, W+jets, Z+jets.
    There are two sets of corrections that are hard-coded based on which MC used: Powheg or Sherpa. The tool will apply the correct corrections based on the channel number that is passed to it. !!!! if these changes, the MCTemplateCorr.cxx should be changed to update the DIDS of the samples used and MCTemplateCorr.h to update the correction factors
    The collection of leptons get sorted as a function of pT. This is relevant only when more than one fake is present in the event. The event is classified based on the higher lepton pT.

 SusyTools will include functions to classify leptons. In the mean time, we suggest this strategy (T=truthType, O=truthOrigin, PdgId =firstEgMotherPdgId):

    Prompt electrons and muons:
        Electrons: ( T=2 || ( O=5 && | PdgId | =11) ) && ( recoCharge*PdgId < 0 )
        Muons: T=6 
    Charge flip electrons:
        ( T=2 || ( O=5 && | PdgId | =11) ) && ( recoCharge*PdgId > 0 ) 
    Heavy flavor electrons and muons:
        Electrons: ( T=3 && ( O=26 || O=33 || O=25 || O=32 ) )
        Muons: ( T=7 && ( O=26 || O=33 || O=25 || O=32 ) ) 
    Light flavor electron and muons:
        All remaining leptons not classified as charge flip or heavy flavor 


*/



#ifndef FakeLepMCTemplate_h
#define FakeLepMCTemplate_h

#include "TF1.h"

const int MAX_LEPT = 10;
const int LeptPtMIN = 10000;
const int NCorrections = 4;

/// defines the truth lepton collection in the event
struct my_lep {
  int channelNumber;
  int num_leptons;
  bool is_electron[MAX_LEPT];
  double pT[MAX_LEPT];
  int recoCharge[MAX_LEPT];
  int truthType[MAX_LEPT];
  int truthFirstMumType[MAX_LEPT];
  int truthFallBackType[MAX_LEPT];
  int truthOrigin[MAX_LEPT];
  int truthFirstMumOrigin[MAX_LEPT];
  int truthFallBackOrigin[MAX_LEPT];
  int truthPdgId[MAX_LEPT];
  int truthFirstMumPdgid[MAX_LEPT];
  int recoPdgId[MAX_LEPT];
  bool is_fake_HF[MAX_LEPT];
  bool is_fake_LF[MAX_LEPT];
  bool is_chmisid[MAX_LEPT];
}; 

/// refers to individual lepton and not the collection 
struct srtLep {
  double pT;
  int ind;
};

class MCTemplateCorr
{
 public:
  
  MCTemplateCorr();
  ~MCTemplateCorr() {}
  void AddElectron(int channelnumber, double pt_in_mev, int charge, int type, int firstmum_type, int fallback_type, int origin, int firstmum_origin, int fallback_origin, int pdgid, int firstmum_pdgid);
  void AddMuon(int channelnumber, double pt_in_mev, int charge, int type, int fallback_type, int origin, int fallback_origin);
  float GetFakeCorrection(float& unc);
  void  SetMCResult(int channelnum);
  void SetMCResult(int channelnum, float *corr, float *err);
  void Reset();

 private:
  void classify_leptons(my_lep *lep);  
  static int lep_pt_comparator(const void * a, const void * b);
  void sortLeptons(my_lep *lep);

 protected: 
  bool m_reset;
  my_lep m_lepinfo;
  int m_index=0;
  bool debug_me = false;

  float chMisId_Correction = 1.0;
  float HF_EL_Correction = 1.0;
  float HF_MU_Correction = 1.0;
  float LF_EL_Correction = 1.0;
  float LF_MU_Correction = 1.0;

  float chMisId_Uncert = 0.0;
  float HF_EL_Uncert = 0.0;
  float HF_MU_Uncert = 0.0;
  float LF_EL_Uncert = 0.0;
  float LF_MU_Uncert = 0.0;

  /// order: template3_ChFlip template4_LFHFe template5_AllButBm template6_Bm
  /// February 23, 2022 strong SS/3L analysis 
  float unc_1 = sqrt(pow(0.077028,2)+pow(0.98679*0.18,2));
  float unc_2 = sqrt(pow(0.14486,2)+pow(1.5781*0.70,2));
  float unc_3 = sqrt(pow(0.13406,2)+pow(1.9758*0.25,2));
  float unc_4 = sqrt(pow(0.046276,2)+pow(1.0587*0.25,2));
  const float pythia_correction[NCorrections] = {0.98679,1.5781,1.9758,1.0587};
  const float pythia_uncertainty[NCorrections] = {unc_1,unc_2,unc_3,unc_4};
  const float sherpa_correction[NCorrections] = {0.98679,1.5781,1.9758,1.0587};
  const float sherpa_uncertainty[NCorrections] = {unc_1,unc_2,unc_3,unc_4};
 

  const float no_correction[NCorrections] = {1.,1.,1.,1.};
  const float no_uncertainty[NCorrections] = {0.,0.,0.,0.};

};


#endif
