#ifndef DiLepton_DiLepton_H
#define DiLepton_DiLepton_H

//EDM includes
#include <EventLoop/Algorithm.h>
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/TActiveStore.h"
#include "SUSYTools/ISUSYObjDef_xAODTool.h"
#include "SUSYTools/SUSYCrossSection.h"
#include "MCTruthClassifier/MCTruthClassifier.h"
#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"

//xAOD Object includes
#ifndef __MAKECINT__
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJet.h"
#include "xAODTau/TauJetContainer.h"
#endif

//other includes
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <utility>
#include <memory>
#include <TMath.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TTree.h>
#include <TFile.h>
#include <TSystem.h>
#include <TLorentzVector.h>
#include "AsgTools/AnaToolHandle.h"
#include <regex>
#include "AsgTools/ToolHandleArray.h"

#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include "TriggerAnalysisInterfaces/ITrigGlobalEfficiencyCorrectionTool.h"
#include "MuonAnalysisInterfaces/IMuonTriggerScaleFactors.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h"
#include "EgammaAnalysisInterfaces/IAsgElectronEfficiencyCorrectionTool.h"
#include "EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h" 
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronEfficiencyCorrection/ElectronChargeEfficiencyCorrectionTool.h"
#include "AsgAnalysisInterfaces/IEfficiencyScaleFactorTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronChargeIDSelectorTool.h"
#include "IsolationSelection/IIsolationSelectionTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "IsolationCorrections/IIsolationCorrectionTool.h"
#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"
#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"
#include "AsgMessaging/StatusCode.h"
#include <DiLepton/MCTemplateCorr.h>
//#include "TriggerMatchingTool/IMatchingTool.h"
//#include "TrigConfInterfaces/ITrigConfigTool.h"
//#include "TrigDecisionTool/TrigDecisionTool.h"
//#include "TriggerMatchingTool/IMatchScoringTool.h"
#include "TrigGlobalEfficiencyCorrection/TrigGlobalEfficiencyCorrectionTool.h"
//#include "TriggerMatchingTool/MatchFromCompositeTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TriggerMatchingTool/MatchingTool.h"

 
// Framework include(s) - base class
#include "AsgTools/AsgMetadataTool.h"
// For reading metadata
 #include "xAODMetaData/FileMetaData.h"
#include "GammaORTools/IVGammaORTool.h"


#ifndef XAOD_STANDALONE // For now metadata is Athena-only
 #include "AthAnalysisBaseComps/AthAnalysisHelper.h"
#endif


namespace CP{
  class MuonTriggerScaleFactors;
  class IMuonTriggerScaleFactors;
}
using namespace CP;

class JetCleaningTool;
class MCTruthClassifier;
class JetReclusteringTool;
class AsgElectronChargeIDSelectorTool;


namespace LHAPDF{
  class PDFSet;
  class PDF;
}

class DiLepton : public EL::Algorithm
{
 
#ifndef __CINT__
  MCTruthClassifier *MCClassifier; //!
  MCTemplateCorr fakeLepCorr; //!
  
  asg::AnaToolHandle<ST::ISUSYObjDef_xAODTool> SusyObjTool{"ST::SUSYObjDef_xAOD/SS3ldev_SusyTools"}; //!
  asg::AnaToolHandle<IGoodRunsListSelectionTool> GRLTool{"GoodRunsListSelectionTool/SS3ldev_grl"}; //!
asg::AnaToolHandle<IVGammaORTool> m_VgammaORTool{"VGammaORTool/VGammaORTool"}; //!
  asg::AnaToolHandle<CP::IMuonTriggerScaleFactors> muonTool{"CP::MuonTriggerScaleFactors/MuonTrigEff/ss3ldev_muonTool"}; //!
  std::vector<asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool>> factoryEle; //!
  asg::AnaToolHandle<ITrigGlobalEfficiencyCorrectionTool> myTriggerSFTool{"TrigGlobalEfficiencyCorrectionTool/ss3ldev_TrigGlobal"}; //!
  asg::AnaToolHandle<ITrigGlobalEfficiencyCorrectionTool> myTriggerSFToolUP{"TrigGlobalEfficiencyCorrectionTool/ss3ldev_TrigGlobalUP"}; //!
  asg::AnaToolHandle<ITrigGlobalEfficiencyCorrectionTool> myTriggerSFToolDOWN{"TrigGlobalEfficiencyCorrectionTool/ss3ldev_TrigGlobalDOWN"}; //!
//  ToolHandle<ITrigGlobalEfficiencyCorrectionTool> m_trigGlobalEffCorrTool_diLep; //!
  asg::AnaToolHandle<ITrigGlobalEfficiencyCorrectionTool> m_trigGlobalEffCorrTool_diLep; //!
  asg::AnaToolHandle<Trig::IMatchingTool> m_trigMatchingTool; //!
//  ToolHandle<Trig::MatchingTool> m_trigMatchingTool; //!
  //asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfTool; //!
 // asg::AnaToolHandle<Trig::TrigDecisionTool> m_trigDecTool; //!
 // asg::AnaToolHandle<Trig::IMatchScoringTool> m_trigDRScoringTool; //!
  ToolHandleArray<CP::IMuonTriggerScaleFactors> m_muonTrigSFTools; //!
  asg::AnaToolHandle<CP::IMuonTriggerScaleFactors> m_muonTriggerSFTool; //!
  std::vector<asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool>> m_elecEfficiencySFTool_trig_mixLep; //!
  std::vector<asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool>> m_elecEfficiencySFTool_trigEff_mixLep; //!
  ToolHandleArray<IAsgElectronEfficiencyCorrectionTool> m_elecTrigSFTools; //!
  ToolHandleArray<IAsgElectronEfficiencyCorrectionTool> m_elecTrigEffTools; //!
#endif 


  private:
  std::ofstream cutflow_file_trig; //!
  std::ofstream cutflow_file_baseline; //!
  std::ofstream cutflow_file_all; //!
  std::ofstream cutflow_file_EWcombi; //!
  std::ofstream cutflow_file_SS; //!
  std::ofstream cutflow_file_emu; //!
  std::ofstream cutflow_file_ee; //!
  std::ofstream cutflow_file_mumu; //!
  std::ofstream cutflow_file_badmu; //!
  std::ofstream cutflow_file_pvt; //!
  std::ofstream cutflow_file_jetOR; //!
  std::ofstream cutflow_file_badjet; //!
  std::ofstream cutflow_file_sjet; //!
  std::ofstream cutflow_file_grl; //!
  std::ofstream cutflow_file_flags; //!
  std::ofstream cutflow_file_ee_bjet; //!
  std::ofstream cutflow_file_emu_bjet; //!
  std::ofstream cutflow_file_mumu_bjet; //!
  std::ofstream cutflow_file_ee_jet; //!
  std::ofstream cutflow_file_emu_jet; //!
  std::ofstream cutflow_file_mumu_jet; //!
  std::ofstream cutflow_file_ee_MET; //!
  std::ofstream cutflow_file_emu_MET; //!
  std::ofstream cutflow_file_mumu_MET; //!

  bool isMC; //!  

  std::vector<std::string> split_elements(const std::string& s, const std::string& delim); //!
  
  Long_t    EventNumber; //!
  Long_t    RunNumber; //!
  unsigned int randomRN; //!

  int LB; //!
  int MCId; //!
  int configYear; //!
  float IntPerX_CorrectedScaled; //!
  int NFiles; //!
 
  int GLDec1; //!
  int GLDec2; //! 
  int isGGMHinoZh; //!

  unsigned int NSys; //!
  unsigned int NSr; //!

  int   xAODEventSum; //!
  float xAODWeightSum; //!

  float EventWeight; //!
  float PileUpWeight; //!
  float ObjectSF; //!
  float triggerSF; //!
  float FakeLepMCTemplateWeight; //!
  float FakeMuMCTemplateUnc; //!
  float FakeEleMCTemplateUnc; //!
  float weight; //!
  float m_btagSFCentral; //!
  float weightFakeMu_MCTemplate; //!
  float weightFakeEl_MCTemplate; //!
  float uncFakeMu_MCTemplate; //!
  float uncFakeEl_MCTemplate; //!
  float m_jvtSF; //!
 
  std::vector<float> SysWeights; //!
  /// std::vector<float> PDFWeights; //!
  std::vector<std::string> SysNames; //!  

  float EtMiss; //!
  float EtMissPhi; //!
  float EtMissSig; //!
  float meff; //!
  float minv; //!
  float mt; //!
  float ht; //!
  float mj; //!
  float mjj; //!
  float mjjdR; //!
  float mjRec; //!
  float truthHt; //!
  float truthMET; //!

  float mu_1stPt; //!
  float mu_2ndPt; //!
  float mu_3rdPt; //!
  float mu_1stEta; //!
  float mu_2ndEta; //!
float jet_1stPhi;
float jet_1stEta;
float jet_2ndPhi;
float jet_2ndEta;
float jet_3thEta;
float jet_4thEta;
float c3;
float c4;
float jet_diffPhi;
float jet_mulEta;
float jet_diffEta;

int Njet25x; //!
///////
float ptll ;
float ptllphi ;
float ptllx;
float ptlly;
float etx;
float ety;
float mll;
float etll;
float mtn;
float Mt;
float m13;
float m14;
float m24;
float m23;
float mrel3;
float mrel4;
  float el_1stPt; //!
  float el_2ndPt; //!
  float el_3rdPt; //!
  float el_1stEta; //!
  float el_2ndEta; //!
  float lep_1stPt; //!
  float lep_2ndPt; //!
  float lep_3rdPt; //!
  float jet_1stPt; //!
  float jet_2ndPt; //!
  float jet_3rdPt; //!
float lep_1stEta; //!
  float lep_2ndEta; //!
  float lep_1stPhi; //!
  float lep_2ndPhi; //!

  /// int isZevent; //!
  TString topoStr; //!

  int mu_n_EWcombi; //!
  int el_n_EWcombi; //!  
  int mu_n; //!
  int el_n; //!
  int lep_n; //!
  int jet_n; //!
  int b_n; //!

  int NlepBL; //!
  int Njet25; //!
  int Njet35; //!
  int Njet40; //!
  int Njet50; //!
  int Nbjet; //!
  int NSignalBjet; //!
  int SSChannel; //!
  int EventTM; //!
  int nLepCut;
  
  std::vector<float> muCharges; //!
  std::vector<float> elCharges; //!
  std::vector<float> lepCharges; //!  
  std::vector<float> jetCharges; //!
  std::vector<float> muQualities; //!
  std::vector<float> elQualities; //!
  std::vector<float> lepQualities; //!   
  std::vector<float> jetQualities; //!
  std::vector<TLorentzVector> muTLV; //!
  std::vector<TLorentzVector> elTLV; //!
  std::vector<TLorentzVector> lepTLV; //!
  std::vector<TLorentzVector> lepTLV_BL;//!
  std::vector<TLorentzVector> jetTLV; //!
  std::vector<TLorentzVector> jetTLV_R1; //!
  std::vector<float> jetBtag; //!
  std::vector<float> jetD12_R1;//!
  std::vector<float> muSF; //!
  std::vector<float> elSF; //!
  std::vector<float> jetDL1r; //!
  std::vector<float> jetJVT; //!
  std::vector<int> lepTruth; //! 
  
  bool InitializeTree(); //!
  bool InitializeHistograms(); //!
  bool InitializeTriggerTools(std::string var=""); //!
  
  void FillTree(TTree *t, bool fill); //!
  void FillHisto(TH1 *h, float x, float w=1., bool fill=1); //!
  void FillHisto(TH2 *h, float x, float y, float w=1., bool fill=1); //!
  
  void setROOTmsg(int level); //!
  bool containStr(std::vector<std::string> v, std::string name); //!
  std::vector<std::string> tokenize(std::string); //!
  bool match(TLorentzVector v1, TLorentzVector v2); //!  
  int getCutNumber(std::vector<TLorentzVector> particles, float ptcut); //!

  static const int NSR  = 5; //!
  static const int NSYS = 100; //!
  struct SR {
    TString  Name;
    int      Njet; 
    int      NBjet;
    int      NBmax;
    float    JetPt;
    float    EtMissCut;    
    float    MeffCut;   
    TString  Pairing;
  }; //!
  std::vector< SR > SignalRegions; //!
  bool addSR(TString name, int jetmin, int bmin, int bmax, float jetpt, float metmin, float meffmin, TString pairing); //!
  void PrintSR(SR sr, const char* APP_NAME); //!

  void PrintCutflow(TH1* hCut); //!
  
  std::vector< std::string > triggerInfo; //!
  std::vector< std::string > triggerList2015; //!
  std::vector< std::string > triggerList2016; //!
  std::vector< std::string > triggerList2017; //!
  std::vector< std::string > triggerList2018; //!
  bool setTriggers(std::vector<std::string> &trigList, std::string trigname="", int option=2016); //!
  bool isTriggered(std::vector<std::string> trigList, std::vector<std::string> &trigInfo, bool enabled=1); //!
  bool isTriggeredMet(std::vector<std::string> trigList, float met, std::vector<std::string> trigInfo, float metcut=250000.); //!
  bool isTriggeredMet2016(std::vector<std::string> trigList, float met, std::vector<std::string> trigInfo, int period, float metcut=250000.); //!
  
  std::vector<std::string> Period_List_Vectors(int data_period);//!
  bool HLT_mu_Trigg(std::string trigInfo);//!
  bool isTriggeredMulti(std::vector<std::string> trigList, std::vector<std::string> &trigInfo, bool enabled, int data_period);//!
  void setTrigItem(std::vector<std::string> &trigList, std::string trigItem, unsigned int it); //!  
  int  nMetTrigger(std::vector<std::string> trigInfo); //!
  int  nMetTrigger_xe90(std::vector<std::string> trigInfo); //!
  int  nMetTrigger_xe100(std::vector<std::string> trigInfo); //!
  int  nMetTrigger_xe110(std::vector<std::string> trigInfo); //!
  
  bool isResonance(std::vector<TLorentzVector> vectors, std::vector<float> charges, int channel=3); //!
  
  float getMt(std::vector< TLorentzVector > leptons, float met, float met_phi); //!
  float getPileupWeight(); //!
  
  bool inCrackRegion(xAOD::Electron *el); //! 
  
  void getSignalLeptons(const char* type, std::vector< TLorentzVector > &leptons, std::vector<float> &charges, std::vector<float> &qual, unsigned int Nlep); //!
  bool hasLeptons(std::vector<TLorentzVector> leptons, unsigned int nLepCut=2); //!
  bool SSleptons(std::vector<TLorentzVector> lepTLV, std::vector<float> lepCharges); //!
  std::vector< TLorentzVector > SSpair(std::vector<TLorentzVector> leptons, std::vector<float> charges); //!
  int getChannel(std::vector<TLorentzVector> ss, std::vector<TLorentzVector> muons, std::vector<TLorentzVector> electrons, int lepConfig); //!
  int getLepNumber(std::vector<TLorentzVector> mu, std::vector<TLorentzVector> el, bool noEtaCut=true, float ele_TrackEtaCut=999); //!

  void sortPt(std::vector<TLorentzVector>& v, std::vector<float>& c, std::vector<float>& q); //!
  float dR(float eta1, float eta2, float phi1, float phi2); //!
  float dR(TLorentzVector v1, TLorentzVector v2); //!
  float MjjdRmin(std::vector<TLorentzVector> jets); //!
  std::vector<float> getGGMGridWeight(xAOD::TEvent *event, bool isGGM, int pdgid1, int pdgid2, bool debug); //!

  enum JetWeight {DL1r, JVT}; //!  

 
#ifndef __MAKECINT__
  bool isTrigMatch(int yearOpt, xAOD::MuonContainer *muons, xAOD::ElectronContainer* electrons, float met, std::vector<std::string> trigInfo, bool doMatch=true, int period=0, int data_period=0, float metcut=250000.); //!
 
  bool getMET(float &met, float &metphi, xAOD::MissingETContainer *MET, 
	  const xAOD::JetContainer * jet, const xAOD::ElectronContainer * el,const xAOD::MuonContainer * mu, 
	  const xAOD::PhotonContainer * ph, bool doTST=true, bool doJVT=true, bool info=false); //!
	  
  bool cosmicsVeto(const xAOD::MuonContainer* muons, bool enabled=0); //!
  bool badJetVeto(const xAOD::JetContainer*   jets,  bool enabled=1); //!
  bool badMuonVeto(const xAOD::MuonContainer* muons, bool enabled=1); //!
  bool badJvt(const xAOD::Jet& jet, float ptCut=60000., float jvtCut=0.5, float jvtCut_HightEta=-999); //!
  
  int getQuality(const xAOD::Muon& mu); //!
  int getQuality(const xAOD::Electron& el); //!
  int getQuality(const xAOD::Jet& jet); //!
  
  void checkOR(const xAOD::IParticleContainer *p); //!  
/*  
  void Lep_Jet_OR(xAOD::IParticleContainer *electrons_baseline,xAOD::IParticleContainer *muons_baseline,xAOD::JetContainer *jets_baseline); //!
  void Electron_Muon_OR(xAOD::IParticleContainer *electrons_baseline,xAOD::IParticleContainer *muons_baseline); //!

  bool isBaseline(xAOD::ElectronContainer *electrons, bool setCuts=true, float d0Cut=5., float pTCut=10000.); //!
  bool isBaseline(xAOD::MuonContainer* muons, bool setCuts=true, float pTCut=10000., float etaCut=2.5); //!
  std::vector< std::vector<float> > getECIDSbtd(const xAOD::ElectronContainer *electrons); //!
  bool isbJet(const xAOD::Jet &jet); //!
  double isbJetWeight(const xAOD::Jet &jet); //!
 
  bool isSignalMuon(const xAOD::Muon &mu, bool isSig=true, float etamax=2.5); //!
  bool isSignalElectron (const xAOD::Electron &el, bool isSig=true, float etamax=2.0); //!
*/

  bool isSignalMuon(const xAOD::Muon &mu); //!
  bool isSignalElectron (const xAOD::Electron &el, float etamax=2.0); //!





  bool isSignalJet(const xAOD::Jet& jet, float ptcut=20000., float EtaMax=2.8, float EtaMaxB=2.5); //!



  std::vector<float> getBtags(std::vector<TLorentzVector> jetV, const xAOD::JetContainer *jets); //!  
  int getNjets(const xAOD::JetContainer *Jets, int seli); //!

  float getWeight(const xAOD::Jet& jet, DiLepton::JetWeight type, bool msg=0); //!
  std::vector<float> getGlobTriggerSF(int yearOpt, 
	  const xAOD::MuonContainer *Muons, const xAOD::ElectronContainer* Electrons, 
	  float met, std::vector<std::string> triggerInfo, bool doSF, bool info=0, float metcut=250000.);//!
  std::vector<float> getSF(const xAOD::MuonContainer *muons, MCTemplateCorr fakeCorr, int mcChNumber, float& wFakeMu_MCTemplate, float& uncFakeMu_MCTemplate, bool doSF, bool info=0);
  std::vector<float> getSF(const xAOD::ElectronContainer *electrons, MCTemplateCorr fakeCorr, int mcChNumber, float& wFakeEl_MCTemplate, float& uncFakeEl_MCTemplate/**, float el_ECIDS*/, bool doSF, bool info=0); //!
  float getTotalSF(const xAOD::MuonContainer *mu, const xAOD::ElectronContainer* el, const xAOD::JetContainer *je, /**float mu_iso, float ele_iso, float el_cft,*/ bool debug=false); //!

  bool resetSystematics(bool info=1); //!
  ST::SystInfo emptySys(); //!
  bool applySystematic(CP::SystematicSet sys, bool info=1); //!
  void PrintSys(std::vector<ST::SystInfo> syslist); //!
  void checkNames(std::vector<ST::SystInfo> &syslist, std::vector<std::string> names); //!
  void checkNames_ToRemoveSyst(std::vector<ST::SystInfo> &syslist, std::vector<std::string> names_to_remove); //
  void checkNames(std::vector< SR > &signalregions, std::vector<std::string> names); //! 
  enum SysType {Weight, Kinematic}; //!
  CP::SystematicSet getSys(std::string name, std::vector<ST::SystInfo> syslist); //!
  std::vector<ST::SystInfo> SysInfoList; //!
  std::vector<ST::SystInfo> SysInfoList_W; //!
  std::vector<ST::SystInfo> SysInfoList_K; //!
  std::vector<ST::SystInfo> getSysInfoList(bool fill, std::vector<ST::SystInfo> syslist, DiLepton::SysType type, bool useTau=0, bool usePhoton=0); //!
  std::vector<std::string> getSysNames(std::vector<ST::SystInfo> syslist); //!
  std::vector<float> getSysWeights(const xAOD::MuonContainer *Muons, const xAOD::ElectronContainer *Electrons, const xAOD::JetContainer *Jets, 
      std::vector<ST::SystInfo> syslist, std::vector<std::string> &names, unsigned int nsys, bool _debug=false, unsigned int dataperiod=0); //!

  int truthID(const xAOD::Jet *jet, bool isMC); //!
  float getTruthVar(xAOD::TEvent *event, int type); //!
  bool truthVeto(int MCId, float trHt, float trMET); //!
  std::vector<int> getTruthInfo(std::vector<TLorentzVector> lep, const xAOD::MuonContainer *muons, const xAOD::ElectronContainer *electrons); //!
  bool getJetOrigin(const xAOD::JetContainer *Jets, xAOD::TEvent *event, bool isMC, bool msg=0); //!
  bool getMotherParticle(const xAOD::Jet *jet, xAOD::TEvent *event, bool msg); //!
  bool getParents(const xAOD::TruthParticle *tp, bool &isW, bool &isZ, bool &isLQ, bool &isTop, bool &isLepPh, bool &isMeson, bool msg); //!
  float quarkFromWdR(const xAOD::TruthParticle *tp, bool msg); //!
  std::vector<int> getSusyPDG(xAOD::TEvent *event); //! 

 
  float getDPSweight(int MCId, int njets); //!
  bool isSherpaZllMC(const xAOD::EventInfo* eventInfo); //!
#endif


 public:

  int Debug;
  int isData;
  int isAtlfast;
  int isAtlfast2;
  int isAtlfast3;
  int PileupReweighting;
  int doSys;
  bool isFakeMC;
  int makeHist;
  int makeTree;
  int setLeptons;
  int doBJetVeto;
  int NSignalLeptons;
  int isPHYSLITE;  



  std::string SysList;
  std::vector<std::string> SysList_toRemove={
	"JET_Rtrk_Baseline_mass__1up",
	"JET_Rtrk_Baseline_mass__1down",
	"JET_Rtrk_Baseline_pT__1up",
	"JET_Rtrk_Baseline_pT__1down",
	"JET_Rtrk_Closure_mass__1up",
	"JET_Rtrk_Closure_mass__1down",
	"JET_Rtrk_Closure_pT__1up",
	"JET_Rtrk_Closure_pT__1down",
	"JET_Rtrk_Modelling_mass__1up",
	"JET_Rtrk_Modelling_mass__1down",
	"JET_Rtrk_Modelling_pT__1up",
	"JET_Rtrk_Modelling_pT__1down",
	"JET_Rtrk_TotalStat_mass__1up",
	"JET_Rtrk_TotalStat_mass__1down",
	"JET_Rtrk_TotalStat_pT__1up",
	"JET_Rtrk_TotalStat_pT__1down",
	"JET_Rtrk_Tracking_mass__1up",
	"JET_Rtrk_Tracking_mass__1down",
	"JET_Rtrk_Tracking_pT__1up",
	"JET_Rtrk_Tracking_pT__1down",
	"JET_PunchThrough_AFII__1up",
	"JET_PunchThrough_AFII__1down"
  };
 
  
  std::string TriggerName;
  std::string SigRegion;
 
 public:

  int     m_entry; //!
  int     m_fileentry; //!

  float   lep_PtCut[3]; //!
  float   jet_PtCut[2]; //!
  bool    applySF; //!
  bool    doPileup; //!
  bool    truthMsg; //!
  bool    doJetCut; //!
  bool    ccCheck; //! 

  bool    usePhotons; //!

  TTree *ControlTree; //!
  TTree *DiLeptonTree[NSR][NSYS]; //!

  /// Histograms added to output
  TH1    *hCutflow     = 0; //!
  TH1    *hEventsTotal = 0; //!

  TH1    *hEvents[NSR][NSYS] = {{0}}; //!
  TH1    *hEventWeight[NSR][NSYS]   = {{0}}; //!
  TH1    *hEventWeightSF[NSR][NSYS] = {{0}}; //!
  
  TH1    *BaselineMet[NSR][NSYS]    = {{0}}; //!
  TH2    *BaselineLepPt[NSR][NSYS]  = {{0}}; //!
  TH2    *BaselineLepEta[NSR][NSYS] = {{0}}; //!

  TH1    *BaselineJet_Mj[NSR][NSYS]  = {{0}}; //!
  TH1    *BaselineJet_Mjj[NSR][NSYS] = {{0}}; //!
  TH1    *BaselineJet_MjjdR[NSR][NSYS] = {{0}}; //! 
  TH1    *BaselineJet_MjRec[NSR][NSYS] = {{0}}; //!
  TH1    *BaselineJet_MjAll[NSR][NSYS] = {{0}}; //! 
  TH1    *BaselineJet_MjRecAll[NSR][NSYS] = {{0}}; //! 

  TH1    *TrigMet[NSR][NSYS]  = {{0}}; //!
  TH2    *TrigMuPt[NSR][NSYS] = {{0}}; //!
  TH2    *TrigMuEta[NSR][NSYS] = {{0}}; //!
  TH2    *TrigElPt[NSR][NSYS]  = {{0}}; //!
  TH2    *TrigElEta[NSR][NSYS] = {{0}}; //!
  TH2    *TrigJetPt[NSR][NSYS] = {{0}}; //! 

  TH1    *NMu[NSR][NSYS]   = {{0}}; //!
  TH1    *NMu_EWcombi[NSR][NSYS]   = {{0}}; //!
  TH1    *NEl[NSR][NSYS]   = {{0}}; //!
  TH1    *NEl_EWcombi[NSR][NSYS]   = {{0}}; //!
  TH1    *NJet[NSR][NSYS]  = {{0}}; //!
  TH1    *NBJet[NSR][NSYS] = {{0}}; //!

  TH1    *Mu_1stPt[NSR][NSYS] = {{0}}; //!
  TH1    *Mu_2ndPt[NSR][NSYS] = {{0}}; //!
  TH1    *Mu_3rdPt[NSR][NSYS] = {{0}}; //!
  TH1    *El_1stPt[NSR][NSYS] = {{0}}; //!
  TH1    *El_2ndPt[NSR][NSYS] = {{0}}; //!
  TH1    *El_3rdPt[NSR][NSYS] = {{0}}; //!
  TH1    *Lep_1stPt[NSR][NSYS] = {{0}}; //!
  TH1    *Lep_2ndPt[NSR][NSYS] = {{0}}; //!
  TH1    *Lep_3rdPt[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_1stPt[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_2ndPt[NSR][NSYS] = {{0}}; //!
  TH1    *Mu_1stEta[NSR][NSYS] = {{0}}; //!
  TH1    *Mu_2ndEta[NSR][NSYS] = {{0}}; //!
  TH1    *El_1stEta[NSR][NSYS] = {{0}}; //!
  TH1    *El_2ndEta[NSR][NSYS] = {{0}}; //!
   //
  TH1    *Jet_diffEta[NSR][NSYS] = {{0}}; //!

 TH1    *Jet_diffPhi[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_mulEta[NSR][NSYS] = {{0}}; //!

 TH1    *Jet_1stEta[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_2ndEta[NSR][NSYS] = {{0}}; //!

   TH1    *Jet_1stPhi[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_2ndPhi[NSR][NSYS] = {{0}}; //!
TH1    *Jet_3thEta[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_4thEta[NSR][NSYS] = {{0}}; //!

  TH1    *C3[NSR][NSYS] = {{0}}; //!
  TH1    *C4[NSR][NSYS] = {{0}}; //!
/////////

  TH1    *Jet_Mj[NSR][NSYS]  = {{0}}; //!
  TH1    *Jet_Mjj[NSR][NSYS] = {{0}}; //!
  TH1    *Mtn[NSR][NSYS] = {{0}}; //!
  TH1    *Mll[NSR][NSYS] = {{0}}; //!
 TH1    *Etll[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_M24[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_Mrel4[NSR][NSYS] = {{0}}; //!
    TH1    *Jet_Mrel3[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_M23[NSR][NSYS] = {{0}}; //!
   TH1    *Jet_M14[NSR][NSYS] = {{0}}; //!
    TH1    *Jet_M13[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_MjjdR[NSR][NSYS] = {{0}}; //! 
  TH1    *Jet_MjRec[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_MjAll[NSR][NSYS] = {{0}}; //!
  TH1    *Jet_MjRecAll[NSR][NSYS] = {{0}}; //! 



  TH1    *ETmiss[NSR][NSYS] = {{0}}; //!
  TH1    *Meff[NSR][NSYS]   = {{0}}; //!
  TH1    *Minv[NSR][NSYS]   = {{0}}; //!
  TH1    *MT[NSR][NSYS] = {{0}}; //!
  TH1    *HT[NSR][NSYS] = {{0}}; //!

  TH1    *Mu_SumPt[NSR][NSYS] = {{0}}; //! 
  TH1    *El_SumPt[NSR][NSYS] = {{0}}; //! 
  TH1    *Jet_SumPt[NSR][NSYS] = {{0}}; //!

  xAOD::TEvent *m_event; //!
  xAOD::TStore store; //!


#ifndef __MAKECINT__
  void PrintInfo(const xAOD::Electron* el,const char* APP_NAME, bool msg); //!
  void PrintInfo(const xAOD::Muon* mu,    const char* APP_NAME, bool msg); //!
  void PrintInfo(const xAOD::Jet* jet,    const char* APP_NAME, bool msg); //!
  void PrintInfo(const xAOD::Photon* ph,  const char* APP_NAME, bool msg); //!
  void PrintInfo(const xAOD::TruthParticle *tp, const char* APP_NAME, bool msg); //!
#endif

  /// standard constructor
  DiLepton ();
  
  
  /// these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  /// this is needed to distribute the algorithm to the workers
  ClassDef(DiLepton, 1);
};

#endif
