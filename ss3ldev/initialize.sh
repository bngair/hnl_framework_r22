cd ..
setupATLAS -3
lsetup cmake
echo 'in dir ' $PWD 

#select here (only) the Analysis Base version
#24.2.9 
#22.2.113
AnalysisBaseVersion=24.2.13
echo 'AnalysisBase version: ' $AnalysisBaseVersion


ntuple_string='HNL_Ntuples_AB24_13'
trees_string='HNL_Trees_AB24_13'


# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisBaseReleaseNotes21_2
if [ ! -d "build/" ]; then

    mkdir build
    cd build/

    lsetup "asetup $AnalysisBaseVersion,AnalysisBase" rucio panda
	# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisRelease
    
    cmake -DANALYSBASE=$AnalysisBaseVersion ../ss3ldev
    cmake --build .
	echo $CMTCONFIG
    source $CMTCONFIG/setup.sh
	
else
    cd build/
    lsetup "asetup $AnalysisBaseVersion,AnalysisBase" rucio panda
    echo 'in dir ' $PWD
    #cmake --clean-first .
    cmake --build . --target clean
    cmake -DANALYSBASE=$AnalysisBaseVersion ../ss3ldev
    cmake --build .
	echo $CMTCONFIG
    source $CMTCONFIG/setup.sh
fi

cd ..

if [ ! -d "Run/" ]; then
    mkdir Run;
    cd Run;
	echo "Now in Run dir!"
    ln -s ../ss3ldev/DiLepton/scripts/*py .;
    cd -;
fi

#Basic initializations
#select the base directory' id you work on lxplus select '/afs/cern.ch/work/user' 
export BASEDIR=$PWD


#export AFSDIR=/afs/cern.ch/work/${USER:0:1}/$USER
export EOSDIR=/eos/user/${USER:0:1}/$USER
#export CUSTOMDIR=/your/custom/path

#cd AFSDIR
cd $EOSDIR
#cd $CUSTOMDIR


if [ ! -d "hpp/" ]; then
    mkdir hpp;
        echo "hpp/ folder created"
else
    echo "hpp directory exist"
fi

export WORKDIR=$EOSDIR/hpp
#export WORKDIR=$AFSDIR/hpp
#export WORKDIR=$CUSTOMDIR/hpp

export GRID_ROOT=$EOSDIR



cd $WORKDIR
if [ ! -d "LOGS/" ]; then
    mkdir LOGS
else
    echo 'LOGS directory exist' $WORKDIR/LOGS
fi

if [ ! -d "INPUTFILES/" ]; then
    mkdir INPUTFILES
else
    echo 'INPUTFILES directory exist' $WORKDIR/INPUTFILES
fi

if [ ! -d "OUTPUTFILES/" ]; then
    mkdir OUTPUTFILES
else
    echo 'OUTPUTFILES directory exist' $WORKDIR/OUTPUTFILES
fi

export LOGS_DIR=$WORKDIR/LOGS


cd $GRID_ROOT
if [ ! -d "GRID_NTUP/" ]; then
    mkdir GRID_NTUP
else
    #echo 'Grid directory exist' $GRID_ROOT/GRID_NTUP
fi

export GRID_DIR=$GRID_ROOT/GRID_NTUP


cd $GRID_DIR

#if [ ! -d "$ntuple_string/" ]; then
#    mkdir $ntuple_string
#else
    #echo 'NTUPLE directory exist' $NTUPLE_DIR/$ntuple_string
#fi

#if [ ! -d "$trees_string/" ]; then
#    mkdir $trees_string
#else
#    #echo 'Trees directory exist' $NTUPLE_DIR/$trees_string
#fi


export NTUPLE_DIR=$GRID_DIR/$ntuple_string
export TREES_DIR=$GRID_DIR/$trees_string



cd $BASEDIR/hnl_framework_r22

# Add same AB release to CI & Docker files
sed -i '/^  image: atlas\/analysisbase*/c\ \ image: atlas\/analysisbase:$AnalysisBaseVersion' ./ss3ldev/.gitlab-ci.yml
sed -i '/^FROM atlas\/analysisbase*/cFROM atlas\/analysisbase:$AnalysisBaseVersion' ./ss3ldev/Dockerfile
