pwd
export BASEDIR=$PWD
mkdir run
cd run
echo 'cp ../DiLepton/scripts/run.py ./'
cp ../DiLepton/scripts/run.py ./
echo 'mkdir -p TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/'
mkdir -p TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/
echo 'xrdcp root://eosuser.cern.ch//eos/user/l/liuya/RECAST/SS3LDev/TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/DDAOD_SUSY2.23125625._000001.pool.root.1 $BASEDIR/run/TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/DAOD_SUSY2.23125625._000001.pool.root.1'

xrdcp root://eosuser.cern.ch//eos/user/l/liuya/RECAST/SS3LDev/TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/DAOD_SUSY2.23125625._000001.pool.root.1 $BASEDIR/run/TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/DAOD_SUSY2.23125625._000001.pool.root.1 

echo 'ls TestInputDAODs/'
ls TestInputDAODs/

echo 'python run.py --LocalSample "mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990" --InPath "$BASEDIR/run/TestInputDAODs/"'

python run.py --LocalSample "mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990" --InPath "$BASEDIR/run/TestInputDAODs"

