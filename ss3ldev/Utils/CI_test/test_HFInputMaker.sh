pwd
export BASEDIR=$PWD
mkdir run
cd run
echo 'cp ../ss3ldev/DiLepton/scripts/run.py ./'
cp ../ss3ldev/DiLepton/scripts/run.py ./
echo 'mkdir -p TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/'
mkdir -p TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/
echo 'xrdcp root://eosuser.cern.ch//eos/user/l/liuya/RECAST/SS3LDev/TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/DDAOD_SUSY2.23125625._000001.pool.root.1 $BASEDIR/run/TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/DAOD_SUSY2.23125625._000001.pool.root.1'

xrdcp root://eosuser.cern.ch//eos/user/l/liuya/RECAST/SS3LDev/TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/DAOD_SUSY2.23125625._000001.pool.root.1 $BASEDIR/run/TestInputDAODs/mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990/DAOD_SUSY2.23125625._000001.pool.root.1 

echo 'ls TestInputDAODs/'
ls TestInputDAODs/

echo 'python run.py --LocalSample "mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990" --InPath "$BASEDIR/run/TestInputDAODs/"'

python run.py --LocalSample "mc16_13TeV.412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990" --InPath "$BASEDIR/run/TestInputDAODs"

cd $BASEDIR
cd run

echo $PWD
echo 'ln -s ../ss3ldev/Utils/makeHFInput.C'
ln -s ../ss3ldev/Utils/makeHFInput.C 
ln -s ../ss3ldev/Utils/command_ihep_run_makeHFInput.sh

mkdir -p $BASEDIR/INPUTFILES/Test_HFInputMaker/
mkdir -p $BASEDIR/OUTPUTFILES/Test_HFInputMaker/
mv $BASEDIR/OUTPUTFILES/*/TASK*/data-output/* $BASEDIR/INPUTFILES/Test_HFInputMaker/user.liuya.SystABp149_412043.aMcAtNloPythia8EvtGen_A14NNPDF31_SM4topsNLO.deriv.DAOD_SUSY2.e7101_a875_r9364_p3990_output_merged.root

# echo ''
#root -b -q "makeHFInput.C(\"$BASEDIR/INPUTFILES/Test_HFInputMaker/\", \"../OUTPUTFILES/Test_HFInputMaker/Test_4tops.root\", \"DiLeptonTree_SRall\", \"4tops\", \"$BASEDIR/ss3ldev/Utils/xSecDir/\", 36207.659,44307.4,58450.1, false, \"\")"
#root -b -q makeHFInput.C
source command_ihep_run_makeHFInput.sh
